<?php

namespace Tests\Unit;

use App\Codebase\Constants\Fields;
use App\Codebase\Constants\Statuses;
use App\Category;
use App\Product;
use App\Repositories\ProductRepository;
use App\Repositories\Scopes\CategoryScope;
use App\Repositories\Scopes\StatusScope;
use App\Repositories\Scopes\UserScope;
use App\Support;
use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class ProductsTest extends TestCase
{
    use RefreshDatabase;

    private $repository;

    public function __construct(?string $name = null, array $data = [], string $dataName = '')
    {
        parent::__construct($name, $data, $dataName);

        $this->repository = app(ProductRepository::class);
    }

    public function testFind()
    {
        $product = $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
        ]);

        $found = $this->repository->find($product->getId());

        $this->assertNotNull($found);
    }

    public function testFindBy()
    {
        $product = $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
        ]);

        $found = $this->repository->findBy(Product::FIELD_NAME, $product->getName());

        $this->assertNotNull($found);
    }

    public function testCreate()
    {
        $product = $this->getFactory()->make([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1
        ]);

        $created = $this->repository->save($product);

        $this->assertNotNull($created);
    }

    public function testUpdate()
    {
        $product = $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1
        ]);

        $product->setName('foo');

        $updated = $this->repository->save($product);

        $this->assertTrue($updated);
    }

    public function testSearch()
    {
        $keyword = 'foo';

        $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
            Product::FIELD_NAME => 'foo'
        ]);

        $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
            Product::FIELD_NAME => 'bar'
        ]);

        $results = $this->repository->search(Product::FIELD_NAME, $keyword);

        $this->assertCount(1, $results);
    }

    public function testFindAll()
    {
        $this->getFactory(5)->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
        ]);

        $results = $this->repository->all();

        $this->assertCount(5, $results);
    }


    public function testDelete()
    {
        $product = $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
        ]);

        $deleted = $this->repository->delete($product->getId());

        $this->assertTrue($deleted);
    }

    public function testDeleteBy()
    {
        $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
        ]);

        $this->getFactory()->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
        ]);

        $this->getFactory()->create([
            Fields::CATEGORY_ID => 2,
            Fields::USER_ID => 2,
        ]);

        $this->repository->deleteBy(Fields::CATEGORY_ID, 1);

        $count = Product::count();

        $this->assertEquals(1, $count);
    }

    public function testCount()
    {
        $this->getFactory(5)->create([
            Fields::CATEGORY_ID => 1,
            Fields::USER_ID => 1,
            Fields::STATUS => Statuses::DRAFT,
        ]);

        $this->getFactory(5)->create([
            Fields::CATEGORY_ID => 2,
            Fields::USER_ID => 2,
            Fields::STATUS => Statuses::PUBLISHED,
        ]);

        $this->assertEquals(10, $this->repository->count());

        $this->repository->clearScopes();

        $this->repository->addScope(new CategoryScope(1));
        $this->assertEquals(5, $this->repository->count());

        $this->repository->clearScopes();

        $this->repository->addScope(new UserScope(1));
        $this->assertEquals(5, $this->repository->count());

        $this->repository->clearScopes();

        $this->repository->addScope(new StatusScope(Statuses::DRAFT));
        $this->assertEquals(5, $this->repository->count());

        $this->repository->clearScopes();

        $this->repository->addScope(new CategoryScope(2));
        $this->assertEquals(5, $this->repository->count());

        $this->repository->clearScopes();

        $this->repository->addScope(new UserScope(2));
        $this->assertEquals(5, $this->repository->count());

        $this->repository->clearScopes();

        $this->repository->addScope(new StatusScope(Statuses::PUBLISHED));
        $this->assertEquals(5, $this->repository->count());
    }

    public function test_product_belongs_to_user()
    {
        $user = factory(User::class)->create();

        $product = $this->getFactory()->create([
            Fields::USER_ID => $user->getId(),
            Fields::CATEGORY_ID => 1,
        ]);

        $this->assertEquals($user->getId(), optional($product->user)->getId());
    }

    public function test_product_belongs_to_category()
    {
        $category = factory(Category::class)->create();

        $product = $this->getFactory()->create([
            Fields::USER_ID => 1,
            Fields::CATEGORY_ID => $category->getId(),
        ]);

        $this->assertEquals($category->getId(), optional($product->category)->getId());
    }

    public function test_product_has_abuse_reports()
    {
        $product = $this->getFactory()->create([
            Fields::USER_ID => 1,
            Fields::CATEGORY_ID => 1,
        ]);

        factory(Support::class)->create([
            Support::FIELD_SENDER_ID => 1,
            Support::FIELD_ABUSER_ID => 2,
            Support::FIELD_ABUSABLE_ID => $product->getId(),
            Support::FIELD_ABUSABLE_TYPE => Product::class,
        ]);

        $this->assertEquals(1, $product->abuseReports->count());
    }

    private function getFactory($times = null)
    {
        return factory(Product::class, $times);
    }
}
