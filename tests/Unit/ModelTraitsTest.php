<?php

namespace Tests\Unit;

use App\Codebase\Constants\Fields;
use App\Codebase\Constants\Statuses;
use App\Traits\BelongsToBrand;
use App\Traits\BelongsToCategory;
use App\Traits\BelongsToUser;
use App\Traits\HasDescription;
use App\Traits\HasId;
use App\Traits\HasName;
use App\Traits\HasPrice;
use App\Traits\HasQuantity;
use App\Traits\HasStatus;
use App\Traits\HasVAT;
use App\User;
use Tests\TestCase;

class MockModel {
    private $attributes = [];
    public function setAttribute($name, $value) {
        $this->attributes[$name] = $value;
    }
    public function getAttribute($name) {
        if (isset($this->attributes[$name])) {
            return $this->attributes[$name];
        }
        return false;
    }
}

class ModelTraitsTest extends TestCase
{
    public function testBelongsToBrand()
    {
        $modelMock = new class extends MockModel {
            use BelongsToBrand;
        };
        $modelMock->setBrandId(1);
        $this->assertTrue($modelMock->getBrandId() === 1);
    }

    public function testBelongsToBrandWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use BelongsToBrand;
        };
        $modelMock->setBrandId('lorem');
    }

    public function testBelongsToCategory()
    {
        $modelMock = new class extends MockModel {
            use BelongsToCategory;
        };
        $modelMock->setCategoryId(1);
        $this->assertEquals(1, $modelMock->getCategoryId());
    }

    public function testBelongsToCategoryWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use BelongsToCategory;
        };
        $modelMock->setCategoryId([]);
    }

    public function testHasDescription()
    {
        $modelMock = new class extends MockModel {
            use HasDescription;
        };
        $modelMock->setDescription('foo');
        $this->assertEquals('foo', $modelMock->getDescription());
    }

    public function testHasId()
    {
        $modelMock = new class extends MockModel {
            use HasId;
        };
        $modelMock->setId(1);
        $this->assertEquals(1, $modelMock->getId());
    }

    public function testHasName()
    {
        $modelMock = new class extends MockModel {
            use HasName;
        };
        $modelMock->setName('foo');
        $this->assertEquals('foo',  $modelMock->getName());
    }

    public function testHasPriceWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use HasPrice;
        };
        $modelMock->setPrice('foo');
    }

    public function testHasPrice()
    {
        $modelMock = new class extends MockModel {
            use HasPrice;
        };
        $modelMock->setPrice(1.25);
        $this->assertEquals(1.25, $modelMock->getPrice());
    }

    public function testHasQuantity()
    {
        $modelMock = new class extends MockModel {
            use HasQuantity;
        };
        $modelMock->setQuantity(1);
        $this->assertEquals(1, $modelMock->getQuantity());
    }

    public function testHasStatus()
    {
        $modelMock = new class extends MockModel {
            use HasStatus;
        };
        $modelMock->setStatus(Statuses::PUBLISHED);
        $this->assertEquals(Statuses::PUBLISHED, $modelMock->getStatus());
    }

    public function testHasStatusWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use HasStatus;
        };
        $modelMock->setStatus(new class {});
    }

    public function testHasVAT()
    {
        $modelMock = new class extends MockModel {
            use HasVAT;
        };
        $modelMock->setVAT(1.25);
        $this->assertEquals(1.25, $modelMock->getVAT());
    }

    public function testHasVATWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use HasVAT;
        };
        $modelMock->setVAT('some-string-value');
    }

    public function testScopeForUser()
    {
        $modelMock = new class extends MockModel {
            use BelongsToUser;
        };
        $queryMock = new class {
            public function where() {
                return func_get_args();
            }
        };
        $targetUserId = 1;
        $expected = [
            Fields::USER_ID,
            $targetUserId
        ];
        $actual = $modelMock->scopeForUser($queryMock, $targetUserId);
        $this->assertEquals($expected, $actual);
    }

    public function testScopeForUserWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use BelongsToUser;
        };
        $modelMock->scopeForUser(new class {}, 'number-one');
    }

    public function testBelongsToUser()
    {
        $modelMock = new class extends MockModel {
            use BelongsToUser;
        };
        $modelMock->setUserId(1);
        $this->assertEquals(1, $modelMock->getUserId());
    }

    public function testBelongsToUserWithTypeError()
    {
        $this->expectException('TypeError');
        $modelMock = new class extends MockModel {
            use BelongsToUser;
        };
        $modelMock->setUserId([]);
    }

    public function testBelongsToUserTraitUserRelation()
    {
        $modelMock = new class extends MockModel {
            use BelongsToUser;
            public function belongsTo() {
                return func_get_args();
            }
        };
        $expected = [
            User::class,
            Fields::USER_ID,
            Fields::PK
        ];
        $passedArgs = $modelMock->user();
        $this->assertEquals($expected,$passedArgs);
    }
}