<?php

use App\Actions\User\CreateUser;
use App\Post;
use App\Product;
use App\User;
use Packagen\Generators\ControllerGenerator;
use Packagen\Generators\EventGenerator;
use Packagen\Generators\FactoryGenerator;
use Packagen\Generators\HttpRequestGenerator;
use Packagen\Generators\HttpResourceGenerator;
use Packagen\Generators\MigrationGenerator;
use Packagen\Generators\ModelGenerator;
use Packagen\Generators\PresenterGenerator;
use Packagen\Generators\RoutesGenerator;
use Packagen\Interfaces\GeneratorInterface;
use Packagen\PackagenManager;

Route::get('/', function (CreateUser $action) {

    /** @var \App\Product $product */
    $product = Product::find(1);

    dd($product->setName('foo')->getName());

//     Post::with(['taxonomies', 'user'])->without('meta')->find(1);
return '<body></body>';
    try {

        /*$user = $action->execute([
            User::FIELD_NAME => 'Mohamed',
            User::FIELD_EMAIL => 'm.zahran910@gmail.com',
            User::FIELD_PASSWORD => bcrypt(123456),
            User::FIELD_API_TOKEN => str_random(60),
            User::FIELD_IS_ACTIVE => true
        ]);*/

        /*$user = (new User)
                ->setName('Mohamed')
                ->setEmail('m.zahran910@gmail.com')
                ->setPassword(bcrypt(123456))
                ->setApiToken(str_random(60))
                ->setActive()
        ;

        $user = $action->execute($user);

        if ($user) {
            dd($user);
        } 

        return 'not created';*/

    } catch (Exception $e) {
        dd($e->getMessage());
    }
});

/*Route::get('/', function (\App\Repositories\BrandRepository $repository) {

    $packagen = new PackagenManager('Category');

    $params = $packagen->getParams();

    $packagen->registerGenerators(collect([
        // Http
        new ControllerGenerator($params),
        new HttpRequestGenerator($params),
        new HttpResourceGenerator($params),
        // Model
        new ModelGenerator($params),
        // Repositories
        // Database
        new FactoryGenerator($params),
        new MigrationGenerator($params),
        // Routes
        new RoutesGenerator($params, 'web.php', 'routes.web.stub'),
        new RoutesGenerator($params, 'api.php', 'routes.api.stub'),
        // Presenters
        new PresenterGenerator($params, 'UrlPresenter.php', 'url-presenter.stub'),
        // Events
        new EventGenerator($params, 'Created'),
        new EventGenerator($params, 'Updated'),
        new EventGenerator($params, 'Deleted'),
    ]));

    $packagen->generate();

    return view('welcome');
});
*/

Route::namespace('Admin')->prefix('admin')->group(function () {
    Route::resource('brands', 'BrandController');
});