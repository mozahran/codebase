<?php

namespace App\Providers;

use App\Repositories\BrandRepository;
use App\Repositories\Interfaces\BrandRepositoryInterface;
use App\Repositories\Interfaces\ChatRepositoryInterface;
use App\Repositories\ChatRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(BrandRepositoryInterface::class, BrandRepository::class);
        $this->app->bind(ChatRepositoryInterface::class, ChatRepository::class);

        $this->defineConstants();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    private function defineConstants()
    {
        if ( ! defined('KEYWORD_PER_PAGE')) {
            define('KEYWORD_PER_PAGE', 'per_page');
        }

        if ( ! defined('KEYWORD_CURRENT_PAGE')) {
            define('KEYWORD_CURRENT_PAGE', 'page');
        }

        if ( ! defined('KEYWORD_SHOW')) {
            define('KEYWORD_SHOW', 'show');
        }

        if ( ! defined('KEYWORD_SHOW_ALL')) {
            define('KEYWORD_SHOW_ALL', 'all');
        }

        if ( ! defined('KEYWORD_STATUS_ACTIVE')) {
            define('KEYWORD_STATUS_ACTIVE', 'active');
        }

        if ( ! defined('KEYWORD_STATUS_INACTIVE')) {
            define('KEYWORD_STATUS_INACTIVE', 'inactive');
        }

        if ( ! defined('KEYWORD_STATUS_SUSPENDED')) {
            define('KEYWORD_STATUS_SUSPENDED', 'suspended');
        }

        if ( ! defined('KEYWORD_STATUS_ENABLED')) {
            define('KEYWORD_STATUS_ENABLED', 'enabled');
        }

        if ( ! defined('KEYWORD_STATUS_DISABLED')) {
            define('KEYWORD_STATUS_DISABLED', 'disabled');
        }

        if ( ! defined('KEYWORD_STATUS_FEATURED')) {
            define('KEYWORD_STATUS_FEATURED', 'featured');
        }

        if ( ! defined('KEYWORD_STATUS_NOT_FEATURED')) {
            define('KEYWORD_STATUS_NOT_FEATURED', 'not_featured');
        }

        if ( ! defined('KEYWORD_STATUS_PUBLISHED')) {
            define('KEYWORD_STATUS_PUBLISHED', 'published');
        }

        if ( ! defined('KEYWORD_STATUS_DRAFT')) {
            define('KEYWORD_STATUS_DRAFT', 'draft');
        }

        if ( ! defined('KEYWORD_STATUS_PENDING')) {
            define('KEYWORD_STATUS_PENDING', 'pending');
        }

        if ( ! defined('KEYWORD_STATUS_APPROVED')) {
            define('KEYWORD_STATUS_APPROVED', 'approved');
        }

        if ( ! defined('KEYWORD_SEARCH_QUERY')) {
            define('KEYWORD_SEARCH_QUERY', 'q');
        }
    }
}
