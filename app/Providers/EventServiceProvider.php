<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\ProductCreated' => [],
        'App\Events\ProductUpdated' => [],
        'App\Events\ProductDeleted' => [],
        'App\Events\CategoryCreated' => [],
        'App\Events\CategoryUpdated' => [],
        'App\Events\CategoryDeleted' => [],
        'App\Events\BrandCreated' => [],
        'App\Events\BrandUpdated' => [],
        'App\Events\BrandDeleted' => [],
        'App\Events\OrderCreated' => [
            'App\Listeners\OrderCreatedListener',
        ],
        'App\Events\OrderShipped' => [
            'App\Listeners\OrderShippedListener',
        ],
        'App\Events\OrderCancelled' => [
            'App\Listeners\OrderCancelledListener',
        ],
        'App\Events\OrderRefunded' => [
            'App\Listeners\OrderRefundedListener',
        ],
        'App\Events\InvoiceCreated' => [
            'App\Listeners\InvoiceCreatedListener',
        ],
        'App\Events\InvoiceUpdated' => [
            'App\Listeners\InvoiceUpdatedListener',
        ],
        'App\Events\InvoiceDeleted' => [
            'App\Listeners\InvoiceDeletedListener',
        ],
        'App\Events\CouponUsed' => [],
        'App\Events\CouponUpdated' => [],
        'App\Events\CouponDeleted' => [],
        'App\Events\TaxRateCreated' => [],
        'App\Events\TaxRateUpdated' => [],
        'App\Events\TaxRateDeleted' => [],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
