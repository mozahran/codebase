<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use PackageGenerator\GeneratorManager;
use PackageGenerator\Generators\ControllerGenerator;
use PackageGenerator\Generators\EventGenerator;
use PackageGenerator\Generators\HttpRequestGenerator;
use PackageGenerator\Generators\HttpResourceGenerator;
use PackageGenerator\Generators\ModelGenerator;
use PackageGenerator\Generators\PresenterGenerator;
use PackageGenerator\Generators\RoutesGenerator;

class PackageGeneratorCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:package {package}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a new package.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $name = $this->argument('package');

        $this->info('Generating package "'. studly_case(strtolower($name)) .'" ...');

        $manager = new GeneratorManager($name);

        $params = $manager->getParams();

        $generators = collect([
            // Http
            new ControllerGenerator($params),
            new HttpRequestGenerator($params),
            new HttpResourceGenerator($params),
            // Model
            new ModelGenerator($params),
            // Repositories
            // Database
            // Routes
            new RoutesGenerator($params, 'web.php', 'routes.web.stub'),
            new RoutesGenerator($params, 'api.php', 'routes.api.stub'),
            // Presenters
            new PresenterGenerator($params, 'UrlPresenter.php', 'url-presenter.stub'),
            // Events
            new EventGenerator($params, 'Created'),
            new EventGenerator($params, 'Updated'),
            new EventGenerator($params, 'Deleted'),

        ]);

        $manager->registerGenerators($generators);

        $manager->generate();

        $this->info('Finished creating the package.');
    }
}
