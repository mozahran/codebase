<?php

namespace App;

use App\Events\InvoiceCreated;
use App\Events\InvoiceDeleted;
use App\Events\InvoiceUpdated;
use App\Exceptions\InvalidStatusException;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'invoices';

    const FIELD_PK = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_ORDER_ID = 'order_id';
    const FIELD_QTY = 'qty';
    const FIELD_TOTAL = 'total';
    const FIELD_STATUS = 'status';
    const FIELD_COMMENTS = 'comments';

    const STATUS_ALL = null;
    const STATUS_PAID = 'paid';
    const STATUS_DUE = 'due';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_ORDER_ID,
        self::FIELD_QTY,
        self::FIELD_TOTAL,
        self::FIELD_STATUS,
        self::FIELD_COMMENTS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_USER_ID => 'integer',
        self::FIELD_ORDER_ID => 'integer',
        self::FIELD_QTY => 'integer',
        self::FIELD_TOTAL => 'decimal',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => InvoiceCreated::class,
        'updated' => InvoiceUpdated::class,
        'deleted' => InvoiceDeleted::class,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'items'
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the invoice Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the user Id.
     *
     * @return int
     */
    public function getUserId() : int
    {
        return $this->getAttribute(self::FIELD_USER_ID);
    }

    /**
     * Get the order Id.
     *
     * @return int
     */
    public function getOrderId() : int
    {
        return $this->getAttribute(self::FIELD_ORDER_ID);
    }

    /**
     * Get the quantity of the items.
     *
     * @return int
     */
    public function getQty() : int
    {
        return $this->getAttribute(self::FIELD_QTY);
    }

    /**
     * Get the status of the invoice.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the invoice is paid.
     *
     * @return bool
     */
    public function isPaid() : bool
    {
        return $this->getStatus() == self::STATUS_PAID;
    }

    /**
     * Check if the invoice is due.
     *
     * @return bool
     */
    public function isDue() : bool
    {
        return $this->getStatus() == self::STATUS_DUE;
    }

    /**
     * Get the comments on the invoice.
     *
     * @return string
     */
    public function getComments() : string
    {
        return $this->getAttribute(self::FIELD_COMMENTS);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the user Id.
     *
     * @param int $id
     * @return $this
     */
    public function setUserId(int $id)
    {
        return $this->setAttribute(self::FIELD_USER_ID, $id);
    }

    /**
     * Set the order Id.
     *
     * @param int $id
     * @return $this
     */
    public function setOrderId(int $id)
    {
        return $this->setAttribute(self::FIELD_ORDER_ID, $id);
    }

    /**
     * Set the quantity of the items.
     *
     * @param int $qty
     * @return $this
     */
    public function setQty(int $qty)
    {
        return $this->setAttribute(self::FIELD_QTY, $qty);
    }

    /**
     * Set the status of the invoice.
     *
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status)
    {
        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the invoice as paid.
     *
     * @return $this
     */
    public function setPaid()
    {
        return $this->setStatus(self::STATUS_PAID);
    }

    /**
     * Set the invoice as due.
     *
     * @return $this
     */
    public function setDue()
    {
        return $this->setStatus(self::STATUS_DUE);
    }

    /**
     * Get the comments on the invoice.
     *
     * @param string $comments
     * @return $this
     */
    public function setComments(string $comments)
    {
        return $this->setAttribute(self::FIELD_COMMENTS, $comments);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope invoices by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_DUE,
            self::STATUS_PAID,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid invoice status '%s'", $status));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope paid invoices.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopePaid($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_PAID);
    }

    /**
     * Scope due invoices.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeDue($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_DUE);
    }

    /**
     * Scope invoices by user Id.
     *
     * @param $query
     * @param int $userId
     * @return mixed
     */
    public function scopeOfUser($query, int $userId)
    {
        return $this->where(self::FIELD_USER_ID, $userId);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The user that the invoice belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            self::FIELD_USER_ID,
            User::FIELD_PK
        );
    }

    /**
     * The order that the invoice belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(
            Order::class,
            Order::FIELD_PK,
            self::FIELD_ORDER_ID
        );
    }

    /**
     * The items that belong to the invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(
            InvoiceItem::class,
            InvoiceItem::FIELD_INVOICE_ID,
            self::FIELD_PK
        );
    }

    /**
     * The abuse reports that belongs to the invoice.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function abuseReports()
    {
        return $this->morphMany(
            Support::class,
            Support::KEYWORD_MORPH_ABUSABLE
        );
    }
}
