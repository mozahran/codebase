<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceItem extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'invoice_items';

    const FIELD_PK = 'id';
    const FIELD_INVOICE_ID = 'invoice_id';
    const FIELD_INVOICEABLE_ID = 'invoiceable_id';
    const FIELD_INVOICEABLE_TYPE = 'invoiceable_type';
    const FIELD_NAME = 'name';
    const FIELD_UNIT_PRICE = 'unit_price';
    const FIELD_QTY = 'qty';
    const FIELD_DISCOUNT = 'discount';
    const FIELD_TAX = 'tax';
    const FIELD_SUBTOTAL = 'subtotal';
    const FIELD_COMMENTS = 'comments';

    const MORPH_INVOICEABLE = 'invoiceable';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_INVOICE_ID,
        self::FIELD_INVOICEABLE_ID,
        self::FIELD_INVOICEABLE_TYPE,
        self::FIELD_UNIT_PRICE,
        self::FIELD_QTY,
        self::FIELD_DISCOUNT,
        self::FIELD_TAX,
        self::FIELD_SUBTOTAL,
        self::FIELD_COMMENTS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_INVOICE_ID => 'integer',
        self::FIELD_INVOICEABLE_ID => 'integer',
        self::FIELD_UNIT_PRICE => 'decimal',
        self::FIELD_QTY => 'integer',
        self::FIELD_DISCOUNT => 'decimal',
        self::FIELD_TAX => 'decimal',
        self::FIELD_SUBTOTAL => 'decimal',
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the item Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the invoice Id.
     *
     * @return int
     */
    public function getInvoiceId() : int
    {
        return $this->getAttribute(self::FIELD_INVOICE_ID);
    }

    /**
     * Get the unit price of the item.
     *
     * @return float
     */
    public function getUnitPrice() : float
    {
        return $this->getAttribute(self::FIELD_UNIT_PRICE);
    }

    /**
     * Get the quantity of the item.
     *
     * @return int
     */
    public function getQty() : float
    {
        return $this->getAttribute(self::FIELD_QTY);
    }

    /**
     * Get the discount for the item.
     *
     * @return float
     */
    public function getDiscount() : float
    {
        return $this->getAttribute(self::FIELD_DISCOUNT);
    }

    /**
     * Get the tax for the item.
     *
     * @return float
     */
    public function getTax() : float
    {
        return $this->getAttribute(self::FIELD_TAX);
    }

    /**
     * Get the subtotal of the item (qty x unit price).
     *
     * @return float
     */
    public function getSubtotal() : float
    {
        return $this->getAttribute(self::FIELD_SUBTOTAL);
    }

    /**
     * Get the comments for the item.
     *
     * @return string
     */
    public function getComments() : string
    {
        return $this->getAttribute(self::FIELD_COMMENTS);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the invoice Id.
     *
     * @param int $id
     * @return $this
     */
    public function setInvoiceId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_INVOICE_ID, $id);
    }

    /**
     * Set the unit price of the item.
     *
     * @param float $price
     * @return $this
     */
    public function setUnitPrice(float $price) : self
    {
        return $this->setAttribute(self::FIELD_UNIT_PRICE, $price);
    }

    /**
     * Set the quantity of the item.
     *
     * @param int $n
     * @return $this
     */
    public function setQty(int $n) : self
    {
        return $this->setAttribute(self::FIELD_QTY, $n);
    }

    /**
     * Set the discount of the item.
     *
     * @param float $amount
     * @return $this
     */
    public function setDiscount(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_DISCOUNT, $amount);
    }

    /**
     * Set the tax for the item.
     *
     * @param float $amount
     * @return $this
     */
    public function setTax(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_TAX, $amount);
    }

    /**
     * Set the subtotal of the item.
     *
     * @param float $subtotal
     * @return $this
     */
    public function setSubtotal(float $subtotal) : self
    {
        return $this->setAttribute(self::FIELD_SUBTOTAL, $subtotal);
    }

    /**
     * Set the comments for the item.
     *
     * @param string $text
     * @return $this
     */
    public function setComments(string $text) : self
    {
        return $this->setAttribute(self::FIELD_COMMENTS, $text);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope items by invoice Id.
     *
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeOfInvoice($query, int $id)
    {
        return $query->where(self::FIELD_INVOICE_ID, $id);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The invoice that the item belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function invoice()
    {
        return $this->belongsTo(Invoice::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function invoiceable()
    {
        return $this->morphTo();
    }
}