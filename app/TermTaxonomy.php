<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermTaxonomy extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'term_taxonomy';

    const FIELD_PK = 'id';
    const FIELD_TERM_ID = 'term_id';
    const FIELD_PARENT_ID = 'parent_id';
    const FIELD_TAXONOMY = 'taxonomy';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_COUNT = 'count';

    protected $table = self::TABLE_NAME;

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    public function getTermId() : int
    {
        return $this->getAttribute(self::FIELD_TERM_ID);
    }

    /**
     * It's used to make a category as a subordinate to another parent category.
     */
    public function getParentId() : int
    {
        return $this->getAttribute(self::FIELD_PARENT_ID);
    }

    /**
     * category
     * tag
     * link category
     */
    public function getTaxonomy() : string
    {
        return $this->getAttribute(self::FIELD_TAXONOMY);
    }

    /**
     * It can be any additional information just like a category description.
     */
    public function getDescription() : ?string
    {
        return $this->getAttribute(self::FIELD_DESCRIPTION);
    }

    /**
     * It's used to count the number of uses of the tag.
     */
    public function getCount() : int
    {
        return $this->getAttribute(self::FIELD_COUNT);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    public function setTermId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_TERM_ID, $id);
    }

    public function setParentId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_PARENT_ID, $id);
    }

    public function setTaxonomy(string $taxonomy) : self
    {
        return $this->setAttribute(self::FIELD_TAXONOMY, $taxonomy);
    }

    public function setDescription(string $text) : self
    {
        return $this->setAttribute(self::FIELD_DESCRIPTION, $text);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    public function term()
    {
        return $this->belongsTo(
            Term::class,
            self::FIELD_TERM_ID,
            Term::FIELD_PK
        );
    }

    public function termRelationship()
    {
        return $this->hasOne(
            TermRelationship::class,
            TermRelationship::FIELD_TERM_TAXONOMY_ID,
            self::FIELD_PK
        );
    }
}
