<?php

namespace App;

use App\Events\PostCreated;
use App\Events\PostDeleted;
use App\Events\PostUpdated;
use App\Exceptions\InvalidTypeException;
use App\Traits\HasStatus;
use App\Traits\BelongsToUser;
use App\Traits\Statuses\HasDraftStatus;
use App\Traits\Statuses\HasPendingStatus;
use App\Traits\Statuses\HasPublishedStatus;
use App\Traits\Taxonomable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Taxonomable,
        BelongsToUser,
        HasStatus,
        HasPendingStatus,
        HasDraftStatus,
        HasPublishedStatus;

    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'posts';

    const FIELD_PK = 'id';
    const FIELD_CATEGORY_ID = 'category_id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_TITLE = 'title';
    const FIELD_CONTENT = 'content';
    const FIELD_TYPE = 'type';
    const FIELD_STATUS = 'status';

    const TYPE_ALL = null;
    const TYPE_POST = 'post';
    const TYPE_PAGE = 'page';

    protected $fillable = [
        self::FIELD_CATEGORY_ID,
        self::FIELD_USER_ID,
        self::FIELD_TITLE,
        self::FIELD_CONTENT,
        self::FIELD_TYPE,
        self::FIELD_STATUS,
    ];

    protected $casts = [
        self::FIELD_CATEGORY_ID => 'integer',
        self::FIELD_USER_ID => 'integer',
    ];

    protected $dispatchesEvents = [
        'created' => PostCreated::class,
        'updated' => PostUpdated::class,
        'deleted' => PostDeleted::class,
    ];

    protected $with = [
        'meta'
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    public function getCategoryId() : int
    {
        return $this->getAttribute(self::FIELD_CATEGORY_ID);
    }

    public function getTitle() : string
    {
        return $this->getAttribute(self::FIELD_TITLE);
    }

    public function getContent() : string
    {
        return $this->getAttribute(self::FIELD_CONTENT);
    }

    public function getType() : string
    {
        return $this->getAttribute(self::FIELD_TYPE);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    public function setCategoryId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_CATEGORY_ID, $id);
    }

    public function setTitle(string $title) : self
    {
        return $this->setAttribute(self::FIELD_TITLE, $title);
    }

    public function setContent(string $content) : self
    {
        return $this->setAttribute(self::FIELD_CONTENT, nl2br($content));
    }

    public function setType(string $type) : self
    {
        return $this->setAttribute(self::FIELD_TYPE, $type);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    public function scopeOfCategory($query, int $id)
    {
        return $query->where(self::FIELD_CATEGORY_ID, $id);
    }

    public function scopeOfType($query, string $type)
    {
        if ($type === self::TYPE_ALL) {
            return $query;
        }

        if ( ! in_array($type, [
            self::TYPE_POST,
            self::TYPE_PAGE,
        ])) {
            throw new InvalidTypeException(sprintf("Invalid post type '%s'", $type));
        }

        return $query->where(self::FIELD_TYPE, $type);
    }

    /**
     * Scope posts of type post.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidTypeException
     */
    public function scopeOfTypePost($query)
    {
        return $this->scopeOfType($query, self::TYPE_POST);
    }

    /**
     * Scope posts of type page.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidTypeException
     */
    public function scopeOfTypePage($query)
    {
        return $this->scopeOfType($query, self::TYPE_PAGE);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    public function image()
    {
        return $this
            ->morphOne(Image::class, Image::FIELD_MORPHS)
            ->where(Image::FIELD_IS_DEFAULT, Image::DEFAULT)
            ->withDefault([
                Image::FIELD_IMAGE_URL => get_default_image(),
                Image::FIELD_THUMB_URL => get_default_image(),
            ]);
    }

    public function images()
    {
        return $this
            ->morphMany(Image::class, Image::FIELD_MORPHS)
            ->where(Image::FIELD_IS_DEFAULT, Image::NOT_DEFAULT);
    }

    public function meta()
    {
        return $this->hasMany(
            PostMeta::class,
            PostMeta::FIELD_POST_ID,
            self::FIELD_PK
        );
    }

    /**
     * The category that the post belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(
            Category::class,
            self::FIELD_CATEGORY_ID,
            self::FIELD_PK
        );
    }

    /**
     * The abuse reports that belongs to the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function abuseReports()
    {
        return $this->morphMany(
            Support::class,
            Support::KEYWORD_MORPH_ABUSABLE
        );
    }
}
