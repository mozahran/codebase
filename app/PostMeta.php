<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PostMeta extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'post_meta';

    const FIELD_PK = 'id';
    const FIELD_POST_ID = 'post_id';
    const FIELD_KEY = 'key';
    const FIELD_VALUE = 'value';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_POST_ID,
        self::FIELD_KEY,
        self::FIELD_VALUE,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_POST_ID => 'integer',
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the post Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the post Id.
     *
     * @return int
     */
    public function getPostId() : int
    {
        return $this->getAttribute(self::FIELD_POST_ID);
    }

    /**
     * Get the meta key.
     *
     * @return string
     */
    public function getKey() : string
    {
        return $this->getAttribute(self::FIELD_KEY);
    }

    /**
     * Get the meta value.
     *
     * @return string
     */
    public function getValue() : string
    {
        return $this->getAttribute(self::FIELD_VALUE);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the post Id.
     *
     * @param int $id
     * @return $this
     */
    public function setPostId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_POST_ID, $id);
    }

    /**
     * Set the meta key.
     *
     * @param string $key
     * @return $this
     */
    public function setKey(string $key) : self
    {
        return $this->setAttribute(self::FIELD_KEY, $key);
    }

    /**
     * Set the meta value.
     *
     * @param string $value
     * @return $this
     */
    public function setValue(string $value) : self
    {
        return $this->setAttribute(self::FIELD_VALUE, $value);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope meta data by post.
     *
     * @param $query
     * @param int $postId
     * @return mixed
     */
    public function scopeOfPost($query, int $postId)
    {
        return $query->where(self::FIELD_POST_ID, $postId);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The post that the meta data belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(
            Post::class,
            Post::FIELD_PK,
            self::FIELD_POST_ID
        );
    }
}
