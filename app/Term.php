<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Term extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'terms';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';

    public $timestamps = false;

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    public function taxonomies()
    {
        return $this->hasMany(
            TermTaxonomy::class,
            TermTaxonomy::FIELD_TERM_ID,
            self::FIELD_PK
        );
    }
}
