<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasContent
{
    public function getContent() : string
    {
        return $this->getAttribute(Fields::CONTENT);
    }

    public function getContentExcerpt($limit = 100, $end = '...')
    {
        return str_limit($this->getContent(),$limit, $end);
    }

    /**
     * @return self
     */
    public function setContent(string $content)
    {
        return $this->setAttribute(Fields::CONTENT, $content);
    }
}