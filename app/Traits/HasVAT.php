<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasVAT
{
    public function getVAT() : float
    {
        return $this->getAttribute(Fields::VAT);
    }

    /**
     * @return self
     */
    public function setVAT(float $rate)
    {
        return $this->setAttribute(Fields::VAT, $rate);
    }
}