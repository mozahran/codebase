<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;
use App\Category;

trait BelongsToCategory
{
    public function getCategoryId() : int
    {
        return $this->getAttribute(Fields::CATEGORY_ID);
    }

    /**
     * @return self
     */
    public function setCategoryId(int $id)
    {
        return $this->setAttribute(Fields::CATEGORY_ID, $id);
    }

    public function scopeOfCategory($query, int $id)
    {
        return $query->where(Fields::CATEGORY_ID, $id);
    }

    public function category()
    {
        return $this->belongsTo(
            Category::class,
            Fields::CATEGORY_ID,
            Fields::PK
        );
    }
}