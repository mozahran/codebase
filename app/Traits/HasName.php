<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasName
{
    public function getName() : string
    {
        return $this->getAttribute(Fields::NAME);
    }

    /**
     * @return self
     */
    public function setName(string $name)
    {
        return $this->setAttribute(Fields::NAME, $name);
    }
}