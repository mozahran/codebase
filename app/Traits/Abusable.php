<?php

namespace App\Traits;

use App\Support;

trait Abusable
{
    public function abuseReports()
    {
        return $this->morphMany(
            Support::class,
            Support::KEYWORD_MORPH_ABUSABLE
        );
    }
}