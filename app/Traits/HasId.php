<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasId
{
    public function getId() : int
    {
        return $this->getAttribute(Fields::PK);
    }

    /**
     * @return self
     */
    public function setId(int $id)
    {
        return $this->setAttribute(Fields::PK, $id);
    }
}