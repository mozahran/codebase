<?php

namespace App\Traits;

use App\Image;
use App\Codebase\Constants\Fields;

trait HasImageRelation
{
    public function image()
    {
        return $this
            ->morphOne(Image::class, Image::FIELD_MORPHS)
            ->where(Fields::IS_DEFAULT, Image::DEFAULT)
            ->withDefault([
                Image::FIELD_IMAGE_URL => get_default_image(),
                Image::FIELD_THUMB_URL => get_default_image(),
            ]);
    }

    public function images()
    {
        return $this
            ->morphMany(Image::class, Image::FIELD_MORPHS)
            ->where(Fields::IS_DEFAULT, Image::NOT_DEFAULT);
    }
}