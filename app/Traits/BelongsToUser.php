<?php

namespace App\Traits;

use App\User;
use App\Codebase\Constants\Fields;

trait BelongsToUser
{
    public function getUserId() : int
    {
        return $this->getAttribute(Fields::USER_ID);
    }

    /**
     * @return self
     */
    public function setUserId(int $id)
    {
        return $this->setAttribute(Fields::USER_ID, $id);
    }

    public function scopeForUser($query, int $id)
    {
        return $query->where(Fields::USER_ID, $id);
    }

    public function user()
    {
        return $this->belongsTo(
            User::class,
            Fields::USER_ID,
            Fields::PK
        );
    }
}