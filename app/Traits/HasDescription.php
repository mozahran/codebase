<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasDescription
{
    public function getDescription() : string
    {
        return $this->getAttribute(Fields::DESCRIPTION);
    }

    public function getDescriptionExcerpt($limit = 100, $end = '...')
    {
        return str_limit($this->getDescription(),$limit, $end);
    }

    public function setDescription(string $text)
    {
        return $this->setAttribute(Fields::DESCRIPTION, $text);
    }
}