<?php

namespace App\Traits\Statuses;

use App\Codebase\Constants\Statuses;

trait HasDraftStatus
{
    public function isDraft() : bool
    {
        return strtolower($this->getStatus()) === Statuses::DRAFT;
    }

    /**
     * @return self
     */
    public function setDraft()
    {
        return $this->setStatus(Statuses::DRAFT);
    }

    public function scopeDraft($query)
    {
        return $this->scopeOfStatus($query, Statuses::DRAFT);
    }
}