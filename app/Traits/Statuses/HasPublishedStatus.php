<?php

namespace App\Traits\Statuses;

use App\Codebase\Constants\Statuses;

trait HasPublishedStatus
{
    public function isPublished() : bool
    {
        return strtolower($this->getStatus()) === Statuses::PUBLISHED;
    }

    /**
     * @return self
     */
    public function setPublished()
    {
        return $this->setStatus(Statuses::PUBLISHED);
    }

    public function scopePublished($query)
    {
        return $this->scopeOfStatus($query, Statuses::PUBLISHED);
    }
}