<?php

namespace App\Traits\Statuses;

use App\Codebase\Constants\Statuses;

trait HasPendingStatus
{
    public function isPending() : bool
    {
        return strtolower($this->getStatus()) === Statuses::PENDING;
    }

    /**
     * @return self
     */
    public function setPending()
    {
        return $this->setStatus(Statuses::PENDING);
    }

    public function scopePending($query)
    {
        return $this->scopeOfStatus($query, Statuses::PENDING);
    }
}