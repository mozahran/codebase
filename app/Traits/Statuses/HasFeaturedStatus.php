<?php

namespace App\Traits\Statuses;

use App\Codebase\Constants\Statuses;

trait HasFeaturedStatus
{
    public function isFeatured() : bool
    {
        return strtolower($this->getStatus()) === Statuses::FEATURED;
    }

    /**
     * @return self
     */
    public function setFeatured()
    {
        return $this->setStatus(Statuses::FEATURED);
    }

    public function scopeFeatured($query)
    {
        return $this->scopeOfStatus($query, Statuses::FEATURED);
    }
}