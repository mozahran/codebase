<?php

namespace App\Traits;

use App\TermRelationship;

trait Taxonomable
{
    public function taxonomies()
    {
        return $this
            ->morphMany(TermRelationship::class, 'object')
            ->with('termTaxonomy.term');
    }
}