<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;
use App\Codebase\Constants\Statuses;

trait HasStatus
{
    public function getStatus() : string
    {
        return $this->getAttribute(Fields::STATUS);
    }

    /**
     * @return self
     */
    public function setStatus(string $status)
    {
        return $this->setAttribute(Fields::STATUS, $status);
    }

    public function scopeOfStatus($query, string $status)
    {
        if ($status === Statuses::ALL) {
            return $query;
        }

        return $query->where(Fields::STATUS, $status);
    }
}