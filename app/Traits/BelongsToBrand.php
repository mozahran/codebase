<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;
use App\Brand;

trait BelongsToBrand
{
    public function getBrandId() : int
    {
        return $this->getAttribute(Fields::BRAND_ID);
    }

    public function setBrandId(int $id)
    {
        return $this->setAttribute(Fields::BRAND_ID, $id);
    }

    public function scopeOfBrand($query, int $id)
    {
        return $query->where(Fields::BRAND_ID, $id);
    }

    public function brand()
    {
        return $this->belongsTo(
            Brand::class,
            Fields::BRAND_ID,
            Fields::PK
        );
    }
}