<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasPrice
{
    public function getPrice() : float
    {
        return $this->getAttribute(Fields::PRICE);
    }

    /**
     * @return self
     */
    public function setPrice(float $price)
    {
        return $this->setAttribute(Fields::PRICE, $price);
    }

    public function scopeInPriceRange($query, float $from, float $to)
    {
        return $query->whereBetween(Fields::PRICE, [
            $from,
            $to
        ]);
    }
}