<?php

namespace App\Traits;

use App\Codebase\Constants\Fields;

trait HasQuantity
{
    public function getQty() : int
    {
        return $this->getAttribute(Fields::QUANTITY);
    }

    public function getQuantity() : int
    {
        return $this->getQty();
    }

    /**
     * @return self
     */
    public function setQty(int $n)
    {
        return $this->setAttribute(Fields::QUANTITY, $n);
    }

    /**
     * @return self
     */
    public function setQuantity(int $n)
    {
        return $this->setQty($n);
    }
}