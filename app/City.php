<?php

namespace App;

use App\Events\CityCreated;
use App\Events\CityDeleted;
use App\Events\CityUpdated;
use App\Exceptions\InvalidStatusException;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'cities';

    const FIELD_PK = 'id';
    const FIELD_COUNTRY_ID = 'country_id';
    const FIELD_NAME = 'name';
    const FIELD_STATUS = 'status';

    const STATUS_ALL = null;
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_COUNTRY_ID,
        self::FIELD_NAME,
        self::FIELD_STATUS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_COUNTRY_ID => 'integer',
        self::FIELD_STATUS => 'boolean',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => CityCreated::class,
        'updated' => CityUpdated::class,
        'deleted' => CityDeleted::class,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'country',
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the city Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the city Id.
     *
     * @return int
     */
    public function getCountryId() : int
    {
        return $this->getAttribute(self::FIELD_COUNTRY_ID);
    }

    /**
     * Get the city name.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the status of the city.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the city is enabled.
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_ENABLED;
    }

    /**
     * Check if the city is disabled.
     *
     * @return bool
     */
    public function isDisabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_DISABLED;
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the country Id.
     *
     * @param int $id
     * @return $this
     */
    public function setCountryId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_COUNTRY_ID, $id);
    }

    /**
     * Set the city name.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the status of the city.
     *
     * @param string $status
     *
     * @return $this
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setStatus(string $status) : self
    {
        if ( ! in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED
        ])) {
            throw new InvalidStatusException(sprintf("Invalid city status '%s'"));
        }

        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the status as enabled.
     *
     * @return \App\City
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setEnabled() : self
    {
        return $this->setStatus(self::STATUS_ENABLED);
    }

    /**
     * Set the status as disabled.
     *
     * @return $this
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setDisabled() : self
    {
        return $this->setStatus(self::STATUS_DISABLED);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope cities by country id.
     *
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeOfCountry($query, int $id)
    {
        return $query->where(self::FIELD_COUNTRY_ID, $id);
    }

    /**
     * Scope cities by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED
        ])) {
            throw new InvalidStatusException(sprintf("Invalid city status '%s'"));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope enabled cities.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeEnabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    /**
     * Scope disabled cities.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeDisabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The country that the city belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(
            Country::class,
            self::FIELD_COUNTRY_ID,
            Country::FIELD_PK
        );
    }

    /**
     * The users that belong to the city.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(
            User::class,
            User::FIELD_COUNTRY_ID,
            self::FIELD_PK
        );
    }
}
