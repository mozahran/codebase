<?php

namespace App;

use App\Events\TaxRateCreated;
use App\Events\TaxRateDeleted;
use App\Events\TaxRateUpdated;
use Illuminate\Database\Eloquent\Model;

class TaxRate extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'tax_rates';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_COST = 'cost';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_COST,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_COST => 'decimal',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => TaxRateCreated::class,
        'updated' => TaxRateUpdated::class,
        'deleted' => TaxRateDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the tax rate id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(static::FIELD_PK);
    }

    /**
     * Get the name of the tax rate.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(static::FIELD_PK);
    }

    /**
     * Get the description of the tax rate.
     *
     * @return string
     */
    public function getDescription() : string
    {
        return $this->getAttribute(static::FIELD_DESCRIPTION);
    }

    /**
     * Get the cost of the tax rate.
     *
     * @return float
     */
    public function getCost() : float
    {
        return $this->getAttribute(static::FIELD_COST);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the name of the tax rate.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(static::FIELD_PK, $name);
    }

    /**
     * Set the description of the tax rate.
     *
     * @param string $text
     * @return $this
     */
    public function setDescription(string $text) : self
    {
        return $this->setAttribute(static::FIELD_DESCRIPTION, $text);
    }

    /**
     * Set the cost of the tax rate.
     *
     * @param float $cost
     * @return $this
     */
    public function setCost(float $cost) : self
    {
        return $this->setAttribute(static::FIELD_COST, $cost);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

}
