<?php

if ( ! function_exists('html_rate_icons')) {

    /**
     * Generate HTML rate icons.
     *
     * @param float $rate
     *
     * @return string
     */
    function html_rate_icons(float $rate)
    {
        $html = '';

        $flooredRate = floor($rate);
        $hasHalfStar = $flooredRate < $rate;
        $emptyStars = 5 - round($rate);

        if ($rate > 0) {
            foreach (range(1, $flooredRate) as $star) {
                $html .= '<i class="fa fa-star text-warning"></i>';
            }
            if ($hasHalfStar) {
                $html .= '<i class="fa fa-star-half-o fa-flip-horizontal text-warning"></i>';
            }
        }

        if ($emptyStars > 0) {
            foreach (range(1, $emptyStars) as $star) {
                $html .= '<i class="fa fa-star-o text-warning" ></i >';
            }
        }

        return $html;
    }
}

if ( ! function_exists('public_asset')) {

    /**
     * Get a link to a public asset.
     *
     * @param string    $path
     * @param bool|null $secure
     *
     * @return string
     */
    function public_asset(string $path, bool $secure = null)
    {
        return asset($path, $secure);
    }
}

if ( ! function_exists('set_title')) {

    /**
     * Set the page title.
     *
     * @param string $pageTitle
     */
    function set_title(string $pageTitle)
    {
        view()->share('pageTitle', $pageTitle);
    }
}

if ( ! function_exists('set_search_action')) {

    /**
     * Set the search action for the dashboard.
     *
     * @param string $route
     */
    function set_search_action(string $route)
    {
        view()->share('searchAction', $route);
    }
}

if ( ! function_exists('enable_search_bar')) {

    /**
     * Enable the search bar in the dashboard.
     *
     * @param bool $status
     */
    function enable_search_bar(bool $status)
    {
        view()->share('enableSearchBar', $status);
    }
}

if ( ! function_exists('enable_edit_mode')) {

    /**
     * Enable edit mode for create-edit views.
     *
     * @param bool $status
     */
    function enable_edit_mode(bool $status)
    {
        view()->share('editMode', $status);
    }
}

if ( ! function_exists('redirect_success')) {

    /**
     * Redirect for success.
     *
     * @param string $route
     * @param string $response
     *
     * @return mixed
     */
    function redirect_success(string $route, string $response)
    {
        return redirect()
            ->to($route)
            ->withStatus($response)
            ->withToastr('success');
    }
}

if ( ! function_exists('redirect_fail')) {

    /**
     * Redirect for failures.
     *
     * @param string $route
     * @param string $response
     *
     * @return mixed
     */
    function redirect_fail(string $route, string $response)
    {
        return redirect()
            ->to($route)
            ->withStatus($response)
            ->withToastr('error');
    }
}

if ( ! function_exists('redirect_with_errors')) {

    /**
     * Redirect with errors.
     *
     * @param        $validator
     * @param string $response
     *
     * @return mixed
     */
    function redirect_with_errors($validator, string $response)
    {
        return redirect()
            ->back()
            ->withErrors($validator)
            ->withStatus($response)
            ->withToastr('error');
    }
}

if ( ! function_exists('api_response')) {

    /**
     * API json response.
     *
     * @param bool       $status
     * @param string     $response
     * @param array|null $data
     *
     * @return array
     */
    function api_response(bool $status, string $response, array $data = null)
    {
        return compact('status', 'response', 'data');
    }
}

if ( ! function_exists('api_response_success')) {

    /**
     * API json response for success.
     *
     * @param string     $response
     * @param array|null $data
     *
     * @return array
     */
    function api_response_success(string $response, array $data = null)
    {
        $status = true;

        return compact('status', 'response', 'data');
    }
}

if ( ! function_exists('api_response_fail')) {

    /**
     * API json response for failure.
     *
     * @param string $response
     * @param array  $data
     *
     * @return array
     */
    function api_response_fail(string $response, array $data = [])
    {
        $status = false;

        return api_response($status, $response, $data);
    }
}

if ( ! function_exists('api_response_error')) {

    /**
     * API json response for errors.
     *
     * @param string          $response
     * @param \Exception|null $exception
     *
     * @return array
     */
    function api_response_error(string $response, Exception $exception = null)
    {
        $status = false;

        $data = env('APP_DEBUG') && $exception instanceof Exception ? [
            'error' => $exception->getMessage(),
        ] : [];

        return api_response($status, $response, $data);
    }
}

if ( ! function_exists('get_settings')) {

    /**
     * Get the settings array or an option's value.
     *
     * @param string|null $optionName
     * @param mixed|null  $default
     *
     * @return array|mixed
     */
    function get_settings(string $optionName = null, mixed $default = null)
    {
        if ($optionName !== null) {
            $value = optional(\App\Option::whereName($optionName)->first())->value;

            if ($value == null) {
                return $default;
            }

            return $value;
        }

        $settings = \App\Option::all();

        $settingsArray = [];

        collect($settings)->map(function ($option) use (&$settingsArray) {
            $settingsArray[$option->name] = $option->value;
        });

        return $settingsArray;
    }
}

if ( ! function_exists('get_default_image')) {

    /**
     * Get the link to the default image.
     *
     * @return string
     */
    function get_default_image()
    {
        return public_asset('img/no-image.png');
    }
}

if ( ! function_exists('get_api_token')) {

    /**
     * Get the API token of the currently logged-in user.
     *
     * @return null|string
     */
    function get_api_token()
    {
        $user = request()->user();

        if ($user) {
            return $user->getApiToken();
        }

        return null;
    }
}

if ( ! function_exists('allowed_results_pagination')) {
    /**
     * Get an array of the allowed numbers of results to be fetched from database in a single query.
     *
     * @return array
     */
    function allowed_results_pagination()
    {
        return [15, 25, 50, 100];
    }
}

if ( ! function_exists('get_per_page')) {
    /**
     * Get the value of per_page from the URI.
     *
     * @param int $default
     *
     * @return int
     */
    function get_per_page(int $default = 25) : int
    {
        $perPage = (int) \request(KEYWORD_PER_PAGE, $default);

        return in_array($perPage, allowed_results_pagination()) ? $perPage : $default;
    }
}

if ( ! function_exists('get_current_page')) {
    /**
     * Get the current page from the URI.
     *
     * @return int
     */
    function get_current_page() : int
    {
        return (int) \request(KEYWORD_CURRENT_PAGE, 0);
    }
}

if ( ! function_exists('calculate_page_offset')) {
    /**
     * Calculate the page offset.
     *
     * @param int $perPage
     * @param int $currentPage
     *
     * @return int
     */
    function calculate_page_offset(int $perPage, int $currentPage) : int
    {
        return $perPage * ($currentPage - 1);
    }
}

if ( ! function_exists('get_search_query')) {
    /**
     * Get the search query from the URI.
     *
     * @return null|string
     */
    function get_search_query() : ?string
    {
        return \request(KEYWORD_SEARCH_QUERY);
    }
}

if ( ! function_exists('get_cache_default_time')) {
    function get_cache_default_time()
    {
        return 5;
    }
}