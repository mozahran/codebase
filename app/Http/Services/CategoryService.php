<?php

namespace App\Services;

use App\Http\Requests\CategoryRequest;
use App\Repositories\CategoryRepository;

class CategoryService implements ServiceInterface
{
    /**
     * @var \App\Repositories\CategoryRepository
     */
    private $repository;

    /**
     * CategoryService constructor.
     *
     * @param \App\Repositories\CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\CategoryRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new category.
     *
     * @param CategoryRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(CategoryRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific category.
     *
     * @param CategoryRequest $request
     * @param int $categoryId
     * @return bool
     */
    public function update(CategoryRequest $request, int $categoryId)
    {
        return $this->repository->update($request->all(), $categoryId);
    }
}