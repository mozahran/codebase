<?php

namespace App\Services;

use App\Http\Requests\ShippingMethodRequest;
use App\Repositories\ShippingMethodRepository;

class ShippingMethodService implements ServiceInterface
{
    /**
     * @var \App\Repositories\ShippingMethodRepository
     */
    private $repository;

    /**
     * ShippingMethodService constructor.
     *
     * @param \App\Repositories\ShippingMethodRepository $repository
     */
    public function __construct(ShippingMethodRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\ShippingMethodRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new shipping method.
     *
     * @param ShippingMethodRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(ShippingMethodRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific shipping method.
     *
     * @param ShippingMethodRequest $request
     * @param int $shippingMethodId
     * @return bool
     */
    public function update(ShippingMethodRequest $request, int $shippingMethodId)
    {
        return $this->repository->update($request->all(), $shippingMethodId);
    }
}