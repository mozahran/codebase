<?php

namespace App\Services;

interface ServiceInterface
{
    /**
     * An instance of the repository.
     */
    public function repository();
}