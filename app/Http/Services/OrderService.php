<?php

namespace App\Services;

use App\Http\Requests\OrderRequest;
use App\OrderItem;
use App\Repositories\OrderRepository;
use Illuminate\Database\Eloquent\Model;

class OrderService implements ServiceInterface
{
    /**
     * @var \App\Repositories\OrderRepository
     */
    private $repository;

    /**
     * OrderService constructor.
     *
     * @param \App\Repositories\OrderRepository $repository
     */
    public function __construct(OrderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\OrderRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new order.
     *
     * @param OrderRequest $request
     * @return bool
     */
    public function create(OrderRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific order.
     *
     * @param OrderRequest $request
     * @param int $orderId
     * @return bool
     */
    public function update(OrderRequest $request, int $orderId)
    {
        return $this->repository->update($request->all(), $orderId);
    }
}