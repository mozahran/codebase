<?php

namespace App\Services;

use App\Http\Requests\CouponRequest;
use App\Repositories\CouponRepository;
use App\Repositories\BaseBaseRepositoryInterface;

class CouponService implements ServiceInterface
{
    /**
     * @var \App\Repositories\CouponRepository
     */
    private $repository;

    /**
     * CouponService constructor.
     *
     * @param \App\Repositories\CouponRepository $repository
     */
    public function __construct(CouponRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\CouponRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new coupon.
     *
     * @param CouponRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(CouponRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific coupon.
     *
     * @param CouponRequest $request
     * @param int $couponId
     * @return bool
     */
    public function update(CouponRequest $request, int $couponId)
    {
        return $this->repository->update($request->all(), $couponId);
    }
}