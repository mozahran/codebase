<?php

namespace App\Services;

use App\Http\Requests\UserRequest;
use App\Repositories\UserRepository;

class UserService implements ServiceInterface
{
    /**
     * @var \App\Repositories\UserRepository
     */
    private $repository;

    /**
     * UserService constructor.
     *
     * @param \App\Repositories\UserRepository $repository
     */
    public function __construct(UserRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new user.
     *
     * @param UserRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(UserRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific user.
     *
     * @param UserRequest $request
     * @param int $userId
     * @return bool
     */
    public function update(UserRequest $request, int $userId)
    {
        return $this->repository->update($request->all(), $userId);
    }
}