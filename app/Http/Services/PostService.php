<?php

namespace App\Services;

use App\Http\Requests\PostRequest;
use App\Repositories\PostRepository;

class PostService implements ServiceInterface
{
    /**
     * @var \App\Repositories\PostRepository
     */
    private $repository;

    /**
     * PostService constructor.
     *
     * @param \App\Repositories\PostRepository $repository
     */
    public function __construct(PostRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\PostRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new post.
     *
     * @param PostRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(PostRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific post.
     *
     * @param PostRequest $request
     * @param int $postId
     * @return bool
     */
    public function update(PostRequest $request, int $postId)
    {
        return $this->repository->update($request->all(), $postId);
    }
}