<?php

namespace App\Services;

use App\Http\Requests\SupportRequest;
use App\Repositories\SupportRepository;

class SupportService implements ServiceInterface
{
    /**
     * @var \App\Repositories\SupportRepository
     */
    private $repository;

    /**
     * SupportService constructor.
     *
     * @param \App\Repositories\SupportRepository $repository
     */
    public function __construct(SupportRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\SupportRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new post.
     *
     * @param SupportRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(SupportRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific post.
     *
     * @param SupportRequest $request
     * @param int $postId
     * @return bool
     */
    public function update(SupportRequest $request, int $postId)
    {
        return $this->repository->update($request->all(), $postId);
    }
}