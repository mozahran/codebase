<?php

namespace App\Services;

use App\Http\Requests\ChatReplyRequest;
use App\Repositories\Interfaces\ChatRepositoryInterface;

class ConversationService implements ServiceInterface
{
    /**
     * @var ChatRepositoryInterface
     */
    private $repository;

    /**
     * ConversationService constructor.
     *
     * @param ChatRepositoryInterface $repository
     */
    public function __construct(ChatRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return ChatRepositoryInterface
     */
    public function repository()
    {
        return $this->repository;
    }
}