<?php

namespace App\Services;

use App\Http\Requests\BrandRequest;
use App\Repositories\BrandRepository;

class BrandService implements ServiceInterface
{
    /**
     * @var \App\Repositories\BrandRepository
     */
    private $repository;

    /**
     * BrandService constructor.
     *
     * @param \App\Repositories\BrandRepository $repository
     */
    public function __construct(BrandRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\BrandRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new brand.
     *
     * @param BrandRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(BrandRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific brand.
     *
     * @param BrandRequest $request
     * @param int $brandId
     * @return bool
     */
    public function update(BrandRequest $request, int $brandId)
    {
        return $this->repository->update($request->all(), $brandId);
    }

    /**
     * Delete a specific brand.
     *
     * @param int $brandId
     * @return bool
     */
    public function delete(int $brandId)
    {
        try {
            return $this->repository->delete($brandId);
        } catch (\Exception $e) {
            exit($e->getMessage());
        }
    }
}