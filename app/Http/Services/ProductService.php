<?php

namespace App\Services;

use App\Http\Requests\ProductRequest;
use App\Repositories\ProductRepository;

class ProductService implements ServiceInterface
{
    /**
     * @var \App\Repositories\ProductRepository
     */
    private $repository;

    /**
     * ProductService constructor.
     *
     * @param \App\Repositories\ProductRepository $repository
     */
    public function __construct(ProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\ProductRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new product.
     *
     * @param ProductRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(ProductRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific product.
     *
     * @param ProductRequest $request
     * @param int $productId
     * @return bool
     */
    public function update(ProductRequest $request, int $productId)
    {
        return $this->repository->update($request->all(), $productId);
    }
}