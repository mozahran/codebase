<?php

namespace App\Services;

use App\Http\Requests\InvoiceRequest;
use App\Repositories\InvoiceRepository;

class InvoiceService implements ServiceInterface
{
    /**
     * @var \App\Repositories\InvoiceRepository
     */
    private $repository;

    /**
     * InvoiceService constructor.
     *
     * @param \App\Repositories\InvoiceRepository $repository
     */
    public function __construct(InvoiceRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\InvoiceRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new invoice.
     *
     * @param InvoiceRequest $request
     * @return bool
     */
    public function create(InvoiceRequest $request)
    {
        // Create invoice
        // Add items to the invoice.

        return true;
    }

    public function update(InvoiceRequest $request, int $invoiceId)
    {
        return $this->repository->update($request->all(), $invoiceId);
    }
}