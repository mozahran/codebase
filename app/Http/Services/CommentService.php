<?php

namespace App\Services;

use App\Http\Requests\CommentRequest;
use App\Repositories\CommentRepository;

class CommentService implements ServiceInterface
{
    /**
     * @var \App\Repositories\CommentRepository
     */
    private $repository;

    /**
     * CommentService constructor.
     *
     * @param \App\Repositories\CommentRepository $repository
     */
    public function __construct(CommentRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\CommentRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new post.
     *
     * @param CommentRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(CommentRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific post.
     *
     * @param CommentRequest $request
     * @param int $postId
     * @return bool
     */
    public function update(CommentRequest $request, int $postId)
    {
        return $this->repository->update($request->all(), $postId);
    }
}