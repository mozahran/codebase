<?php

namespace App\Services;

use App\Http\Requests\WishListRequest;
use App\Repositories\WishListRepository;

class WishListService implements ServiceInterface
{
    /**
     * @var \App\Repositories\WishListRepository
     */
    private $repository;

    /**
     * WishListService constructor.
     *
     * @param \App\Repositories\WishListRepository $repository
     */
    public function __construct(WishListRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\WishListRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new wish list.
     *
     * @param WishListRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(WishListRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific wish list.
     *
     * @param WishListRequest $request
     * @param int $wishListId
     * @return bool
     */
    public function update(WishListRequest $request, int $wishListId)
    {
        return $this->repository->update($request->all(), $wishListId);
    }
}