<?php

namespace App\Services;

use App\Http\Requests\TaxRateRequest;
use App\Repositories\TaxRateRepository;

class TaxRateService implements ServiceInterface
{
    /**
     * @var \App\Repositories\TaxRateRepository
     */
    private $repository;

    /**
     * TaxRateService constructor.
     *
     * @param \App\Repositories\TaxRateRepository $repository
     */
    public function __construct(TaxRateRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * An instance of the repository.
     *
     * @return \App\Repositories\TaxRateRepository
     */
    public function repository()
    {
        return $this->repository;
    }

    /**
     * Create a new tax rate.
     *
     * @param TaxRateRequest $request
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function create(TaxRateRequest $request)
    {
        return $this->repository->create($request->all());
    }

    /**
     * Update a specific tax rate.
     *
     * @param TaxRateRequest $request
     * @param int $taxRateId
     * @return \Illuminate\Database\Eloquent\Model
     */
    public function update(TaxRateRequest $request, int $taxRateId)
    {
        return $this->repository->create($request->all(), $taxRateId);
    }
}