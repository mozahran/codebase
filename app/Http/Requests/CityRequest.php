<?php

namespace App\Http\Requests;

use App\City;
use App\Country;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CityRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            City::FIELD_COUNTRY_ID => [
                'required',
                Rule::exists(Country::TABLE_NAME, Country::FIELD_PK)
            ],
            City::FIELD_NAME => 'required|min:3|max:120',
            City::FIELD_STATUS => [
                'required',
                Rule::in([
                    City::STATUS_ENABLED,
                    City::STATUS_DISABLED
                ])
            ],
        ];
    }
}
