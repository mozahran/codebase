<?php

namespace App\Http\Requests;

use App\ShippingMethod;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ShippingMethodRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            ShippingMethod::FIELD_NAME => 'required|min:2|max:120',
            ShippingMethod::FIELD_DESCRIPTION => 'required|min:2|max:1000',
            ShippingMethod::FIELD_PRICE => 'required|numeric',
            ShippingMethod::FIELD_STATUS => [
                'nullable',
                Rule::in([
                    ShippingMethod::STATUS_ENABLED,
                    ShippingMethod::STATUS_DISABLED,
                ]),
            ],
        ];
    }
}
