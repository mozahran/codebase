<?php

namespace App\Http\Requests;

use App\Country;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CountryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Country::FIELD_NAME => 'required|min:3|max:120',
            Country::FIELD_ISO_NAME => 'nullable|min:3|max:120',
            Country::FIELD_STATUS => [
                'required',
                Rule::in([
                    Country::STATUS_ENABLED,
                    Country::STATUS_DISABLED,
                ])
            ]
        ];
    }
}
