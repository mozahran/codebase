<?php

namespace App\Http\Requests;

use App\Coupon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CouponRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Coupon::FIELD_DESCRIPTION => 'min:2|max:500',
            Coupon::FIELD_CODE => 'required|min:4|max:10',
            Coupon::FIELD_DISCOUNT_TYPE => [
                'required',
                Rule::in([
                    Coupon::DISCOUNT_TYPE_AMOUNT_OFF_EACH_ITEM,
                    Coupon::DISCOUNT_TYPE_AMOUNT_OFF_ORDER_TOTAL,
                    Coupon::DISCOUNT_TYPE_PERCENTAGE_OFF_EACH_ITEM
                ]),
            ],
            Coupon::FIELD_DISCOUNT_AMOUNT => 'required|numeric',
            Coupon::FIELD_USES_PER_COUPON => 'numeric|min:1',
            Coupon::FIELD_USES_PER_USER => 'numeric|min:1',
            Coupon::FIELD_USAGE_COUNT => 'numeric',
            Coupon::FIELD_FREE_SHIPPING => 'boolean',
            Coupon::FIELD_PRODUCT_IDS => 'array',
            Coupon::FIELD_EXCLUDED_PRODUCT_IDS => 'array',
            Coupon::FIELD_PRODUCT_CATEGORIES => 'array',
            Coupon::FIELD_EXCLUDED_PRODUCT_CATEGORIES => 'array',
            Coupon::FIELD_STATUS => [
                'nullable',
                Rule::in([
                    Coupon::STATUS_ENABLED,
                    Coupon::STATUS_DISABLED
                ]),
            ],
            Coupon::FIELD_EXPIRES_AT => 'required|date',
        ];
    }
}
