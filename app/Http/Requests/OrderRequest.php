<?php

namespace App\Http\Requests;

use App\Codebase\Constants\Fields;
use App\Order;
use App\OrderItem;
use App\Product;
use App\ShippingMethod;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Order::FIELD_USER_ID => [
                'required',
                Rule::exists(User::class, User::FIELD_PK),
            ],
            Order::FIELD_SHIPPING_METHOD_ID => [
                'nullable',
                Rule::exists(ShippingMethod::class, ShippingMethod::FIELD_PK),
            ],
            Order::FIELD_QTY => 'required|numeric|min:1',
            Order::FIELD_TOTAL => 'required|numeric|min:0',
            'items.*.' . OrderItem::FIELD_PRODUCT_ID => [
                'required',
                Rule::exists(Product::class, Fields::PK),
            ],
            'items.*.' . OrderItem::FIELD_QTY => 'required|numeric|min:1',
            'items.*.' . OrderItem::FIELD_PRICE => 'required|numeric|min:0',
            'items.*.' . OrderItem::FIELD_SUBTOTAL => 'required|numeric|min:0',
        ];
    }
}
