<?php

namespace App\Http\Requests;

use App\Category;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Category::FIELD_PARENT_ID => [
                'nullable',
                Rule::exists(Category::TABLE_NAME, Category::FIELD_PK),
            ],
            Category::FIELD_NAME => 'required|min:2|max:200',
            Category::FIELD_SORT_ORDER => 'nullable|numeric',
            Category::FIELD_IS_ACTIVE => 'boolean',
        ];
    }
}
