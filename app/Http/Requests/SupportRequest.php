<?php

namespace App\Http\Requests;

use App\Support;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Support::FIELD_PARENT_ID => [
                'nullable',
                Rule::exists(Support::class,  Support::FIELD_PK),
            ],
            Support::FIELD_SENDER_ID => [
                'required',
                Rule::exists(User::class, User::FIELD_PK),
            ],
            Support::FIELD_ABUSER_ID => [
                'nullable',
                Rule::exists(User::class, User::FIELD_PK),
            ],
            Support::FIELD_SUBJECT => 'required|min:2|max:120',
            Support::FIELD_CONTENT => 'required|min:2|max:500',
        ];
    }
}
