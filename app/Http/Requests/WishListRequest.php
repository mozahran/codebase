<?php

namespace App\Http\Requests;

use App\Codebase\Constants\Fields;
use App\Product;
use App\User;
use App\WishListItem;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class WishListRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            WishListItem::FIELD_USER_ID => [
                'required',
                Rule::exists(User::class, User::FIELD_PK),
            ],
            WishListItem::FIELD_PRODUCT_ID => [
                'required',
                Rule::exists(Product::class, Fields::PK),
            ],
        ];
    }
}
