<?php

namespace App\Http\Requests;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CommentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Comment::FIELD_USER_ID => [
                'required',
                Rule::exists(User::TABLE_NAME, User::FIELD_PK)
            ],
            Comment::FIELD_POST_ID => [
                'required',
                Rule::exists(Post::TABLE_NAME, Post::FIELD_PK)
            ],
            Comment::FIELD_TEXT => 'required|min:2|max:500',
            Comment::FIELD_STATUS => [
                'required',
                Rule::in([
                    Comment::STATUS_PENDING,
                    Comment::STATUS_APPROVED,
                ])
            ],
        ];
    }
}
