<?php

namespace App\Http\Requests;

use App\TaxRate;
use Illuminate\Foundation\Http\FormRequest;

class TaxRateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            TaxRate::FIELD_NAME => 'required|min:2|max:120',
            TaxRate::FIELD_DESCRIPTION => 'nullable|min:2|max:500',
            TaxRate::FIELD_COST => 'required|numeric',
        ];
    }
}
