<?php

namespace App\Http\Requests;

use App\User;
use App\City;
use App\Country;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            User::FIELD_COUNTRY_ID => 'nullable|exists:' . Country::TABLE_NAME . ',' . Country::FIELD_PK,
            User::FIELD_CITY_ID => 'nullable|exists:' . City::TABLE_NAME . ',' . City::FIELD_PK,
            User::FIELD_NAME => 'required|min:2|max:50',
            User::FIELD_PHONE_NUMBER => 'nullable|min:10|max:12',
            User::FIELD_IS_ACTIVE => 'boolean',
            User::FIELD_IS_SUSPENDED => 'boolean',
        ];

        if (strtoupper($this->getMethod()) == HTTP_METHOD_POST) {
            $rules[User::FIELD_EMAIL] = 'required|min:2|max:50|unique:' . User::TABLE_NAME . ',' . User::FIELD_EMAIL;
            $rules[User::FIELD_PASSWORD] = 'required|min:6|max:18';
        } else {
            $rules[User::FIELD_EMAIL] = 'required|min:2|max:50|unique:' . User::TABLE_NAME . ',' . User::FIELD_EMAIL . ',' . $this->get('id');
            $rules[User::FIELD_PASSWORD] = 'min:6|max:18';
        }

        return $rules;
    }
}
