<?php

namespace App\Http\Requests;

use App\Brand;
use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Brand::FIELD_NAME => 'required|min:2|max:200',
            Brand::FIELD_DESCRIPTION => 'nullable|min:2|max:500',
            Brand::FIELD_IS_FEATURED => 'boolean',
        ];
    }
}
