<?php

namespace App\Http\Requests;

use App\Codebase\Constants\Fields;
use App\Codebase\Constants\Statuses;
use App\Brand;
use App\Category;
use App\Product;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Fields::CATEGORY_ID => [
                'required',
                Rule::exists(Category::class, Fields::PK),
            ],
            Fields::BRAND_ID => [
                'nullable',
                Rule::exists(Brand::class, Fields::PK),
            ],
            Fields::USER_ID => [
                'required'
            ],
            Fields::NAME => 'required|min:2|max:120',
            Fields::DESCRIPTION => 'required|min:2|max:1000',
            Fields::PRICE => 'required|numeric',
            Fields::QUANTITY => 'required|numeric|min:0',
            Fields::VAT => 'required|numeric|min:0',
            Fields::STATUS => [
                'nullable',
                Rule::in([
                    Statuses::DRAFT,
                    Statuses::PENDING,
                    Statuses::PUBLISHED,
                    Statuses::FEATURED,
                ]),
            ],
        ];
    }
}
