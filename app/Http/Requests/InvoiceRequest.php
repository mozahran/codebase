<?php

namespace App\Http\Requests;

use App\Invoice;
use App\Order;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Invoice::FIELD_USER_ID => [
                'required',
                Rule::exists(User::class, User::FIELD_PK),
            ],
            Invoice::FIELD_ORDER_ID => [
                'required',
                Rule::exists(Order::class, Order::FIELD_PK),
            ],
            Invoice::FIELD_QTY => 'required|numeric|min:1',
            Invoice::FIELD_TOTAL => 'required|numeric',
            Invoice::FIELD_STATUS => [
                'nullable',
                Rule::in([
                    Invoice::STATUS_DUE,
                    Invoice::STATUS_PAID
                ]),
            ],
            Invoice::FIELD_COMMENTS => 'nullable|min:1|max:500',
        ];
    }
}
