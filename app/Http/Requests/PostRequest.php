<?php

namespace App\Http\Requests;

use App\Category;
use App\Post;
use App\User;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            Post::FIELD_CATEGORY_ID => [
                'required',
                Rule::exists(Category::TABLE_NAME, Category::FIELD_PK),
            ],
            Post::FIELD_USER_ID => [
                'required',
                Rule::exists(User::TABLE_NAME, User::FIELD_PK),
            ],
            Post::FIELD_TITLE => 'required|min:2|max:120',
            Post::FIELD_CONTENT => 'required|min:2|max:1000',
            Post::FIELD_TYPE => [
                'nullable',
                Rule::in([
                    Post::TYPE_POST,
                    Post::TYPE_PAGE
                ]),
            ],
            Post::FIELD_STATUS => [
                'nullable',
                Rule::in([
                    Post::STATUS_PENDING,
                    Post::STATUS_DRAFT,
                    Post::STATUS_PUBLISH,
                ]),
            ],

        ];
    }
}
