<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ConversationReplyResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'sender_id' => $this->getSenderId(),
            'text' => $this->getText(),
            'created_at' => optional($this->created_at)->toDateTimeString()
        ];
    }
}
