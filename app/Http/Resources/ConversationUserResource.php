<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ConversationUserResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => optional($this->user)->getId(),
            'name' => optional($this->user)->getName(),
            'thumb_url' => optional(optional($this->user)->image)->thumb_url,
        ];
    }
}
