<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\Resource;

class ConversationResource extends Resource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->getId(),
            'creator_id' => $this->getCreatorId(),
            'users' => ConversationUserResource::collection($this->whenLoaded('users')),
            'lastReply' => new ConversationReplyResource($this->whenLoaded('lastReply')),
        ];
    }
}
