<?php

namespace App\Http\Controllers\Admin;

use App\Brand;
use Illuminate\Http\Request;
use App\Services\BrandService;
use App\Http\Controllers\Controller;
use App\Repositories\BrandRepository;
use App\Repositories\Scopes\SearchScope;
use Illuminate\Pagination\LengthAwarePaginator;
use App\Cache\Filesystem\BrandCacheRepository;

class BrandController extends Controller
{
    /**
     * @var \App\Services\BrandService
     */
    private $service;

    /**
     * @var \App\Repositories\BrandRepository
     */
    private $repository;

    /**
     * @var \App\Cache\Filesystem\BrandCacheRepository
     */
    private $cacheRepository;

    /**
     * BrandController constructor.
     *
     * @param \App\Services\BrandService                 $service
     * @param \App\Repositories\BrandRepository          $repository
     * @param \App\Cache\Filesystem\BrandCacheRepository $cacheRepository
     */
    public function __construct(
        BrandService $service,
        BrandRepository $repository,
        BrandCacheRepository $cacheRepository
    )
    {
        $this->service = $service;
        $this->repository = $repository;
        $this->cacheRepository = $cacheRepository;

        $this->cacheRepository->setRepository($this->repository);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // Pagination config
        $perPage = get_per_page();
        $currentPage = get_current_page();
        $offset = calculate_page_offset($perPage, $currentPage);

        // Make the results searchable.
        if ($searchQuery = get_search_query()) {
            $this->repository->addScope(new SearchScope(Brand::FIELD_NAME, $searchQuery));
            $this->cacheRepository->forgetCachedCounts();
        }

        // Filtering stats
        $totalBrands = $this->cacheRepository->count();
        $totalFeaturedBrands = $this->cacheRepository->countFeaturedBrands();
        $totalNotFeaturedBrands = $this->cacheRepository->countNotFeaturedBrands();

        switch ($filter = \request(KEYWORD_SHOW)) {
            case KEYWORD_STATUS_FEATURED:
                $items = $this->repository->getFeaturedBrands($perPage, $offset);
                $total = $totalFeaturedBrands;
                break;
            case KEYWORD_STATUS_NOT_FEATURED:
                $items = $this->repository->getNotFeaturedBrands($perPage, $offset);
                $total = $totalNotFeaturedBrands;
                break;
            default:
                $items = $this->repository->getBrands($perPage, $offset);
                $total = $totalBrands;
                $filter = KEYWORD_SHOW_ALL;
        }

        $urlQueryStringArray = [
            KEYWORD_SHOW => $filter,
            KEYWORD_PER_PAGE => $perPage,
            KEYWORD_SEARCH_QUERY => $searchQuery,
        ];

        $pagination = new LengthAwarePaginator(
            $items,
            $total,
            $perPage,
            $currentPage, [
            'path' => 'brands',
            'query' => $urlQueryStringArray
        ]);

        $filters = [
            KEYWORD_SHOW_ALL => [
                'text' => trans('misc.show_all'),
                'count' => $totalBrands,
            ],
            KEYWORD_STATUS_FEATURED => [
                'text' => trans('brands.status_featured'),
                'count' => $totalFeaturedBrands,
            ],
            KEYWORD_STATUS_NOT_FEATURED => [
                'text' => trans('brands.status_not_featured'),
                'count' => $totalNotFeaturedBrands,
            ],
        ];

        $filterViewOptions = [
            'route' => 'brands.index',
            'query' => $urlQueryStringArray,
            'filters' => $filters,
            'active' => $filter,
        ];

        return view('brands.index')->with(compact(
            'filter',
            'perPage',
            'pagination',
            'totalBrands',
            'filterViewOptions',
            'urlQueryStringArray',
            'totalFeaturedBrands',
            'totalNotFeaturedBrands'
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Brand $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function show(Brand $brand)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Brand $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function edit(Brand $brand)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Brand               $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Brand $brand
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Brand $brand)
    {
        //
    }

    /**
     * Remove the specified resources from storage.
     *
     * @param \Illuminate\Http\Request $request
     */
    public function bulkDestroy(Request $request)
    {
    }
}
