<?php

namespace App\Listeners;

use App\Events\InvoiceDeleted;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceDeletedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param InvoiceDeleted $event
     * @return void
     */
    public function handle(InvoiceDeleted $event)
    {
        //
    }
}
