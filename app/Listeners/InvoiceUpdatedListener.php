<?php

namespace App\Listeners;

use App\Events\InvoiceUpdated;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class InvoiceUpdatedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param InvoiceUpdated $event
     * @return void
     */
    public function handle(InvoiceUpdated $event)
    {
        //
    }
}
