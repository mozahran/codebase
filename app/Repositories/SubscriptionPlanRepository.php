<?php

namespace App\Repositories;

use App\SubscriptionPlan as Plan;

class SubscriptionPlanRepository extends BaseRepository
{
    /**
     * Subscription plans repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Plan::class));
    }
}