<?php

namespace App\Repositories;

use App\Category;

class CategoryRepository extends BaseRepository
{
    /**
     * Category repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Category::class));
    }
}