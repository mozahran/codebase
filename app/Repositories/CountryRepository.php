<?php

namespace App\Repositories;

use App\Country;

class CountryRepository extends BaseRepository
{
    /**
     * Country repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Country::class));
    }
}