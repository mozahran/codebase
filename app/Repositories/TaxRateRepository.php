<?php

namespace App\Repositories;

use App\TaxRate;

class TaxRateRepository extends BaseRepository
{
    /**
     * Tax rates repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(TaxRate::class));
    }
}