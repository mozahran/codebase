<?php

namespace App\Repositories;

use App\Coupon;

class CouponRepository extends BaseRepository
{
    /**
     * Coupon repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Coupon::class));
    }
}