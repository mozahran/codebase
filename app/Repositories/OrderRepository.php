<?php

namespace App\Repositories;

use App\Order;

class OrderRepository extends BaseRepository
{
    /**
     * Order repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Order::class));
    }
}