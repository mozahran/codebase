<?php

namespace App\Repositories;

use App\Support;

class SupportRepository extends BaseRepository
{
    /**
     * EloquentSupportRepository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Support::class));
    }
}