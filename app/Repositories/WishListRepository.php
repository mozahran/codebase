<?php

namespace App\Repositories;

use App\WishListItem;

class WishListRepository extends BaseRepository
{
    /**
     * Wish lists repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(WishListItem::class));
    }
}