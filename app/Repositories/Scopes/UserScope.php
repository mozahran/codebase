<?php

namespace App\Repositories\Scopes;

class UserScope implements ScopeInterface
{
    private $id;

    /**
     * User scope constructor.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->setId($id);
    }

    /**
     * Set the user id.
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->where('user_id', $this->id);
    }
}