<?php

namespace App\Repositories\Scopes;

class BooleanStatusScope implements ScopeInterface
{
    private $status;
    /**
     * @var string
     */
    private $field;

    /**
     * BooleanStatusScope constructor.
     *
     * @param string $field
     * @param bool   $status
     */
    public function __construct(string $field, bool $status)
    {
        $this->setStatus($status);
        $this->setField($field);
    }

    /**
     * Set the status.
     *
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * Set the name of the field.
     *
     * @param string $field
     */
    public function setField(string $field)
    {
        $this->field = $field;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->where($this->field, $this->status);
    }
}