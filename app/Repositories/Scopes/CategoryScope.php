<?php

namespace App\Repositories\Scopes;

class CategoryScope implements ScopeInterface
{
    private $id;

    /**
     * RepositoryCategoryScope constructor.
     *
     * @param int|array $id
     */
    public function __construct($id)
    {
        $this->setId($id);
    }

    /**
     * Set the category Id.
     *
     * @param int|array $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        switch($this->id) {

            case is_array($this->id):
                $query->whereIn('category_id', $this->id);
                break;

            case is_int($this->id):
                $query->where('category_id', $this->id);
                break;

        }
    }
}