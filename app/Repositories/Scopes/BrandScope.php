<?php

namespace App\Repositories\Scopes;

class BrandScope implements ScopeInterface
{
    private $id;

    /**
     * RepositoryBrandScope constructor.
     *
     * @param int $id
     */
    public function __construct(int $id)
    {
        $this->setId($id);
    }

    /**
     * Set the category Id.
     *
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->where('brand_id', $this->id);
    }
}