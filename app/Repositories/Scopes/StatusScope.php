<?php

namespace App\Repositories\Scopes;

class StatusScope implements ScopeInterface
{
    private $status;

    /**
     * Status scope constructor.
     *
     * @param string $status
     */
    public function __construct(string $status)
    {
        $this->setStatus($status);
    }

    /**
     * Set the status.
     *
     * @param string $status
     */
    public function setStatus(string $status)
    {
        $this->status = $status;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->where('status', $this->status);
    }
}