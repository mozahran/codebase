<?php

namespace App\Repositories\Scopes;

class UserEmailScope implements ScopeInterface
{
    private $email;

    /**
     * User scope constructor.
     *
     * @param string $email
     */
    public function __construct(string $email)
    {
        $this->setEmail($email);
    }

    /**
     * Set the user email.
     *
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->where('email', $this->email);
    }
}