<?php

namespace App\Repositories\Scopes;

interface ScopeInterface
{
    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query);
}