<?php

namespace App\Repositories\Scopes;

class SearchScope implements ScopeInterface
{
    /**
     * @var string
     */
    private $keyword;

    /**
     * @var string
     */
    private $field;

    /**
     * BooleanStatusScope constructor.
     *
     * @param string $field
     * @param string $keyword
     */
    public function __construct(string $field, string $keyword)
    {
        $this->setKeyword($keyword);
        $this->setField($field);
    }

    /**
     * Set the keyword.
     *
     * @param string $keyword
     */
    public function setKeyword(string $keyword)
    {
        $this->keyword = $keyword;
    }

    /**
     * Set the name of the field.
     *
     * @param string $field
     */
    public function setField(string $field)
    {
        $this->field = $field;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     *
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->where($this->field, 'like',  '%' . $this->keyword . '%');
    }
}