<?php

namespace App\Repositories\Scopes;

class RangeScope implements ScopeInterface
{
    private $start;
    private $end;
    private $field;

    /**
     * RangeScope constructor.
     *
     * @param $start
     * @param $end
     * @param string $field
     */
    public function __construct($start, $end, string $field)
    {
        $this->start = $start;
        $this->end = $end;
        $this->field = $field;
    }

    /**
     * Set the start point of the range.
     *
     * @param $start
     */
    public function setStart($start)
    {
        $this->start = $start;
    }

    /**
     * Set the end point of the range.
     *
     * @param $end
     */
    public function setEnd($end)
    {
        $this->end = $end;
    }

    /**
     * Set the field name.
     *
     * @param string $field
     */
    public function setField(string $field)
    {
        $this->field = $field;
    }

    /**
     * Apply the scope to the given query.
     *
     * @param $query
     * @return mixed
     */
    public function apply(&$query)
    {
        $query->whereBetween($this->field, [
            $this->start,
            $this->end
        ]);
    }
}