<?php

namespace App\Repositories;

use App\Invoice;

class InvoiceRepository extends BaseRepository
{
    /**
     * Invoice repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Invoice::class));
    }
}