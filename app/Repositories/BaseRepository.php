<?php

namespace App\Repositories;

use App\Repositories\Interfaces\BaseRepositoryInterface;
use App\Repositories\Traits\HasRelations;
use App\Repositories\Traits\HasScopes;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

abstract class BaseRepository implements BaseRepositoryInterface
{
    use HasScopes;
    use HasRelations;

    /**
     * An instance of the model.
     *
     * @var Model
     */
    private $model;

    /**
     * Set the model.
     *
     * @param Model $model
     */
    public function setModel(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get the model's instance.
     *
     * @return Model
     */
    public function model(): Model
    {
        return $this->model;
    }

    /**
     * Find one or more models by Id.
     *
     * @param       $id
     * @param array $columns
     *
     * @return Model|Collection|static[]|static|null
     */
    public function find($id, array $columns = ['*'])
    {
        $query = $this
            ->model()
            ->with($this->relations)
            ->withCount($this->countRelations);

        $this->applyScopes($query);

        return $query->find($id, $columns);
    }

    /**
     * Find a model by an attribute.
     *
     * @param string $attribute
     * @param        $value
     * @param array  $columns
     *
     * @return Model|null
     */
    public function findBy(
        string $attribute,
        $value,
        array $columns = ['*']
    )
    {
        $query = $this
            ->model()
            ->with($this->relations)
            ->withCount($this->countRelations)
            ->where($attribute, $value);

        $this->applyScopes($query);

        return $query->first($columns);
    }

    /**
     * Get all models.
     *
     * @param array $columns
     *
     * @return Collection
     */
    public function all(array $columns = ['*']): Collection
    {
        $query = $this
            ->model()
            ->with($this->relations)
            ->withCount($this->countRelations);

        $this->applyScopes($query);

        return $query->get($columns);
    }

    /**
     * Paginate existing models.
     *
     * @param int   $limit
     * @param int   $offset
     * @param array $columns
     *
     * @return Collection
     */
    public function get(
        int $limit = 15,
        int $offset = 0,
        array $columns = ['*']
    ): Collection
    {
        $query = $this
            ->model()
            ->with($this->relations)
            ->withCount($this->countRelations)
            ->limit($limit)
            ->offset($offset);

        $this->applyScopes($query);

        $results = $query->get($columns);


        return $results;
    }

    /**
     * Search existing models.
     *
     * @param string $attribute
     * @param string $keyword
     * @param int    $limit
     * @param int    $offset
     * @param array  $columns
     *
     * @return Collection
     */
    public function search(
        string $attribute,
        string $keyword,
        int $limit = 15,
        int $offset = 0,
        array $columns = ['*']
    ): Collection
    {
        $query = $this
            ->model()
            ->with($this->relations)
            ->withCount($this->countRelations)
            ->limit($limit)
            ->offset($offset)
            ->where($attribute, 'like', '%' . $keyword . '%');

        $this->applyScopes($query);

        return $query->get($columns);
    }

    /**
     * Count all models.
     *
     * @return int
     */
    public function count(): int
    {
        $query = $this->model()->select('count(*)');

        $this->applyScopes($query);

        return $query->count();
    }

    /**
     * Save the model.
     *
     * @param Model $model
     *
     * @return bool
     */
    public function save(Model $model): bool
    {
        return $model->save();
    }

    /**
     * Delete model by Id.
     *
     * @param $id
     *
     * @throws \Exception
     * @return bool
     */
    public function delete($id): bool
    {
        $model = $this->find($id);

        if ($model instanceof Model) {
            return $model->delete();
        }

        if (is_array($id)) {
            foreach ($id as $_id) {
                $this->delete($_id);
            }
            return true;
        }

        return false;
    }

    /**
     * Delete one or more model(s) by an attribute.
     *
     * @param string $attribute
     * @param string $value
     *
     * @throws \Exception
     */
    public function deleteBy(
        string $attribute,
        string $value
    ): void
    {
        $query = $this->model()->where($attribute, $value);

        $this->applyScopes($query);

        $models = $query->get();

        foreach ($models as $model) {
            $this->delete($model->getKey());
        }
    }
}