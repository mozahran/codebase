<?php

namespace App\Repositories;

use App\ShippingMethod;

class ShippingMethodRepository extends BaseRepository
{
    /**
     * Shipping methods repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(ShippingMethod::class));
    }
}