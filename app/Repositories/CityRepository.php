<?php

namespace App\Repositories;

use App\City;

class CityRepository extends BaseRepository
{
    /**
     * City repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(City::class));
    }
}