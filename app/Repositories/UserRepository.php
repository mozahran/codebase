<?php

namespace App\Repositories;

use App\User;
use App\Repositories\Scopes\UserEmailScope;

class UserRepository extends BaseRepository
{
    /**
     * Users repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(User::class));
    }

    /**
     * Check if the given email exists in the database.
     * 
     * @param  string $email
     * @return bool
     */
    public function emailExists(string $email) : bool
    {
        $this->addScope(new UserEmailScope($email));

        return (bool) $this->count();
    }
}