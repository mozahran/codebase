<?php

namespace App\Repositories;

use App\Brand;
use Illuminate\Database\Eloquent\Collection;
use App\Repositories\Scopes\BooleanStatusScope;
use App\Repositories\Interfaces\BrandRepositoryInterface;

class BrandRepository
    extends BaseRepository
    implements BrandRepositoryInterface
{
    /**
     * Brand repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Brand::class));
    }

    /**
     * @inheritDoc
     */
    public function getBrands(
        int $limit = 15,
        int $offset = 0,
        array $columns = ['*']
    ): Collection
    {
        return $this->get($limit, $offset,  $columns);
    }

    /**
     * @inheritDoc
     */
    public function count() : int
    {
        $query = $this->model()->newQuery();

        $this->applyScopes($query);

        return $query->count();
    }

    /**
     * @inheritDoc
     */
    public function countFeaturedBrands() : int
    {
        $query = $this->model()->featured();

        $this->applyScopes($query);

        return $query->count();
    }

    /**
     * @inheritDoc
     */
    public function countNotFeaturedBrands() : int
    {
        $query = $this->model()->notFeatured();

        $this->applyScopes($query);

        return $query->count();
    }

    /**
     * @inheritDoc
     */
    public function getFeaturedBrands($limit = 15, $offset = 0, $columns = ['*']) : Collection
    {
        $this->addScope(new BooleanStatusScope(Brand::FIELD_IS_FEATURED, Brand::STATUS_FEATURED));

        return $this->get($limit, $offset, $columns);
    }

    /**
     * @inheritDoc
     */
    public function getNotFeaturedBrands($limit = 15, $offset = 0, $columns = ['*']) : Collection
    {
        $this->addScope(new BooleanStatusScope(Brand::FIELD_IS_FEATURED, Brand::STATUS_NOT_FEATURED));

        return $this->get($limit, $offset, $columns);
    }
}