<?php

namespace App\Repositories\Interfaces;

interface Countable
{
    /**
     * Count all models.
     *
     * @return int
     */
    public function count() : int;
}