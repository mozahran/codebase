<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

interface BaseRepositoryInterface extends
    Countable,
    Searchable
{
    /**
     * Find one or more models by Id.
     *
     * @param $id
     * @param array $columns
     */
    public function find($id, array $columns = ['*']);

    /**
     * Find a model by an attribute.
     *
     * @param string $attribute
     * @param $value
     * @param array $columns
     */
    public function findBy(
        string $attribute,
        $value,
        array $columns = ['*']
    );

    /**
     * Get all models.
     *
     * @param array $columns
     * @return Collection
     */
    public function all(array $columns = ['*']) : Collection;

    /**
     * Paginate existing models.
     *
     * @param int $limit
     * @param int $offset
     * @param array $columns
     * @return Collection
     */
    public function get(
        int $limit = 15,
        int $offset = 0,
        array $columns = ['*']
    ) : Collection;

    /**
     * Save the model.
     *
     * @param Model $model
     * @return bool
     */
    public function save(Model $model) : bool;

    /**
     * Delete model by Id.
     *
     * @param int $id
     * @throws \Exception
     * @return bool
     */
    public function delete(int $id) : bool;

    /**
     * Delete one or more model(s) by an attribute.
     *
     * @param string $attribute
     * @param string $value
     * @throws \Exception
     */
    public function deleteBy(
        string $attribute,
        string $value
    ) : void;
}