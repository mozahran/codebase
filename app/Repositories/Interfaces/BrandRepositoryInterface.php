<?php

namespace App\Repositories\Interfaces;

use Illuminate\Database\Eloquent\Collection;

interface BrandRepositoryInterface
    extends Countable
{
    /**
     * Get all brands.
     *
     * @param int   $limit
     * @param int   $offset
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getBrands(
        int $limit = 15,
        int $offset = 0,
        array $columns = ['*']
    ): Collection;

    /**
     * Get featured brands.
     *
     * @param int   $limit
     * @param int   $offset
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getFeaturedBrands($limit = 15, $offset = 0, $columns = ['*']) : Collection;

    /**
     * Get brands that are not featured.
     *
     * @param int   $limit
     * @param int   $offset
     * @param array $columns
     *
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getNotFeaturedBrands($limit = 15, $offset = 0, $columns = ['*']) : Collection;

    /**
     * Count featured brands.
     *
     * @return int
     */
    public function countFeaturedBrands() : int;

    /**
     * Count brands that are not featured.
     *
     * @return int
     */
    public function countNotFeaturedBrands() : int;
}