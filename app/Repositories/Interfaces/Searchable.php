<?php

namespace App\Repositories\Interfaces;

interface Searchable
{
    /**
     * Search existing models.
     *
     * @param string $attribute
     * @param string $keyword
     * @param int $limit
     * @param int $offset
     * @param array $columns
     */
    public function search(
        string $attribute,
        string $keyword,
        int $limit = 15,
        int $offset = 0,
        array $columns = ['*']
    );
}