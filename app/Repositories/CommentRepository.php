<?php

namespace App\Repositories;

use App\Comment;
use App\Post;
use App\Repositories\Interfaces\CommentRepositoryInterface;
use Illuminate\Database\Eloquent\Collection;

class CommentRepository extends BaseRepository
{
    /**
     * Comment repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Comment::class));
    }
}