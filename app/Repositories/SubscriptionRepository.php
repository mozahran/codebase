<?php

namespace App\Repositories;

use App\Repositories\Interfaces\SubscriptionRepositoryInterface;
use App\Subscription;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class SubscriptionRepository extends BaseRepository
{
    /**
     * Subscriptions repository constructor.
     */
    public function __construct()
    {
        $this->setModel(app(Subscription::class));
    }
}