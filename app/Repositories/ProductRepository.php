<?php

namespace App\Repositories;

use App\Product;

class ProductRepository extends BaseRepository
{
    /**
     * Product repository constructor.
     */
    public function __construct()
    {
        $this->setModel(new Product);
    }
}