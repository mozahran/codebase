<?php

namespace App;

use App\Codebase\Constants\Fields;
use App\Traits\HasId;
use App\Traits\BelongsToUser;
use App\Traits\BelongsToCategory;
use App\Traits\HasImageRelation;
use App\Traits\Abusable;
use App\Traits\HasName;
use App\Traits\HasDescription;
use App\Traits\HasPrice;
use App\Traits\HasQuantity;
use App\Traits\HasStatus;

use App\Events\ProductCreated;
use App\Events\ProductDeleted;
use App\Events\ProductUpdated;

use App\Traits\Statuses\HasDraftStatus;
use App\Traits\Statuses\HasPendingStatus;
use App\Traits\Statuses\HasPublishedStatus;
use App\Traits\Statuses\HasFeaturedStatus;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasId,
        BelongsToUser,
        BelongsToCategory,
        HasImageRelation,
        Abusable,
        HasName,
        HasDescription,
        HasQuantity,
        HasPrice,
        HasStatus,
        HasPendingStatus,
        HasDraftStatus,
        HasPublishedStatus,
        HasFeaturedStatus;

    protected $fillable = [
        Fields::CATEGORY_ID,
        Fields::BRAND_ID,
        Fields::USER_ID,
        Fields::NAME,
        Fields::DESCRIPTION,
        Fields::PRICE,
        Fields::QUANTITY,
        Fields::VAT,
        Fields::STATUS,
    ];

    protected $casts = [
        Fields::CATEGORY_ID => 'integer',
        Fields::BRAND_ID => 'integer',
        Fields::USER_ID => 'integer',
        Fields::PRICE => 'decimal',
        Fields::QUANTITY => 'decimal',
        Fields::VAT => 'decimal',
    ];

    protected $dispatchesEvents = [
        'created' => ProductCreated::class,
        'updated' => ProductUpdated::class,
        'deleted' => ProductDeleted::class,
    ];
}