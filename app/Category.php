<?php

namespace App;

use App\Events\CategoryCreated;
use App\Events\CategoryDeleted;
use App\Events\CategoryUpdated;
use App\Exceptions\InvalidTypeException;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'categories';

    const FIELD_PK = 'id';
    const FIELD_PARENT_ID = 'parent_id';
    const FIELD_NAME = 'name';
    const FIELD_SORT_ORDER = 'sort_order';
    const FIELD_IS_ACTIVE = 'is_active';

    const DEFAULT_PARENT_ID = 0;

    const STATUS_ALL = -1;
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const TYPE_ALL = -1;
    const TYPE_PARENT = 0;
    const TYPE_CHILD = 1;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_PARENT_ID,
        self::FIELD_NAME,
        self::FIELD_SORT_ORDER,
        self::FIELD_IS_ACTIVE,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_PARENT_ID => 'integer',
        self::FIELD_SORT_ORDER => 'integer',
        self::FIELD_IS_ACTIVE => 'boolean',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => CategoryCreated::class,
        'updated' => CategoryUpdated::class,
        'deleted' => CategoryDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the category Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the parent category Id.
     *
     * @return int
     */
    public function getParentId() : int
    {
        return $this->getAttribute(self::FIELD_PARENT_ID);
    }

    /**
     * The name of the category.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the sort order of the category.
     *
     * @return int
     */
    public function getSortOrder() : int
    {
        return $this->getAttribute(self::FIELD_SORT_ORDER);
    }

    /**
     * Check if the category is active.
     *
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->getAttribute(self::FIELD_IS_ACTIVE) == true;
    }

    /**
     * Check if the category is inactive.
     *
     * @return bool
     */
    public function isInactive() : bool
    {
        return $this->getAttribute(self::FIELD_IS_ACTIVE) == false;
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the parent id of the category.
     *
     * @param int $id
     * @return $this
     */
    public function setParentId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_PARENT_ID, $id);
    }

    /**
     * Set the name of the category.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the sort order of the category.
     *
     * @param int $n
     * @return $this
     */
    public function setSortOrder(int $n) : self
    {
        return $this->setAttribute(self::FIELD_SORT_ORDER, $n);
    }

    /**
     * Set the category as active.
     *
     * @return $this
     */
    public function setActive() : self
    {
        return $this->setAttribute(self::FIELD_IS_ACTIVE, self::STATUS_ACTIVE);
    }

    /**
     * Set the category as inactive.
     *
     * @return $this
     */
    public function setInactive() : self
    {
        return $this->setAttribute(self::FIELD_IS_ACTIVE, self::STATUS_INACTIVE);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope categories by status.
     *
     * @param $query
     * @param int $status
     * @return mixed
     */
    public function scopeOfStatus($query, int $status)
    {
        return $query->where(self::FIELD_IS_ACTIVE, (bool) $status);
    }

    /**
     * Scope active categories.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeOfStatusActive($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ACTIVE);
    }

    /**
     * Scope inactive categories.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeOfStatusInactive($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_INACTIVE);
    }

    /**
     * Scope categories by type.
     *
     * @param     $query
     * @param int $type
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidTypeException
     */
    public function scopeOfType($query, int $type)
    {
        if ($type === self::TYPE_ALL) {
            return $query;
        }

        if ( ! in_array($type, [
            self::TYPE_PARENT,
            self::TYPE_CHILD,
        ])) {
            throw new InvalidTypeException(sprintf("Invalid category type '%s'", $type));
        }

        switch ($type) {
            case self::TYPE_PARENT:
                return $query->where(self::FIELD_PARENT_ID, 0);
                break;

            case self::TYPE_CHILD:
                return $query->where(self::FIELD_PARENT_ID, '!=', 0);
                break;
        }
    }

    /**
     * Scope parent categories.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidTypeException
     */
    public function scopeOfTypeParent($query)
    {
        return $this->scopeOfType($query, self::TYPE_PARENT);
    }

    /**
     * Scope child categories.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidTypeException
     */
    public function scopeOfTypeChild($query)
    {
        return $this->scopeOfType($query, self::TYPE_PARENT);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The image that belongs to the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this->morphOne(Image::class, Image::FIELD_MORPHS)->withDefault([
            Image::FIELD_IMAGE_URL => get_default_image(),
            Image::FIELD_THUMB_URL => get_default_image(),
        ]);
    }

    /**
     * The parent category that this category belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(
            self::class,
            self::FIELD_PARENT_ID,
            self::FIELD_PK
        );
    }

    /**
     * The children categories that belong to this category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->hasMany(
            self::class,
            self::FIELD_PARENT_ID,
            self::FIELD_PK
        );
    }

    /**
     * The products that belong to the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(
            Product::class,
            Product::FIELD_CATEGORY_ID,
            self::FIELD_PK
        );
    }

    /**
     * The posts that belong to the category.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(
            Post::class,
            Post::FIELD_CATEGORY_ID,
            self::FIELD_PK
        );
    }
}
