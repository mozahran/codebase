<?php

namespace App\Presenters\Brand;

use App\Brand;

class UrlPresenter
{
    /**
     * @var \App\Brand
     */
    private $brand;

    /**
     * UrlPresenter constructor.
     *
     * @param \App\Brand $brand
     */
    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    public function __get($key)
    {
        if (method_exists($this, $key)) {
            return $this->$key();
        }

        return $this->$key;
    }

    public function edit()
    {
        return route('brands.edit', $this->brand);
    }

    public function update()
    {
        return route('brands.update', $this->brand);
    }

    public function show()
    {
        return route('brands.show', $this->brand);
    }

    public function delete()
    {
        return route('brands.destroy', $this->brand);
    }
}