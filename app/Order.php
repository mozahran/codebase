<?php

namespace App;

use App\Events\OrderCreated;
use App\Exceptions\InvalidStatusException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'orders';

    const FIELD_PK = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_COUPON_ID = 'coupon_id';
    const FIELD_SHIPPING_METHOD_ID = 'shipping_method_id';
    const FIELD_QTY = 'qty';
    const FIELD_DISCOUNT = 'discount';
    const FIELD_TAX = 'tax';
    const FIELD_SHIPPING_FEES = 'shipping_fees';
    const FIELD_TOTAL = 'total';
    const FIELD_IS_FREE_SHIPPED = 'is_free_shipped';
    const FIELD_STATUS = 'status';
    const FIELD_SHIPPED_AT = 'shipped_at';
    const FIELD_REFUNDED_AT = 'refunded_at';
    const FIELD_CANCELLED_AT = 'cancelled_at';
    const FIELD_CANCELLED_BY = 'cancelled_by';
    const FIELD_CANCEL_REASON = 'cancel_reason';
    const FIELD_COMMENTS = 'comments';

    const STATUS_ALL = null;
    const STATUS_PENDING = 'pending';
    const STATUS_ON_HOLD = 'on_hold';
    const STATUS_SHIPPED = 'shipped';
    const STATUS_CANCELLED = 'cancelled';
    const STATUS_REFUNDED = 'refunded';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_COUPON_ID,
        self::FIELD_SHIPPING_METHOD_ID,
        self::FIELD_QTY,
        self::FIELD_DISCOUNT,
        self::FIELD_TAX,
        self::FIELD_SHIPPING_FEES,
        self::FIELD_TOTAL,
        self::FIELD_IS_FREE_SHIPPED,
        self::FIELD_STATUS,
        self::FIELD_SHIPPED_AT,
        self::FIELD_REFUNDED_AT,
        self::FIELD_CANCELLED_AT,
        self::FIELD_CANCELLED_BY,
        self::FIELD_CANCEL_REASON,
        self::FIELD_COMMENTS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_USER_ID => 'integer',
        self::FIELD_COUPON_ID => 'integer',
        self::FIELD_SHIPPING_METHOD_ID => 'integer',
        self::FIELD_QTY => 'integer',
        self::FIELD_DISCOUNT => 'decimal',
        self::FIELD_TAX => 'decimal',
        self::FIELD_SHIPPING_FEES => 'decimal',
        self::FIELD_TOTAL => 'decimal',
        self::FIELD_IS_FREE_SHIPPED => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        self::FIELD_SHIPPED_AT,
        self::FIELD_REFUNDED_AT,
        self::FIELD_CANCELLED_AT,
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => OrderCreated::class,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'items'
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the order Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the user Id.
     *
     * @return int
     */
    public function getUserId() : int
    {
        return $this->getAttribute(self::FIELD_USER_ID);
    }

    /**
     * Get the coupon Id.
     *
     * @return int
     */
    public function getCouponId() : int
    {
        return $this->getAttribute(self::FIELD_COUPON_ID);
    }

    /**
     * Get the shipping method Id.
     *
     * @return int
     */
    public function getShippingMethodId() : int
    {
        return $this->getAttribute(self::FIELD_SHIPPING_METHOD_ID);
    }

    /**
     * Get the number of items in the order.
     *
     * @return int
     */
    public function getQty() : int
    {
        return $this->getAttribute(self::FIELD_QTY);
    }

    /**
     * Get the discount for the order.
     *
     * @return float
     */
    public function getDiscount() : float
    {
        return $this->getAttribute(self::FIELD_DISCOUNT);
    }

    /**
     * Get the tax for the order.
     *
     * @return float
     */
    public function getTax() : float
    {
        return $this->getAttribute(self::FIELD_TAX);
    }

    /**
     * Get the shipping fees of the order.
     *
     * @return float
     */
    public function getShippingFees() : float
    {
        return $this->getAttribute(self::FIELD_SHIPPING_FEES);
    }

    /**
     * Get the grand total of the items.
     *
     * @return float
     */
    public function getTotal() : float
    {
        return $this->getAttribute(self::FIELD_TOTAL);
    }

    /**
     * Check if the order is free shipped.
     *
     * @return bool
     */
    public function isFreeShipped() : bool
    {
        return $this->getAttribute(self::FIELD_IS_FREE_SHIPPED);
    }

    /**
     * Get the status of the order.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the order is pending.
     *
     * @return bool
     */
    public function isPending() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_PENDING;
    }

    /**
     * Check if the order is on-hold.
     *
     * @return bool
     */
    public function isOnHold() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_ON_HOLD;
    }

    /**
     * Check if the order is shipped.
     *
     * @return bool
     */
    public function isShipped() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_SHIPPED;
    }

    /**
     * Check if the order is cancelled.
     *
     * @return bool
     */
    public function isCancelled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_CANCELLED;
    }

    /**
     * Check if the order is refunded.
     *
     * @return bool
     */
    public function isRefunded() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_REFUNDED;
    }

    /**
     * Get the date of the shipment.
     *
     * @return null|Carbon
     */
    public function getShippedAt() : ?Carbon
    {
        return $this->getAttribute(self::FIELD_SHIPPED_AT);
    }

    /**
     * Get the date of the refund.
     *
     * @return null|Carbon
     */
    public function getRefundedAt() : ?Carbon
    {
        return $this->getAttribute(self::FIELD_REFUNDED_AT);
    }

    /**
     * Get the date of the cancellation.
     *
     * @return null|Carbon
     */
    public function getCancelledAt() : ?Carbon
    {
        return $this->getAttribute(self::FIELD_CANCELLED_AT);
    }

    /**
     * Get the canceller Id.
     *
     * @return int
     */
    public function getCancelledBy() : int
    {
        return $this->getAttribute(self::FIELD_CANCELLED_BY);
    }

    /**
     * Get the cancellation reason.
     *
     * @return string
     */
    public function getCancelReason() : string
    {
        return $this->getAttribute(self::FIELD_CANCEL_REASON);
    }

    /**
     * Get the comments on the order.
     *
     * @return string
     */
    public function getComments() : string
    {
        return $this->getAttribute(self::FIELD_COMMENTS);
    }

    /**
     * Get the date of creating the order.
     *
     * @return Carbon
     */
    public function getCreatedAt() : Carbon
    {
        return $this->getAttribute(self::CREATED_AT);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the user Id.
     *
     * @param int $id
     * @return $this
     */
    public function setUserId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_USER_ID, $id);
    }

    /**
     * Set the coupon Id.
     *
     * @param int $id
     * @return $this
     */
    public function setCouponId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_COUPON_ID, $id);
    }

    /**
     * Set the shipping method Id.
     *
     * @param int $id
     * @return $this
     */
    public function setShippingMethodId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_SHIPPING_METHOD_ID, $id);
    }

    /**
     * Set the number of items in the order.
     *
     * @param int $qty
     * @return $this
     */
    public function setQty(int $qty) : self
    {
        return $this->setAttribute(self::FIELD_QTY, $qty);
    }

    /**
     * Set the total discount of the order.
     *
     * @param float $amount
     * @return $this
     */
    public function setDiscount(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_DISCOUNT, $amount);
    }

    /**
     * Set the total tax of the order.
     *
     * @param float $rate
     * @return $this
     */
    public function setTax(float $rate) : self
    {
        return $this->setAttribute(self::FIELD_TAX, $rate);
    }

    /**
     * Set the shipping fees of the orders.
     *
     * @param float $fees
     * @return $this
     */
    public function setShippingFees(float $fees) : self
    {
        return $this->setAttribute(self::FIELD_SHIPPING_FEES, $fees);
    }

    /**
     * Set the total of the items in the order.
     *
     * @param float $sum
     * @return $this
     */
    public function setTotal(float $sum) : self
    {
        return $this->setAttribute(self::FIELD_TOTAL, $sum);
    }

    /**
     * Set the status of the free shipping of the order.
     *
     * @param bool $status
     * @return $this
     */
    public function setIsFreeShipped(bool $status) : self
    {
        return $this->setAttribute(self::FIELD_IS_FREE_SHIPPED, $status);
    }

    /**
     * Set the status of the order.
     *
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status) : self
    {
        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the status as pending.
     *
     * @return $this
     */
    public function setPending() : self
    {
        return $this->setStatus(self::STATUS_PENDING);
    }

    /**
     * Set the status as on-hold.
     *
     * @return $this
     */
    public function setOnHold() : self
    {
        return $this->setStatus(self::STATUS_ON_HOLD);
    }

    /**
     * Set the status as shipped.
     *
     * @return $this
     */
    public function setShipped() : self
    {
        return $this->setStatus(self::STATUS_SHIPPED);
    }

    /**
     * Set the status as cancelled.
     *
     * @return $this
     */
    public function setCancelled() : self
    {
        return $this->setStatus(self::STATUS_CANCELLED);
    }

    /**
     * Set the status as refunded.
     *
     * @return $this
     */
    public function setRefunded() : self
    {
        return $this->setStatus(self::STATUS_REFUNDED);
    }

    /**
     * Set the date of the shipment.
     *
     * @param Carbon $timestamp
     * @return $this
     */
    public function setShippedAt(Carbon $timestamp) : self
    {
        return $this->setAttribute(self::FIELD_SHIPPED_AT, $timestamp);
    }

    /**
     * Set the date of the refund.
     *
     * @param Carbon $timestamp
     * @return $this
     */
    public function setRefundedAt(Carbon $timestamp) : self
    {
        return $this->setAttribute(self::FIELD_REFUNDED_AT, $timestamp);
    }

    /**
     * Set the date of the cancellation.
     *
     * @param Carbon $timestamp
     * @return $this
     */
    public function setCancelledAt(Carbon $timestamp) : self
    {
        return $this->setAttribute(self::FIELD_CANCELLED_BY, $timestamp);
    }

    /**
     * Set the canceller Id.
     *
     * @param int $id
     * @return $this
     */
    public function setCancelledBy(int $id) : self
    {
        return $this->setAttribute(self::FIELD_CANCELLED_BY, $id);
    }

    /**
     * Set the reason of the cancellation.
     *
     * @param string $text
     * @return $this
     */
    public function setCancelReason(string $text) : self
    {
        return $this->setAttribute(self::FIELD_CANCEL_REASON, $text);
    }

    /**
     * Get the comments on the order.
     *
     * @param string $comments
     * @return $this
     */
    public function setComments(string $comments) : self
    {
        return $this->setAttribute(self::FIELD_COMMENTS, $comments);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope orders by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_PENDING,
            self::STATUS_CANCELLED,
            self::STATUS_ON_HOLD,
            self::STATUS_REFUNDED,
            self::STATUS_SHIPPED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid order status '%s'", $status));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope pending orders.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopePending($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_PENDING);
    }

    /**
     * Scope on-hold orders.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOnHold($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ON_HOLD);
    }

    /**
     * Scope shipped orders.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeShipped($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_SHIPPED);
    }

    /**
     * Scope cancelled orders.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeCancelled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_CANCELLED);
    }

    /**
     * Scope refunded orders.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeRefunded($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_REFUNDED);
    }

    /**
     * Scope free shipped orders.
     *
     * @param $query
     * @return mixed
     */
    public function scopeFreeShipped($query)
    {
        return $query->where(self::FIELD_IS_FREE_SHIPPED, true);
    }

    /**
     * Scope orders that are not free shipped.
     *
     * @param $query
     * @return mixed
     */
    public function scopeNotFreeShipped($query)
    {
        return $query->where(self::FIELD_IS_FREE_SHIPPED, false);
    }

    /**
     * Scope orders that use coupons.
     *
     * @param $query
     * @return mixed
     */
    public function scopeUsesCoupon($query)
    {
        return $query->where(self::FIELD_COUPON_ID, '!=', 0);
    }

    /**
     * Scope orders that don't use coupons.
     *
     * @param $query
     * @return mixed
     */
    public function scopeDoesntUseCoupon($query)
    {
        return $query->where(self::FIELD_COUPON_ID, 0);
    }

    /**
     * Scope orders that have discount.
     *
     * @param $query
     * @return mixed
     */
    public function scopeHasDiscount($query)
    {
        return $query->where(self::FIELD_DISCOUNT, '!=', 0);
    }

    /**
     * Scope orders that have discount.
     *
     * @param $query
     * @return mixed
     */
    public function scopeDoesntHaveDiscount($query)
    {
        return $query->where(self::FIELD_DISCOUNT, 0);
    }

    /**
     * Scope orders by user.
     *
     * @param $query
     * @param int $userId
     * @return mixed
     */
    public function scopeForUser($query, int $userId)
    {
        return $query->where(self::FIELD_USER_ID, $userId);
    }

    /**
     * Scope orders by shipping method.
     *
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeOfShippingMethod($query, int $id)
    {
        return $query->where(self::FIELD_SHIPPING_METHOD_ID, $id);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The user that the order belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            self::FIELD_USER_ID,
            User::FIELD_PK
        );
    }

    /**
     * The items that belong to the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany(
            OrderItem::class,
            OrderItem::FIELD_ORDER_ID,
            self::FIELD_PK
        );
    }

    /**
     * The shipping method of the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function shippingMethod()
    {
        return $this->hasOne(
            ShippingMethod::class,
            ShippingMethod::FIELD_PK,
            self::FIELD_SHIPPING_METHOD_ID
        );
    }

    /**
     * The coupon that is used in the order.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function coupon()
    {
        return $this->hasOne(
            Coupon::class,
            Coupon::FIELD_PK,
            self::FIELD_COUPON_ID
        );
    }
}
