<?php

namespace App;

use App\Events\ShippingMethodCreated;
use App\Events\ShippingMethodDeleted;
use App\Events\ShippingMethodUpdated;
use App\Exceptions\InvalidStatusException;
use Illuminate\Database\Eloquent\Model;

class ShippingMethod extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'shipping_methods';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_PRICE = 'price';
    const FIELD_STATUS = 'status';

    const STATUS_ALL = null;
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_PRICE,
        self::FIELD_STATUS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_PRICE => 'decimal',
        self::FIELD_STATUS => 'boolean',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => ShippingMethodCreated::class,
        'updated' => ShippingMethodUpdated::class,
        'deleted' => ShippingMethodDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the shipping method Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the name of the shipping method.
     *
     * @return mixed
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the description of the shipping method.
     *
     * @return mixed
     */
    public function getDescription() : string
    {
        return $this->getAttribute(self::FIELD_DESCRIPTION);
    }

    /**
     * Get the cost of the shipping method.
     *
     * @return float
     */
    public function getPrice() : float
    {
        return $this->getAttribute(self::FIELD_PRICE);
    }

    /**
     * Get the status of the shipping method.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the shipping method is enabled.
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_ENABLED;
    }

    /**
     * Check if the shipping method is disabled.
     *
     * @return bool
     */
    public function isDisabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_DISABLED;
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the name of the shipping method.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the description of the shipping method.
     *
     * @param string $text
     * @return $this
     */
    public function setDescription(string $text) : self
    {
        return $this->setAttribute(self::FIELD_DESCRIPTION, $text);
    }

    /**
     * Set the cost of the shipping method.
     *
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price) : self
    {
        return $this->setAttribute(self::FIELD_PRICE, $price);
    }

    /**
     * Set the status of the shipping method.
     *
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status) : self
    {
        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the status as enabled.
     *
     * @return $this
     */
    public function setEnabled() : self
    {
        return $this->setStatus(self::STATUS_ENABLED);
    }

    /**
     * Set the status as disabled.
     *
     * @return $this
     */
    public function setDisabled() : self
    {
        return $this->setStatus(self::STATUS_DISABLED);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope shipping methods by status.
     *
     * @param     $query
     * @param int $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, int $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid shipping method status '%s'", $status));
        }

        return $query->where(self::FIELD_STATUS, (bool) $status);
    }

    /**
     * Scope enabled shipping methods.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeEnabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    /**
     * Scope disabled shipping methods.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeDisabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

}
