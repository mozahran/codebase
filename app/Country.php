<?php

namespace App;

use App\Events\CountryCreated;
use App\Events\CountryDeleted;
use App\Events\CountryUpdated;
use App\Exceptions\InvalidStatusException;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'countries';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_ISO_NAME = 'iso_name';
    const FIELD_STATUS = 'status';

    const STATUS_ALL = null;
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_ISO_NAME,
        self::FIELD_STATUS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_STATUS => 'boolean',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => CountryCreated::class,
        'updated' => CountryUpdated::class,
        'deleted' => CountryDeleted::class,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'cities',
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the country Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the country name.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the iso name of the country.
     *
     * @return string
     */
    public function getISOName() : string
    {
        return $this->getAttribute(self::FIELD_ISO_NAME);
    }

    /**
     * Get the status of the country.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the country is enabled.
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_ENABLED;
    }

    /**
     * Check if the country is disabled.
     *
     * @return bool
     */
    public function isDisabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_DISABLED;
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the country name.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the ISO name of the country.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setISOName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_ISO_NAME, $name);
    }

    /**
     * Set the status of the country.
     *
     * @param string $status
     *
     * @return $this
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setStatus(string $status) : self
    {
        if (in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid country status '%s'", $status));
        }

        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the status as enabled.
     *
     * @return $this
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setEnabled() : self
    {
        return $this->setStatus(self::STATUS_ENABLED);
    }

    /**
     * Set the status as disabled.
     *
     * @return $this
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setDisabled() : self
    {
        return $this->setStatus(self::STATUS_DISABLED);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope countries by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status !== self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid country status '%s'", $status));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope enabled countries.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeEnabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    /**
     * Scope disabled countries.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeDisabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The cities that belong to the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cities()
    {
        return $this->hasMany(
            City::class,
            City::FIELD_COUNTRY_ID,
            self::FIELD_PK
        );
    }

    /**
     * The users that belong to the country.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function users()
    {
        return $this->hasMany(
            User::class,
            User::FIELD_COUNTRY_ID,
            self::FIELD_PK
        );
    }
}
