<?php

namespace App;

use Carbon\Carbon;
use App\Events\SubscriptionCreated;
use App\Events\SubscriptionDeleted;
use App\Events\SubscriptionUpdated;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\InvalidStatusException;

class Subscription extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'subscriptions';

    const FIELD_PLAN_ID = 'subscription_plan_id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_FEES = 'fees';
    const FIELD_EXPIRES_AT = 'expires_at';

    const STATUS_ALL = -1;
    const STATUS_VALID = 1;
    const STATUS_EXPIRED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_PLAN_ID,
        self::FIELD_FEES,
        self::FIELD_EXPIRES_AT,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_FEES => 'decimal'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        self::FIELD_EXPIRES_AT,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'user',
        'plan'
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => SubscriptionCreated::class,
        'updated' => SubscriptionUpdated::class,
        'deleted' => SubscriptionDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the subscription plan Id.
     *
     * @return int
     */
    public function getPlanId() : int
    {
        return $this->getAttribute(self::FIELD_PLAN_ID);
    }

    /**
     * Get the user Id.
     *
     * @return int
     */
    public function getUserId() : int
    {
        return $this->getAttribute(self::FIELD_USER_ID);
    }

    /**
     * Get the fees that the user has paid for this subscription.
     *
     * @return float
     */
    public function getFees() : float
    {
        return $this->getAttribute(self::FIELD_FEES);
    }

    /**
     * Get the expiration date.
     *
     * @return Carbon
     */
    public function getExpiresAt() : Carbon
    {
        return $this->getAttribute(self::FIELD_EXPIRES_AT);
    }

    /**
     * Check if the subscription is valid.
     *
     * @return bool
     */
    public function isValid() : bool
    {
        return self::FIELD_EXPIRES_AT >= Carbon::now();
    }

    /**
     * Check if the subscription is expired.
     *
     * @return bool
     */
    public function isExpired() : bool
    {
        return self::FIELD_EXPIRES_AT < Carbon::now();
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Get the subscription plan Id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setPlanId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_PLAN_ID, $id);
    }

    /**
     * Set the user Id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setUserId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_USER_ID, $id);
    }

    /**
     * Set the fees that the user has paid for this subscription.
     *
     * @param float $amount
     *
     * @return $this
     */
    public function setFees(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_FEES, $amount);
    }

    /**
     * Set the expiration date.
     *
     * @param Carbon $date
     *
     * @return $this
     */
    public function setExpiresAt(Carbon $date) : self
    {
        return $this->setAttribute(self::FIELD_EXPIRES_AT, $date);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope subscriptions by plan.
     *
     * @param     $query
     * @param int $id
     *
     * @return mixed
     */
    public function scopeOfPlan($query, int $id)
    {
        return $query->where(self::FIELD_PLAN_ID, $id);
    }

    /**
     * Scope subscriptions for a specific user.
     *
     * @param     $query
     * @param int $id
     *
     * @return mixed
     */
    public function scopeForUser($query, int $id)
    {
        return $query->where(self::FIELD_USER_ID, $id);
    }

    /**
     * Scope posts by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_VALID,
            self::STATUS_EXPIRED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid subscription status '%s'", $status));
        }

        switch ($status) {
            case self::STATUS_VALID:
                return $this->scopeValid($query);
                break;

            case self::STATUS_EXPIRED:
                return $this->scopeExpired($query);
                break;
        }
    }

    /**
     * Scope valid subscriptions.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeValid($query)
    {
        return $query->where(self::FIELD_EXPIRES_AT, '>=', Carbon::now());
    }

    /**
     * Scope expired subscriptions.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeExpired($query)
    {
        return $query->where(self::FIELD_EXPIRES_AT, '<', Carbon::now());
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The subscription plan that the subscription belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function plan()
    {
        return $this->belongsTo(
            SubscriptionPlan::class,
            self::FIELD_PLAN_ID,
            SubscriptionPlan::FIELD_PK
        );
    }

    /**
     * The user that the subscription belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            self::FIELD_USER_ID,
            User::FIELD_PK
        );
    }
}
