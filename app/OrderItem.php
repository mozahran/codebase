<?php

namespace App;

use App\Codebase\Constants\Fields;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'order_items';

    const FIELD_PK = 'id';
    const FIELD_ORDER_ID = 'order_id';
    const FIELD_PRODUCT_ID = 'product_id';
    const FIELD_QTY = 'qty';
    const FIELD_PRICE = 'price';
    const FIELD_DISCOUNT = 'discount';
    const FIELD_TAX = 'tax';
    const FIELD_SUBTOTAL = 'subtotal';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_ORDER_ID,
        self::FIELD_PRODUCT_ID,
        self::FIELD_QTY,
        self::FIELD_PRICE,
        self::FIELD_DISCOUNT,
        self::FIELD_TAX,
        self::FIELD_SUBTOTAL,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_ORDER_ID => 'integer',
        self::FIELD_PRODUCT_ID => 'integer',
        self::FIELD_QTY => 'integer',
        self::FIELD_PRICE => 'decimal',
        self::FIELD_DISCOUNT => 'decimal',
        self::FIELD_TAX => 'decimal',
        self::FIELD_SUBTOTAL => 'decimal',
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the item Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the order Id.
     *
     * @return int
     */
    public function getOrderId() : int
    {
        return $this->getAttribute(self::FIELD_ORDER_ID);
    }

    /**
     * Get the product Id.
     *
     * @return int
     */
    public function getProductId() : int
    {
        return $this->getAttribute(self::FIELD_PRODUCT_ID);
    }

    /**
     * Get the quantity of the item.
     *
     * @return int
     */
    public function getQty() : int
    {
        return $this->getAttribute(self::FIELD_QTY);
    }

    /**
     * Get the price of the item.
     *
     * @return float
     */
    public function getPrice() : float
    {
        return $this->getAttribute(self::FIELD_PRICE);
    }

    /**
     * Get the discount for the item.
     *
     * @return float
     */
    public function getDiscount() : float
    {
        return $this->getAttribute(self::FIELD_DISCOUNT);
    }

    /**
     * Get the tax of the item.
     *
     * @return float
     */
    public function getTax() : float
    {
        return $this->getAttribute(self::FIELD_TAX);
    }

    /**
     * Get the subtotal of the item.
     *
     * @return float
     */
    public function getSubtotal() : float
    {
        return $this->getAttribute(self::FIELD_SUBTOTAL);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the order Id.
     *
     * @param int $id
     * @return $this
     */
    public function setOrderId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_ORDER_ID, $id);
    }

    /**
     * Set the product Id.
     *
     * @param int $id
     * @return $this
     */
    public function setProductId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_PRODUCT_ID, $id);
    }

    /**
     * Set the quantity of the item.
     *
     * @param int $n
     * @return $this
     */
    public function setQty(int $n) : self
    {
        return $this->setAttribute(self::FIELD_QTY, $n);
    }

    /**
     * Set the price of the item.
     *
     * @param float $price
     * @return $this
     */
    public function setPrice(float $price) : self
    {
        return $this->setAttribute(self::FIELD_PRICE, $price);
    }

    /**
     * Set the discount for the item.
     *
     * @param float $amount
     * @return $this
     */
    public function setDiscount(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_DISCOUNT, $amount);
    }

    /**
     * Set the tax for the item.
     *
     * @param float $amount
     * @return $this
     */
    public function setTax(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_TAX, $amount);
    }

    /**
     * Set the subtotal of the item.
     *
     * @param float $sum
     * @return $this
     */
    public function setSubtotal(float $sum) : self
    {
        return $this->setAttribute(self::FIELD_SUBTOTAL, $sum);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope items by order.
     *
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeOfOrder($query, int $id)
    {
        return $query->where(self::FIELD_ORDER_ID, $id);
    }

    /**
     * Scope items by product Id.
     *
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeOfProduct($query, int $id)
    {
        return $query->where(self::FIELD_PRODUCT_ID, $id);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The order that the item belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order()
    {
        return $this->belongsTo(
            Order::class,
            self::FIELD_ORDER_ID,
            Order::FIELD_PK
        );
    }

    /**
     * The product that the item belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(
            Product::class,
            Fields::PK,
            Fields::PRODUCT_ID
        );
    }
}
