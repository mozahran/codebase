<?php

namespace App;

use App\Events\CommentCreated;
use App\Events\CommentDeleted;
use App\Events\CommentUpdated;
use App\Exceptions\InvalidStatusException;
use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'comments';

    const FIELD_PK = 'id';
    const FIELD_USER_ID = 'user_id';
    const FIELD_POST_ID = 'post_id';
    const FIELD_TEXT = 'text';
    const FIELD_STATUS = 'status';

    const STATUS_ALL = null;
    const STATUS_PENDING = 'pending';
    const STATUS_APPROVED = 'approved';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_USER_ID,
        self::FIELD_POST_ID,
        self::FIELD_TEXT,
        self::FIELD_STATUS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_USER_ID => 'integer',
        self::FIELD_POST_ID => 'integer',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => CommentCreated::class,
        'updated' => CommentUpdated::class,
        'deleted' => CommentDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the product Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the user Id.
     *
     * @return int
     */
    public function getUserId() : int
    {
        return $this->getAttribute(self::FIELD_USER_ID);
    }

    /**
     * Get the post Id.
     *
     * @return int
     */
    public function getPostId() : int
    {
        return $this->getAttribute(self::FIELD_POST_ID);
    }

    /**
     * Get the text of the comment.
     *
     * @return string
     */
    public function getText() : string
    {
        return $this->getAttribute(self::FIELD_TEXT);
    }

    /**
     * Get the status of the comment.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the comment is approved.
     *
     * @return bool
     */
    public function isApproved() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_APPROVED;
    }

    /**
     * Check if the comment is pending.
     *
     * @return bool
     */
    public function isPending() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_PENDING;
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the user Id.
     *
     * @param int $id
     * @return $this
     */
    public function setUserId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_USER_ID, $id);
    }

    /**
     * Set the post Id.
     *
     * @param int $id
     * @return $this
     */
    public function setPostId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_POST_ID, $id);
    }

    /**
     * Set the text of the comment.
     *
     * @param string $text
     * @return $this
     */
    public function setText(string $text) : self
    {
        return $this->setAttribute(self::FIELD_TEXT, $text);
    }

    /**
     * Set the status of the comment.
     *
     * @param string $status
     *
     * @return $this
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function setStatus(string $status) : self
    {
        if (in_array($status, [
            self::STATUS_APPROVED,
            self::STATUS_PENDING,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid comment status '%s'", $status));
        }

        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope comments by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_PENDING,
            self::STATUS_APPROVED
        ])) {
            throw new InvalidStatusException(sprintf("Invalid comment status '%s'"));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope approved comments.
     *
     * @param $query
     * @return mixed
     */
    public function scopeApproved($query)
    {
        return $query->where(self::FIELD_STATUS, self::STATUS_APPROVED);
    }

    /**
     * Scope pending comments.
     *
     * @param $query
     * @return mixed
     */
    public function scopePending($query)
    {
        return $query->where(self::FIELD_STATUS, self::STATUS_PENDING);
    }

    /**
     * Scope comment by post.
     *
     * @param $query
     * @param int $id
     * @return mixed
     */
    public function scopeOfPost($query, int $id)
    {
        return $query->where(self::FIELD_POST_ID, $id);
    }

    // ----------------------------------------------------------------------
    // Relationship
    // ----------------------------------------------------------------------

    /**
     * The post that the comment belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(
            Post::class,
            self::FIELD_POST_ID,
            self::FIELD_PK
        );
    }

    /**
     * The user that the comment belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            Post::class,
            self::FIELD_USER_ID,
            self::FIELD_PK
        );
    }

    /**
     * The images that belong to the comment.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function images()
    {
        return $this
            ->morphMany(Image::class, Image::FIELD_MORPHS)
            ->where(Image::FIELD_IS_DEFAULT, Image::NOT_DEFAULT);
    }
}
