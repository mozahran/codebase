<?php

namespace App;

use Illuminate\Database\Eloquent\Relations\Pivot;

class CouponUser extends Pivot
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'coupon_user';

    const FIELD_COUPON_ID = 'coupon_id';
    const FIELD_ORDER_ID = 'order_id';
    const FIELD_USER_ID = 'user_id';
}