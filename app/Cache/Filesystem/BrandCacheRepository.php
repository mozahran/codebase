<?php

namespace App\Cache\Filesystem;

use App\Repositories\Interfaces\BrandRepositoryInterface;

class BrandCacheRepository
{
    const COUNT_ALL_KEY = 'brands.all.count';
    const COUNT_FEATURED_KEY = 'brands.featured.count';
    const COUNT_NOT_FEATURED_KEY = 'brands.not-featured.count';

    /**
     * @var \App\Repositories\Interfaces\BrandRepositoryInterface
     */
    private $repository;

    /**
     * BrandCacheRepository constructor.
     *
     * @param \App\Repositories\Interfaces\BrandRepositoryInterface $repository
     */
    public function __construct(BrandRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Set the database repository.
     *
     * @param \App\Repositories\Interfaces\BrandRepositoryInterface $repository
     */
    public function setRepository(BrandRepositoryInterface $repository)
    {
        $this->repository = $repository;
    }

    /**
     * Get the repository.
     *
     * @return \App\Repositories\BrandRepository
     */
    public function getRepository() : BrandRepositoryInterface
    {
        return $this->repository;
    }

    /**
     * @inheritDoc
     */
    public function count() : int
    {
        return app('cache')->rememberForever(self::COUNT_ALL_KEY, function () {
            return $this->repository->count();
        });
    }

    /**
     * @inheritDoc
     */
    public function countFeaturedBrands() : int
    {
        return app('cache')->rememberForever(self::COUNT_FEATURED_KEY, function () {
            return $this->repository->countFeaturedBrands();
        });
    }

    /**
     * @inheritDoc
     */
    public function countNotFeaturedBrands() : int
    {
        return app('cache')->rememberForever(self::COUNT_NOT_FEATURED_KEY, function () {
            return $this->repository->countNotFeaturedBrands();
        });
    }

    public function forgetCachedCounts()
    {
        app('cache')->forget(self::COUNT_ALL_KEY);
        app('cache')->forget(self::COUNT_FEATURED_KEY);
        app('cache')->forget(self::COUNT_NOT_FEATURED_KEY);
    }
}