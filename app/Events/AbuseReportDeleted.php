<?php

namespace App\Events;

use App\Support;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class AbuseReportDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\Support
     */
    private $support;

    /**
     * Create a new event instance.
     *
     * @param \App\Support $support
     */
    public function __construct(Support $support)
    {
        $this->support = $support;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['support.abuse-reports.deleted'];
    }
}
