<?php

namespace App\Events;

use App\TaxRate;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TaxRateDeleted
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\TaxRate
     */
    private $taxRate;

    /**
     * Create a new event instance.
     *
     * @param \App\TaxRate $taxRate
     */
    public function __construct(TaxRate $taxRate)
    {
        $this->taxRate = $taxRate;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['taxRates.deleted'];
    }
}
