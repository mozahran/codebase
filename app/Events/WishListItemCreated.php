<?php

namespace App\Events;

use App\WishListItem;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class WishListItemCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * @var \App\WishListItem
     */
    private $item;

    /**
     * Create a new event instance.
     *
     * @param \App\WishListItem $item
     */
    public function __construct(WishListItem $item)
    {
        $this->item = $item;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return ['items.created'];
    }
}
