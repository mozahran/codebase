<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'images';

    const FIELD_PK = 'id';
    const FIELD_MORPHS = 'imageable';
    const FIELD_NAME = 'name';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_IMAGE_URL = 'image_url';
    const FIELD_IMAGE_PATH = 'image_path';
    const FIELD_THUMB_URL = 'thumb_url';
    const FIELD_THUMB_PATH = 'thumb_path';
    const FIELD_IS_DEFAULT = 'is_default';

    const FIELD_IMAGEABLE_ID = 'imageable_id';
    const FIELD_IMAGEABLE_TYPE = 'imageable_type';

    const DEFAULT = 1;
    const NOT_DEFAULT = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_IMAGEABLE_ID,
        self::FIELD_IMAGEABLE_TYPE,
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_IMAGE_URL,
        self::FIELD_IMAGE_PATH,
        self::FIELD_THUMB_URL,
        self::FIELD_THUMB_PATH,
        self::FIELD_IS_DEFAULT,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_IS_DEFAULT => 'boolean',
    ];

    public $timestamps = false;

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the image Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the name of the image.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the description of the image.
     *
     * @return string
     */
    public function getDescription() : string
    {
        return $this->getAttribute(self::FIELD_DESCRIPTION);
    }

    /**
     * Get the image url.
     *
     * @return string
     */
    public function getImageUrl() : string
    {
        return $this->getAttribute(self::FIELD_IMAGE_URL) ?: get_default_image();
    }

    /**
     * Get the image path.
     *
     * @return mixed
     */
    public function getImagePath() : string
    {
        return $this->getAttribute(self::FIELD_IMAGE_PATH);
    }

    /**
     * Get the thumbnail url.
     *
     * @return string
     */
    public function getThumbUrl() : string
    {
        return $this->getAttribute(self::FIELD_THUMB_URL) ?: get_default_image();
    }

    /**
     * Get the thumbnail path.
     *
     * @return string
     */
    public function getThumbPath() : string
    {
        return $this->getAttribute(self::FIELD_THUMB_PATH);
    }

    /**
     * Check if the image is default.
     *
     * @return bool
     */
    public function isDefault() : bool
    {
        return $this->getAttribute(self::FIELD_IS_DEFAULT);
    }

    /**
     * Check if the image is not default.
     *
     * @return bool
     */
    public function isNotDefault() : bool
    {
        return ! $this->getAttribute(self::FIELD_IS_DEFAULT);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the name of the image.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the description of the image.
     *
     * @param string $text
     * @return $this
     */
    public function setDescription(string $text) : self
    {
        return $this->setAttribute(self::FIELD_DESCRIPTION, $text);
    }

    /**
     * Set the image url.
     *
     * @param string $url
     * @return $this
     */
    public function setImageUrl(string $url) : self
    {
        return $this->setAttribute(self::FIELD_IMAGE_URL, $url);
    }

    /**
     * Set the image path.
     *
     * @param string $path
     * @return $this
     */
    public function setImagePath(string $path) : self
    {
        return $this->setAttribute(self::FIELD_IMAGE_PATH, $path);
    }

    /**
     * Set the thumbnail url.
     *
     * @param string $url
     * @return $this
     */
    public function setThumbUrl(string $url) : self
    {
        return $this->setAttribute(self::FIELD_THUMB_URL, $url);
    }

    /**
     * Set the thumbnail path.
     *
     * @param string $path
     * @return $this
     */
    public function setThumbPath(string $path) : self
    {
        return $this->setAttribute(self::FIELD_THUMB_PATH, $path);
    }

    /**
     * Set the image as default/not default.
     *
     * @param int $status
     * @return $this
     */
    public function setDefault(int $status) : self
    {
        return $this->setAttribute(self::FIELD_IS_DEFAULT, (bool) $status);
    }

    /**
     * Set the image as not default.
     *
     * @return $this
     */
    public function setNotDefault() : self
    {
        return $this->setDefault(self::NOT_DEFAULT);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------
}
