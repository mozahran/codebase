<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TermRelationship extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'term_relationships';

    const FIELD_OBJECT_ID = 'object_id';
    const FIELD_OBJECT_TYPE = 'object_type';
    const FIELD_TERM_TAXONOMY_ID = 'term_taxonomy_id';
    const FIELD_TERM_ORDER = 'term_order';

    public $incrementing = false;

    public $timestamps = false;

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    public function getObjectId() : int
    {
        return $this->getAttribute(self::FIELD_OBJECT_ID);
    }

    public function getObjectType() : string
    {
        return $this->getAttribute(self::FIELD_OBJECT_TYPE);
    }

    public function getTermTaxonomyId() : int
    {
        return $this->getAttribute(self::FIELD_TERM_TAXONOMY_ID);
    }

    public function getOrder() : int
    {
        return $this->getAttribute(self::FIELD_TERM_ORDER);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    public function setObjectId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_OBJECT_ID, $id);
    }

    public function setObjectType(string $type) : self
    {
        return $this->setAttribute(self::FIELD_OBJECT_TYPE, $type);
    }

    public function setTermTaxonomyId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_TERM_TAXONOMY_ID, $id);
    }

    public function setOrder(int $n) : self
    {
        return $this->setAttribute(self::FIELD_TERM_ORDER,  $n);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    public function termTaxonomy()
    {
        return $this->belongsTo(
            TermTaxonomy::class,
            self::FIELD_TERM_TAXONOMY_ID,
            TermTaxonomy::FIELD_PK
        );
    }
}
