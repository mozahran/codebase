<?php

namespace App\Actions\User;

use App\User;
use App\Repositories\UserRepository;

class CreateUser
{
    private $model;
    private $repository;

    public function __construct(User $model, UserRepository $repository)
    {
        $this->model = $model;
        $this->repository = $repository;
    }

    /**
     * @param $data
     *
     * @return \App\User|bool
     * @throws \Exception
     */
    public function execute($data)
    {
        if ($this->repository->emailExists($data['email'])) {
            throw new \Exception("Email exists in the database", 1);
        }

        /** @var User $user */
        $user = new $this->model;

        $user
            ->setName($data[User::FIELD_NAME])
            ->setEmail($data[User::FIELD_EMAIL])
            ->setPassword($data[User::FIELD_PASSWORD])
            ->setApiToken($data[User::FIELD_API_TOKEN])
        ;

        if (isset($data[User::FIELD_IS_ACTIVE])) {
            $data[User::FIELD_IS_ACTIVE] ? $user->setActive() : $user->setInactive();
        }

        if (isset($data[User::FIELD_IS_SUSPENDED])) {
            $data[User::FIELD_IS_SUSPENDED] ? $user->setSuspended() : $user->setNotSuspended();
        }

        if ($this->repository->save($user)) {
            return $user;
        }

        return false;
    }
}