<?php

namespace App;

use App\Events\SubscriptionPlanCreated;
use App\Events\SubscriptionPlanDeleted;
use App\Events\SubscriptionPlanUpdated;
use App\Exceptions\InvalidStatusException;
use Illuminate\Database\Eloquent\Model;

class SubscriptionPlan extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'subscription_plans';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_FEES = 'fees';
    const FIELD_DURATION = 'duration';
    const FIELD_STATUS = 'status';

    const STATUS_ALL = null;
    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_FEES,
        self::FIELD_DURATION,
        self::FIELD_STATUS,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_FEES => 'decimal',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => SubscriptionPlanCreated::class,
        'updated' => SubscriptionPlanUpdated::class,
        'deleted' => SubscriptionPlanDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the subscription plan Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the name of the subscription plan.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the fees that the user has paid for this subscription.
     *
     * @return float
     */
    public function getFees() : float
    {
        return $this->getAttribute(self::FIELD_FEES);
    }

    /**
     * Get the plan duration.
     *
     * @return int
     */
    public function getDuration() : int
    {
        return $this->getAttribute(self::FIELD_DURATION);
    }

    /**
     * Get the status of the country.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the country is enabled.
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_ENABLED;
    }

    /**
     * Check if the country is disabled.
     *
     * @return bool
     */
    public function isDisabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_DISABLED;
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the name of the subscription plan.
     *
     * @param string $name
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the fees that the user has paid for this subscription.
     *
     * @param float $amount
     * @return $this
     */
    public function setFees(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_FEES, $amount);
    }

    /**
     * Set the duration of the plan.
     *
     * @param int $duration
     * @return $this
     */
    public function setDuration(int $duration) : self
    {
        return $this->setAttribute(self::FIELD_DURATION, $duration);
    }

    /**
     * Set the subscription plan as active.
     * 
     * @param $status
     * @return $this
     */
    public function setActive($status = self::STATUS_ENABLED) : self
    {
        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the subscription plan as inactive.
     *
     * @return $this
     */
    public function setInactive() : self
    {
        return $this->setActive(self::STATUS_DISABLED);
    }

    /**
     * Set the status of the plan.
     *
     * @param string $status
     * @return $this
     */
    public function setStatus(string $status) : self
    {
        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the status as enabled.
     *
     * @return $this
     */
    public function setEnabled() : self
    {
        return $this->setStatus(self::STATUS_ENABLED);
    }

    /**
     * Set the status as disabled.
     *
     * @return $this
     */
    public function setDisabled() : self
    {
        return $this->setStatus(self::STATUS_DISABLED);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope plans by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid subscription plan status '%s'", $status));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope enabled plans.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeEnabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    /**
     * Scope disabled plans.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeDisabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The subscriptions that belong to the subscription plan.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function subscriptions()
    {
        return $this->hasMany(
            Subscription::class,
            Subscription::FIELD_PLAN_ID,
            self::FIELD_PK
        );
    }
}
