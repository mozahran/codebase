<?php

namespace App;

use App\Events\SupportMessageDeleted;
use App\Events\SupportMessageSent;
use App\Exceptions\InvalidStatusException;
use App\Exceptions\InvalidTypeException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Support extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'support';

    const FIELD_PK = 'id';
    const FIELD_PARENT_ID = 'parent_id';
    const FIELD_SENDER_ID = 'sender_id';
    const FIELD_ABUSER_ID = 'abuser_id';
    const FIELD_ABUSABLE_ID = 'abusable_id';
    const FIELD_ABUSABLE_TYPE = 'abusable_type';
    const FIELD_FULLNAME = 'fullname';
    const FIELD_PHONE_NUMBER = 'phone_number';
    const FIELD_EMAIL = 'email';
    const FIELD_SUBJECT = 'subject';
    const FIELD_CONTENT = 'content';
    const FIELD_READ_AT = 'read_at';

    const KEYWORD_MORPH_ABUSABLE = 'abusable';

    const STATUS_ALL = -1;
    const STATUS_READ = 0;
    const STATUS_UNREAD = 1;

    const TYPE_ALL = null;
    const TYPE_ABUSE_REPORT = 'abuse-report';
    const TYPE_SUPPORT_MESSAGE = 'support-message';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_PARENT_ID,
        self::FIELD_SENDER_ID,
        self::FIELD_ABUSER_ID,
        self::FIELD_ABUSABLE_ID,
        self::FIELD_ABUSABLE_TYPE,
        self::FIELD_FULLNAME,
        self::FIELD_PHONE_NUMBER,
        self::FIELD_EMAIL,
        self::FIELD_SUBJECT,
        self::FIELD_CONTENT,
        self::FIELD_READ_AT,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_PARENT_ID => 'integer',
        self::FIELD_SENDER_ID => 'integer',
        self::FIELD_ABUSER_ID => 'integer',
        self::FIELD_ABUSABLE_ID => 'integer',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        self::FIELD_READ_AT
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => SupportMessageSent::class,
        'deleted' => SupportMessageDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the support message Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the parent message Id.
     *
     * @return int
     */
    public function getParentId() : int
    {
        return $this->getAttribute(self::FIELD_PARENT_ID);
    }

    /**
     * Get the sender Id.
     *
     * @return int
     */
    public function getSenderId() : int
    {
        return $this->getAttribute(self::FIELD_SENDER_ID);
    }

    /**
     * Get the abuser Id.
     *
     * @return int
     */
    public function getAbuserId() : int
    {
        return $this->getAttribute(self::FIELD_ABUSER_ID);
    }

    /**
     * Get the abusable Id.
     *
     * @return int
     */
    public function getAbusableId() : int
    {
        return $this->getAttribute(self::FIELD_ABUSABLE_ID);
    }

    /**
     * Get the type of the abusable thing.
     *
     * @return string
     */
    public function getAbusableType() : string
    {
        return $this->getAttribute(self::FIELD_ABUSABLE_TYPE);
    }

    /**
     * Get the full name of the (unregistered) sender.
     *
     * @return string
     */
    public function getFullname() : string
    {
        return $this->getAttribute(self::FIELD_FULLNAME);
    }

    /**
     * Get the phone number of the (unregistered) sender.
     *
     * @return string
     */
    public function getPhoneNumber() : string
    {
        return $this->getAttribute(self::FIELD_PHONE_NUMBER);
    }

    /**
     * Get the email of the (unregistered) sender.
     *
     * @return string
     */
    public function getEmail() : string
    {
        return $this->getAttribute(self::FIELD_PHONE_NUMBER);
    }

    /**
     * Get the subject of the message.
     *
     * @return string
     */
    public function getSubject() : string
    {
        return $this->getAttribute(self::FIELD_SUBJECT);
    }

    /**
     * Get the content of the message.
     *
     * @return string
     */
    public function getContent() : string
    {
        return $this->getAttribute(self::FIELD_CONTENT);
    }

    /**
     * Get the time at which the message was read by the recipient.
     *
     * @return Carbon
     */
    public function getReadAt() : Carbon
    {
        return $this->getAttribute(self::FIELD_READ_AT);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the parent message Id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setParentId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_PARENT_ID, $id);
    }

    /**
     * Set the sender Id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setSenderId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_SENDER_ID, $id);
    }

    /**
     * Set the abuser Id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setAbuserId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_ABUSER_ID, $id);
    }

    /**
     * Set the abusable Id.
     *
     * @param int $id
     *
     * @return $this
     */
    public function setAbusableId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_ABUSABLE_ID, $id);
    }

    /**
     * Set the type of the abusable thing.
     *
     * @param string $type
     *
     * @return $this
     */
    public function setAbusableType(string $type) : self
    {
        return $this->setAttribute(self::FIELD_ABUSABLE_TYPE, $type);
    }

    /**
     * Set the full name of the (unregistered) sender.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setFullname(string $name) : self
    {
        return $this->setAttribute(self::FIELD_FULLNAME, $name);
    }

    /**
     * Set the phone number of the (unregistered) sender.
     *
     * @param string $number
     *
     * @return $this
     */
    public function setPhoneNumber(string $number) : self
    {
        return $this->setAttribute(self::FIELD_PHONE_NUMBER, $number);
    }

    /**
     * Set the email of the (unregistered) sender.
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email) : self
    {
        return $this->setAttribute(self::FIELD_PHONE_NUMBER, $email);
    }

    /**
     * Set the subject of the message.
     *
     * @param string $subject
     *
     * @return $this
     */
    public function setSubject(string $subject) : self
    {
        return $this->setAttribute(self::FIELD_SUBJECT, $subject);
    }

    /**
     * Set the content of the message.
     *
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content) : self
    {
        return $this->setAttribute(self::FIELD_CONTENT, $content);
    }

    /**
     * Set the time at which the message was read by the recipient.
     *
     * @param Carbon $time
     *
     * @return $this
     */
    public function setReadAt(Carbon $time) : self
    {
        return $this->setAttribute(self::FIELD_READ_AT, $time);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope support messages/abuse reports by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_READ,
            self::STATUS_UNREAD,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid status '%s'", $status));
        }

        if ($status === self::STATUS_READ) {
            return $this->scopeRead($query);
        } else {
            return $this->scopeUnread($query);
        }
    }

    /**
     * Scope read messages.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeRead($query)
    {
        return $query->whereNotNull(self::FIELD_READ_AT);
    }

    /**
     * Scope unread messages.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeUnread($query)
    {
        return $query->whereNull(self::FIELD_READ_AT);
    }

    /**
     * Scope support messages/abuse reports by type.
     *
     * @param        $query
     * @param string $type
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidTypeException
     */
    public function scopeOfType($query, string $type)
    {
        if ($type === self::TYPE_ALL) {
            return $query;
        }

        if ( ! in_array($type, [
            self::TYPE_ABUSE_REPORT,
            self::TYPE_SUPPORT_MESSAGE,
        ])) {
            throw new InvalidTypeException(sprintf("Invalid type '%s'", $type));
        }

        switch ($type) {
            case self::TYPE_ABUSE_REPORT:
                return $this->scopeAbuseReports($query);
                break;

            case self::TYPE_SUPPORT_MESSAGE:
                return $this->scopeMessages($query);
                break;
        }
    }

    /**
     * Scope abuse reports.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeAbuseReports($query)
    {
        return $query->whereNotNull(self::FIELD_ABUSER_ID);
    }

    /**
     * Scope abuse reports.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeMessages($query)
    {
        return $query->whereNull(self::FIELD_ABUSER_ID);
    }

    /**
     * Scope only conversations (not replies).
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeConversations($query)
    {
        return $query
            ->whereNull(self::FIELD_PARENT_ID)
            ->orWhere(self::FIELD_PARENT_ID, 0);
    }

    /**
     * Scope replies of a specific message.
     *
     * @param     $query
     * @param int $supportMessageId
     *
     * @return mixed
     */
    public function scopeRepliesOf($query, int $supportMessageId)
    {
        return $query->where(self::FIELD_PARENT_ID, $supportMessageId);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The parent message that this message belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(
            self::class,
            self::FIELD_PARENT_ID,
            self::FIELD_PK
        );
    }

    /**
     * The user that the message belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->belongsTo(
            User::class,
            self::FIELD_SENDER_ID,
            User::FIELD_PK
        );
    }

    /**
     * The user that was reported (abuser) by the sender.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function abuser()
    {
        return $this->belongsTo(
            User::class,
            self::FIELD_ABUSER_ID,
            User::FIELD_PK
        );
    }

    /**
     * The abusable thing.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function abusable()
    {
        return $this->morphTo();
    }
}
