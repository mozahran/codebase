<?php

namespace App\Codebase\Constants;

abstract class Tables
{
    const USERS = 'users';
    const CATEGORIES = 'categories';
    const BRANDS = 'brands';
    const PRODUCTS = 'products';
    const POSTS = 'posts';
}