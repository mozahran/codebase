<?php

namespace App\Codebase\Constants;

abstract class Fields
{
    const PK = 'id';
    const NAME = 'name';
    const TITLE = 'title';
    const DESCRIPTION = 'description';
    const CONTENT = 'content';
    const STATUS = 'status';
    const PRICE = 'price';
    const FEES = 'fees';
    const QTY = 'qty';
    const QUANTITY = self::QTY;
    const VAT = 'vat';

    // ----------------------------------------------------------------------
    // Flags
    // ----------------------------------------------------------------------

    const IS_DEFAULT = 'is_default';

    // ----------------------------------------------------------------------
    // DateTime
    // ----------------------------------------------------------------------

    const CREATED_AT = 'created_at';
    const UPDATED_AT = 'updated_at';
    const DELETED_AT = 'deleted_at';
    const EXPIRES_AT = 'expires_at';
    const EXPIRED_AT = 'expired_at';
    const SUBSCRIBED_AT = 'subscribed_at';

    // ----------------------------------------------------------------------
    // Foreign Keys
    // ----------------------------------------------------------------------

    const USER_ID = 'user_id';
    const CATEGORY_ID = 'category_id';
    const BRAND_ID = 'brand_id';
    const POST_ID = 'post_id';
    const PRODUCT_ID = 'product_id';
    const CITY_ID = 'city_id';
    const COUNTRY_ID = 'country_id';
}