<?php

namespace App\Codebase\Constants;

abstract class Statuses
{
    const ALL = null;
    const ACTIVE = 'active';
    const INACTIVE = 'inactive';
    const DRAFT = 'draft';
    const PENDING = 'pending';
    const PUBLISHED = 'published';
    const FEATURED = 'featured';
}