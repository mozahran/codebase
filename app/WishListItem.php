<?php

namespace App;

use App\Codebase\Constants\Fields;
use App\Events\WishListItemCreated;
use App\Events\WishListItemDeleted;
use Illuminate\Database\Eloquent\Model;

class WishListItem extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'wishlists';

    const FIELD_PK = 'id';
    const FIELD_PRODUCT_ID = 'product_id';
    const FIELD_USER_ID = 'user_id';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = self::TABLE_NAME;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_PRODUCT_ID,
        self::FIELD_USER_ID,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_PRODUCT_ID => 'integer',
        self::FIELD_USER_ID => 'integer',
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => WishListItemCreated::class,
        'deleted' => WishListItemDeleted::class,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'user',
        'product',
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the wish list item Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the product Id.
     *
     * @return int
     */
    public function getProductId() : int
    {
        return $this->getAttribute(self::FIELD_PRODUCT_ID);
    }

    /**
     * Get the user Id.
     *
     * @return int
     */
    public function getUserId() : int
    {
        return $this->getAttribute(self::FIELD_USER_ID);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the product Id.
     *
     * @param int $id
     * @return $this
     */
    public function setProductId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_PRODUCT_ID, $id);
    }

    /**
     * Set the user Id.
     *
     * @param int $id
     * @return $this
     */
    public function setUserId(int $id) : self
    {
        return $this->setAttribute(self::FIELD_USER_ID, $id);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope wish-listed items by product.
     *
     * @param $query
     * @param int $productId
     * @return mixed
     */
    public function scopeOfProduct($query, int $productId)
    {
        return $query->where(self::FIELD_PRODUCT_ID, $productId);
    }

    /**
     * Scope wish-listed items by user.
     *
     * @param $query
     * @param int $userId
     * @return mixed
     */
    public function scopeOfUser($query, int $userId)
    {
        return $query->where(self::FIELD_USER_ID, $userId);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The user that the wishlist item belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(
            User::class,
            self::FIELD_USER_ID,
            User::FIELD_PK
        );
    }

    /**
     * The product that the wishlist item belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function product()
    {
        return $this->hasOne(
            Product::class,
            Fields::PK,
            self::FIELD_PRODUCT_ID
        );
    }
}
