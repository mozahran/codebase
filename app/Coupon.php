<?php

namespace App;

use App\Events\CouponCreated;
use App\Events\CouponDeleted;
use App\Events\CouponUpdated;
use App\Exceptions\InvalidStatusException;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'coupons';

    const FIELD_PK = 'id';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_CODE = 'code';
    const FIELD_DISCOUNT_TYPE = 'discount_type';
    const FIELD_DISCOUNT_AMOUNT = 'discount_amount';
    const FIELD_USES_PER_COUPON = 'uses_per_coupon';
    const FIELD_USES_PER_USER = 'uses_per_user';
    const FIELD_USAGE_COUNT = 'usage_count';
    const FIELD_FREE_SHIPPING = 'free_shipping';
    const FIELD_PRODUCT_IDS = 'product_ids';
    const FIELD_EXCLUDED_PRODUCT_IDS = 'excluded_product_ids';
    const FIELD_PRODUCT_CATEGORIES = 'product_categories';
    const FIELD_EXCLUDED_PRODUCT_CATEGORIES = 'excluded_product_categories';
    const FIELD_STATUS = 'status';
    const FIELD_EXPIRES_AT = 'expires_at';

    const DISCOUNT_TYPE_ALL = -1;
    const DISCOUNT_TYPE_AMOUNT_OFF_ORDER_TOTAL = 'amount_off_order_total';
    const DISCOUNT_TYPE_AMOUNT_OFF_EACH_ITEM = 'amount_off_each_item';
    const DISCOUNT_TYPE_PERCENTAGE_OFF_EACH_ITEM = 'percentage_off_each_item';

    const STATUS_ENABLED = 'enabled';
    const STATUS_DISABLED = 'disabled';

    const VALIDITY_ALL = -1;
    const VALIDITY_VALID = 1;
    const VALIDITY_EXPIRED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_PK,
        self::FIELD_DESCRIPTION,
        self::FIELD_CODE,
        self::FIELD_DISCOUNT_TYPE,
        self::FIELD_DISCOUNT_AMOUNT,
        self::FIELD_USES_PER_COUPON,
        self::FIELD_USES_PER_USER,
        self::FIELD_USAGE_COUNT,
        self::FIELD_FREE_SHIPPING,
        self::FIELD_PRODUCT_IDS,
        self::FIELD_EXCLUDED_PRODUCT_IDS,
        self::FIELD_PRODUCT_CATEGORIES,
        self::FIELD_EXCLUDED_PRODUCT_CATEGORIES,
        self::FIELD_IS_ENABLED,
        self::FIELD_EXPIRES_AT,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_DISCOUNT_AMOUNT => 'decimal',
        self::FIELD_USES_PER_COUPON => 'integer',
        self::FIELD_USES_PER_USER => 'integer',
        self::FIELD_USAGE_COUNT => 'integer',
        self::FIELD_FREE_SHIPPING => 'boolean',
        self::FIELD_PRODUCT_IDS => 'array',
        self::FIELD_EXCLUDED_PRODUCT_IDS => 'array',
        self::FIELD_PRODUCT_CATEGORIES => 'array',
        self::FIELD_EXCLUDED_PRODUCT_CATEGORIES => 'array',
        self::FIELD_IS_ENABLED => 'boolean',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        self::FIELD_EXPIRES_AT
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => CouponCreated::class,
        'updated' => CouponUpdated::class,
        'deleted' => CouponDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the coupon Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the description of the coupon.
     *
     * @return string
     */
    public function getDescription() : string
    {
        return $this->getAttribute(self::FIELD_DESCRIPTION);
    }

    /**
     * Get the code of the coupon.
     *
     * @return string
     */
    public function getCode() : string
    {
        return $this->getAttribute(self::FIELD_CODE);
    }

    /**
     * Get the discount type of the coupon.
     *
     * @return string
     */
    public function getDiscountType() : string
    {
        return $this->getAttribute(self::FIELD_DISCOUNT_TYPE);
    }

    /**
     * Get the discount amount of the coupon.
     *
     * @return float
     */
    public function getDiscountAmount() : float
    {
        return $this->getAttribute(self::FIELD_DISCOUNT_AMOUNT);
    }

    /**
     * Get the number of uses allowed per coupon.
     *
     * @return int
     */
    public function getUsesPerCoupon() : int
    {
        return $this->getAttribute(self::FIELD_USES_PER_COUPON);
    }

    /**
     * Get the number of uses allowed for the user.
     *
     * @return int
     */
    public function getUsesPerUser() : int
    {
        return $this->getAttribute(self::FIELD_USES_PER_USER);
    }

    /**
     * Get the number of uses of the coupon.
     *
     * @return int
     */
    public function getUsageCount() : int
    {
        return $this->getAttribute(self::FIELD_USAGE_COUNT);
    }

    /**
     * Check if the coupon offers free shipping.
     *
     * @return bool
     */
    public function hasFreeShipping() : bool
    {
        return $this->getAttribute(self::FIELD_FREE_SHIPPING);
    }

    /**
     * Get an array of the product that are affected by the coupon.
     *
     * @return array
     */
    public function getProductIds() : array
    {
        $serializedStr = $this->getAttribute(self::FIELD_PRODUCT_IDS);

        return (array) unserialize($serializedStr);
    }

    /**
     * Get an array of the products that are excluded (not-affected by the coupon).
     *
     * @return array
     */
    public function getExcludedProductIds() : array
    {
        $serializedStr = $this->getAttribute(self::FIELD_EXCLUDED_PRODUCT_IDS);

        return (array) unserialize($serializedStr);
    }

    /**
     * Get an array of the categories that are affected by the coupon.
     *
     * @return array
     */
    public function getProductCategories() : array
    {
        $serializedStr = $this->getAttribute(self::FIELD_PRODUCT_CATEGORIES);

        return (array) unserialize($serializedStr);
    }

    /**
     * Get an array of the categories that  are excluded (not-affected by the coupon).
     *
     * @return array
     */
    public function getExcludedProductCategories() : array
    {
        $serializedStr = $this->getAttribute(self::FIELD_EXCLUDED_PRODUCT_CATEGORIES);

        return (array) unserialize($serializedStr);
    }

    /**
     * Get the status of the coupon.
     *
     * @return string
     */
    public function getStatus() : string
    {
        return $this->getAttribute(self::FIELD_STATUS);
    }

    /**
     * Check if the coupon is enabled.
     *
     * @return bool
     */
    public function isEnabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_ENABLED;
    }

    /**
     * Check if the coupon is disabled.
     *
     * @return bool
     */
    public function isDisabled() : bool
    {
        return strtolower($this->getStatus()) === self::STATUS_DISABLED;
    }

    /**
     * Get the expiration date of the coupon.
     *
     * @return null|Carbon
     */
    public function getExpiresAt() : ?Carbon
    {
        return $this->getAttribute(self::FIELD_EXPIRES_AT);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the description of the coupon.
     *
     * @param string $text
     *
     * @return $this
     */
    public function setDescription(string $text) : self
    {
        return $this->setAttribute(self::FIELD_DESCRIPTION, $text);
    }

    /**
     * Set the code of the coupon.
     *
     * @param string $code
     *
     * @return $this
     */
    public function setCode(string $code) : self
    {
        return $this->setAttribute(self::FIELD_CODE, $code);
    }

    /**
     * Set the discount type of the coupon.
     *
     * @param string $type
     *
     * @return $this
     */
    public function setDiscountType(string $type) : self
    {
        return $this->setAttribute(self::FIELD_DISCOUNT_TYPE, $type);
    }

    /**
     * Set the discount amount of the coupon.
     *
     * @param float $amount
     *
     * @return $this
     */
    public function setDiscountAmount(float $amount) : self
    {
        return $this->setAttribute(self::FIELD_DISCOUNT_AMOUNT, $amount);
    }

    /**
     * Set the number of uses allowed per coupon.
     *
     * @param int $n
     *
     * @return $this
     */
    public function setUsesPerCoupon(int $n) : self
    {
        return $this->setAttribute(self::FIELD_USES_PER_COUPON, $n);
    }

    /**
     * Set the number of uses allowed per user.
     *
     * @param int $n
     *
     * @return $this
     */
    public function setUsesPerUser(int $n) : self
    {
        return $this->setAttribute(self::FIELD_USES_PER_USER, $n);
    }

    /**
     * Set the number of uses of the coupon.
     *
     * @param int $n
     *
     * @return $this
     */
    public function setUsageCount(int $n) : self
    {
        return $this->setAttribute(self::FIELD_USAGE_COUNT, $n);
    }

    /**
     * Set the free shipping status of the coupon.
     *
     * @param int $status
     *
     * @return $this
     */
    public function setFreeShipping(int $status) : self
    {
        return $this->setAttribute(self::FIELD_FREE_SHIPPING, $status);
    }

    /**
     * Set the products that are to be affected by the coupon.
     *
     * @param array $ids
     *
     * @return $this
     */
    public function setProductIds(array $ids) : self
    {
        return $this->setAttribute(self::FIELD_PRODUCT_IDS, serialize($ids));
    }

    /**
     * Set the products that are excluded (not affected by the coupon).
     *
     * @param array $ids
     *
     * @return $this
     */
    public function setExcludedProductIds(array $ids) : self
    {
        return $this->setAttribute(self::FIELD_EXCLUDED_PRODUCT_IDS, serialize($ids));
    }

    /**
     * Set the categories that are to be affected by the coupon.
     *
     * @param array $ids
     *
     * @return $this
     */
    public function setProductCategories(array $ids) : self
    {
        return $this->setAttribute(self::FIELD_PRODUCT_CATEGORIES, serialize($ids));
    }

    /**
     * Set the categories that are excluded (not affected by the coupon).
     *
     * @param array $ids
     *
     * @return $this
     */
    public function setExcludedProductCategories(array $ids) : self
    {
        return $this->setAttribute(self::FIELD_EXCLUDED_PRODUCT_CATEGORIES, serialize($ids));
    }

    /**
     * Set the status of the coupon.
     *
     * @param string $status
     *
     * @return $this
     */
    public function setStatus(string $status) : self
    {
        return $this->setAttribute(self::FIELD_STATUS, $status);
    }

    /**
     * Set the status as enabled.
     *
     * @return $this
     */
    public function setEnabled() : self
    {
        return $this->setStatus(self::STATUS_ENABLED);
    }

    /**
     * Set the status as disabled.
     *
     * @return $this
     */
    public function setDisabled() : self
    {
        return $this->setStatus(self::STATUS_DISABLED);
    }

    /**
     * Set the expiration date of the coupon.
     *
     * @param Carbon $timestamp
     *
     * @return $this
     */
    public function setExpiresAt(Carbon $timestamp) : self
    {
        return $this->setAttribute(self::FIELD_EXPIRES_AT, $timestamp);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope coupons by discount type.
     *
     * @param        $query
     * @param string $type
     *
     * @return mixed
     */
    public function scopeOfDiscountType($query, string $type)
    {
        return $query->where(self::FIELD_DISCOUNT_TYPE, $type);
    }

    /**
     * Scope coupons by status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, string $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_ENABLED,
            self::STATUS_DISABLED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid coupon status '%s'", $status));
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope enabled coupons.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeEnabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    /**
     * Scope disabled coupons.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeDisabled($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_ENABLED);
    }

    /**
     * Scope coupons by validity status.
     *
     * @param        $query
     * @param string $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfValidityStatus($query, string $status)
    {
        if ($status === self::VALIDITY_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::VALIDITY_VALID,
            self::VALIDITY_EXPIRED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid coupon validity status '%s'", $status));
        }

        switch ($status) {
            case self::VALIDITY_VALID:
                return $this->scopeValid($query);
                break;
            case self::VALIDITY_EXPIRED:
                return $this->scopeExpired($query);
                break;
        }

        return $query->where(self::FIELD_STATUS, $status);
    }

    /**
     * Scope valid coupons.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeValid($query)
    {
        $now = Carbon::now();

        return $query->whereDate(self::FIELD_EXPIRES_AT, '>', $now);
    }

    /**
     * Scope expired coupons.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeExpired($query)
    {
        $now = Carbon::now();

        return $query->whereDate(self::FIELD_EXPIRES_AT, '<', $now);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The users that have used the coupon.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class)->using(CouponUser::class);
    }
}
