<?php

namespace App;

use App\Codebase\Constants\Fields;
use Carbon\Carbon;
use App\Events\UserCreated;
use App\Events\UserDeleted;
use App\Events\UserUpdated;
use Illuminate\Notifications\Notifiable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;
    use HasRolesAndAbilities;

    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'users';
    const FIELD_PK = 'id';
    const FIELD_COUNTRY_ID = 'country_id';
    const FIELD_CITY_ID = 'city_id';
    const FIELD_NAME = 'name';
    const FIELD_EMAIL = 'email';
    const FIELD_PASSWORD = 'password';
    const FIELD_PHONE_NUMBER = 'phone_number';
    const FIELD_REMEMBER_TOKEN = 'remember_token';
    const FIELD_API_TOKEN = 'api_token';
    const FIELD_IS_ACTIVE = 'is_active';
    const FIELD_IS_SUSPENDED = 'is_suspended';

    const STATUS_ALL = -1;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;

    const STATUS_SUSPENDED = 1;
    const STATUS_NOT_SUSPENDED = 0;

    const DEFAULT_COUNTRY = 0;
    const DEFAULT_CITY = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_EMAIL,
        self::FIELD_PASSWORD,
        self::FIELD_PHONE_NUMBER,
        self::FIELD_API_TOKEN,
        self::FIELD_IS_ACTIVE,
        self::FIELD_IS_SUSPENDED,
    ];

    protected $casts = [
        self::FIELD_IS_SUSPENDED => 'boolean',
        self::FIELD_IS_ACTIVE => 'boolean',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        self::FIELD_PASSWORD,
        self::FIELD_REMEMBER_TOKEN,
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => UserCreated::class,
        'updated' => UserUpdated::class,
        'deleted' => UserDeleted::class,
    ];

    /**
     * The relations to eager load on every query.
     *
     * @var array
     */
    protected $with = [
        'roles',
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the user Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the name of the user.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the email of the user.
     *
     * @return string
     */
    public function getEmail() : string
    {
        return $this->getAttribute(self::FIELD_EMAIL);
    }

    /**
     * Get the user phone number.
     *
     * @return string
     */
    public function getPhoneNumber() : string
    {
        return $this->getAttribute(self::FIELD_PHONE_NUMBER);
    }

    /**
     * Checks if the user is active.
     *
     * @return bool
     */
    public function isActive() : bool
    {
        return $this->getAttribute(self::FIELD_IS_ACTIVE);
    }

    /**
     * Checks if the user is active.
     *
     * @return bool
     */
    public function isSuspended() : bool
    {
        return $this->getAttribute(self::FIELD_IS_SUSPENDED);
    }

    /**
     * @param string $name
     *
     * @return bool
     */
    public function hasRole(string $name) : bool
    {
        return (bool) collect($this->roles)->where('name', $name)->count();
    }

    /**
     * Check if the user is an admin.
     *
     * @return bool
     */
    public function isAdmin() : bool
    {
        return $this->hasRole('admin');
    }

    /**
     * Check if the user is a moderator.
     *
     * @return bool
     */
    public function isModerator() : bool
    {
        return $this->hasRole('moderator');
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the name of the user.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the email of the user.
     *
     * @param string $email
     *
     * @return $this
     */
    public function setEmail(string $email) : self
    {
        return $this->setAttribute(self::FIELD_EMAIL, $email);
    }

    /**
     * Set the phone number of the user.
     *
     * @param string $phoneNumber
     *
     * @return $this
     */
    public function setPhoneNumber(string $phoneNumber) : self
    {
        return $this->setAttribute(self::FIELD_PHONE_NUMBER, $phoneNumber);
    }

    /**
     * Set the password of the user.
     *
     * @param $value
     *
     * @return string
     */
    public function setPassword($value) : self
    {
        return $this->setAttribute(self::FIELD_PASSWORD, bcrypt($value));
    }

    /**
     * Set the api token for the user.
     *
     * @param $value
     *
     * @return string
     */
    public function setApiToken($value) : self
    {
        return $this->setAttribute(self::FIELD_API_TOKEN, $value);
    }

    /**
     * Set the user as active.
     *
     * @return $this
     */
    public function setActive() : self
    {
        return $this->setAttribute(self::FIELD_IS_ACTIVE, true);
    }

    /**
     * Set the user as inactive.
     *
     * @return $this
     */
    public function setInactive() : self
    {
        return $this->setAttribute(self::FIELD_IS_ACTIVE, false);
    }

    /**
     * Set the user as suspended.
     *
     * @return $this
     */
    public function setSuspended() : self
    {
        return $this->setAttribute(self::FIELD_IS_SUSPENDED, true);
    }

    /**
     * Set the user as not-suspended.
     *
     * @return $this
     */
    public function setNotSuspended() : self
    {
        return $this->setAttribute(self::FIELD_IS_SUSPENDED, false);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope active users.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeActive($query)
    {
        return $query->where(self::FIELD_IS_ACTIVE, true);
    }

    /**
     * Scope inactive users.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeInactive($query)
    {
        return $query->where(self::FIELD_IS_ACTIVE, false);
    }

    /**
     * Scope suspended users.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeSuspended($query)
    {
        return $query->where(self::FIELD_IS_SUSPENDED, true);
    }

    /**
     * Scope users that are not suspended.
     *
     * @param $query
     *
     * @return mixed
     */
    public function scopeNotSuspended($query)
    {
        return $query->where(self::FIELD_IS_SUSPENDED, false);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The image that belongs to the post.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this
            ->morphOne(Image::class, Image::FIELD_MORPHS)
            ->where(Image::FIELD_IS_DEFAULT, Image::DEFAULT)
            ->withDefault([
                Image::FIELD_IMAGE_URL => get_default_image(),
                Image::FIELD_THUMB_URL => get_default_image(),
            ]);
    }

    /**
     * The products that belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(
            Product::class,
            Fields::USER_ID,
            Fields::PK
        );
    }

    /**
     * The posts that belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(
            Post::class,
            Post::FIELD_USER_ID,
            self::FIELD_PK
        );
    }

    /**
     * The orders that belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orders()
    {
        return $this->hasMany(
            Order::class,
            Order::FIELD_USER_ID,
            self::FIELD_PK
        );
    }

    /**
     * The invoices that belong to the user.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function invoices()
    {
        return $this->hasMany(
            Invoice::class,
            Invoice::FIELD_USER_ID,
            self::FIELD_PK
        );
    }

    /**
     * The coupons that the user have used.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function coupons()
    {
        return $this
            ->belongsToMany(Coupon::class, 'coupon_user')
            ->withTimestamps();
    }

    /**
     * The conversations that the user is involved in.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasManyThrough
     */
    public function conversations()
    {
        return $this->hasManyThrough(
            Conversation::class,
            ConversationUser::class,
            ConversationUser::FIELD_USER_ID,
            self::FIELD_PK,
            Conversation::FIELD_PK,
            ConversationUser::FIELD_CONVERSATION_ID
        );
    }

    /**
     * The subscriptions that belong to the user.
     *
     */
    public function subscriptions()
    {
        return $this->hasMany(
            Subscription::class,
            Subscription::FIELD_USER_ID,
            self::FIELD_PK
        );
    }

    /**
     * Valid subscriptions that belong to the user.
     *
     * @return $this
     */
    public function validSubscriptions()
    {
        return $this->subscriptions()->where(
            Subscription::FIELD_EXPIRES_AT,
            '>=',
            Carbon::now()
        );
    }

    /**
     * Expired subscriptions that belong to the user.
     *
     * @return $this
     */
    public function expiredSubscriptions()
    {
        return $this->subscriptions()->where(
            Subscription::FIELD_EXPIRES_AT,
            '<',
            Carbon::now()
        );
    }

    /**
     * The country that the user belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function country()
    {
        return $this->belongsTo(
            Country::class,
            self::FIELD_COUNTRY_ID,
            Country::FIELD_PK
        );
    }

    /**
     * The city that the user belongs to.
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function city()
    {
        return $this->belongsTo(
            City::class,
            self::FIELD_CITY_ID,
            City::FIELD_PK
        );
    }
}
