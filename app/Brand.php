<?php

namespace App;

use App\Events\BrandCreated;
use App\Events\BrandDeleted;
use App\Events\BrandUpdated;
use App\Exceptions\InvalidStatusException;
use App\Presenters\Brand\UrlPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    // ----------------------------------------------------------------------
    // Table Schema
    // ----------------------------------------------------------------------

    const TABLE_NAME = 'brands';

    const FIELD_PK = 'id';
    const FIELD_NAME = 'name';
    const FIELD_DESCRIPTION = 'description';
    const FIELD_IS_FEATURED = 'is_featured';

    const STATUS_ALL = -1;
    const STATUS_FEATURED = 1;
    const STATUS_NOT_FEATURED = 0;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::FIELD_NAME,
        self::FIELD_DESCRIPTION,
        self::FIELD_IS_FEATURED,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        self::FIELD_IS_FEATURED => 'boolean',
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'url'
    ];

    /**
     * The event map for the model.
     *
     * Allows for object-based events for native Eloquent events.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        'created' => BrandCreated::class,
        'updated' => BrandUpdated::class,
        'deleted' => BrandDeleted::class,
    ];

    // ----------------------------------------------------------------------
    // Getters
    // ----------------------------------------------------------------------

    /**
     * Get the brand Id.
     *
     * @return int
     */
    public function getId() : int
    {
        return $this->getAttribute(self::FIELD_PK);
    }

    /**
     * Get the name of the brand.
     *
     * @return string
     */
    public function getName() : string
    {
        return $this->getAttribute(self::FIELD_NAME);
    }

    /**
     * Get the description of the brand.
     *
     * @return string
     */
    public function getDescription() : string
    {
        return $this->getAttribute(self::FIELD_DESCRIPTION);
    }

    /**
     * Check if the brand is featured.
     *
     * @return bool
     */
    public function isFeatured() : bool
    {
        return $this->getAttribute(self::FIELD_IS_FEATURED);
    }

    /**
     * Check if the brand is not featured.
     *
     * @return bool
     */
    public function isNotFeatured() : bool
    {
        return ! $this->isFeatured();
    }

    /**
     * Get the url presenter for the model.
     *
     * @return \App\Presenters\Brand\UrlPresenter
     */
    public function getUrlAttribute()
    {
        return new UrlPresenter($this);
    }

    // ----------------------------------------------------------------------
    // Setters
    // ----------------------------------------------------------------------

    /**
     * Set the name of the brand.
     *
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name) : self
    {
        return $this->setAttribute(self::FIELD_NAME, $name);
    }

    /**
     * Set the description of the brand.
     *
     * @param string $text
     *
     * @return $this
     */
    public function setDescription(string $text) : self
    {
        return $this->setAttribute(self::FIELD_DESCRIPTION, $text);
    }

    /**
     * Set the status of the brand as featured.
     *
     * @return $this
     */
    public function setFeatured() : self
    {
        return $this->setAttribute(self::FIELD_IS_FEATURED, self::STATUS_FEATURED);
    }

    /**
     * Set the status of the brand whether as not featured.
     *
     * @return $this
     */
    public function setNotFeatured() : self
    {
        return $this->setAttribute(self::FIELD_IS_FEATURED, self::STATUS_NOT_FEATURED);
    }

    // ----------------------------------------------------------------------
    // Scopes
    // ----------------------------------------------------------------------

    /**
     * Scope brands by status.
     *
     * @param     $query
     * @param int $status
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeOfStatus($query, int $status)
    {
        if ($status === self::STATUS_ALL) {
            return $query;
        }

        if ( ! in_array($status, [
            self::STATUS_FEATURED,
            self::STATUS_NOT_FEATURED,
        ])) {
            throw new InvalidStatusException(sprintf("Invalid brand status '%s'", $status));
        }

        return $query->where(self::FIELD_IS_FEATURED, $status);
    }

    /**
     * Scope featured brands.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeFeatured($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_FEATURED);
    }

    /**
     * Scope brands that are not featured.
     *
     * @param $query
     *
     * @return mixed
     * @throws \App\Exceptions\InvalidStatusException
     */
    public function scopeNotFeatured($query)
    {
        return $this->scopeOfStatus($query, self::STATUS_NOT_FEATURED);
    }

    // ----------------------------------------------------------------------
    // Relationships
    // ----------------------------------------------------------------------

    /**
     * The image that belongs to the brand.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphOne
     */
    public function image()
    {
        return $this
            ->morphOne(Image::class, Image::FIELD_MORPHS)
            ->withDefault([
                Image::FIELD_IMAGE_URL => get_default_image(),
                Image::FIELD_THUMB_URL => get_default_image(),
            ]);
    }

    /**
     * The products that belong to the brand.
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function products()
    {
        return $this->hasMany(
            Product::class,
            Product::FIELD_BRAND_ID,
            self::FIELD_PK
        );
    }
}