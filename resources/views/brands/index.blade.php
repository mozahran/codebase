<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .text-bold {
            font-weight: bold;
        }
    </style>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css"
          integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4"
          crossorigin="anonymous">
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"
          integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN"
          crossorigin="anonymous">
</head>
<body>

<div class="container">

    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center mb-3 mt-3 border-bottom">
        <h1 class="h2">Brands</h1>
        @include('layouts.partials.filter', $filterViewOptions)
    </div>

    <div class="d-flex justify-content-between flex-wrap align-items-center">
        @include('layouts.partials.search', ['route' => route('brands.index')])
        @include('layouts.partials.show-per-page', ['routeName' => 'brands.index', 'perPage' => $perPage])
    </div>

    <table class="table">
        <thead>
        <tr>
            <th scope="col" width="1%">#</th>
            <th scope="col">@lang('brands.name')</th>
            <th scope="col" class="text-center">@lang('brands.products_count')</th>
            <th scope="col" class="text-right">@lang('misc.controls')</th>
        </tr>
        </thead>
        <tbody>
        @each('brands.partials.table-row', $pagination->items(), 'item', 'layouts.partials.table-row-empty')
        </tbody>
    </table>

    {{ $pagination->links() }}

</div>

</body>
</html>