<form action="{{ $item->url->delete }}" method="post">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}

    <a href="{{ $item->url->show }}"
       class="btn btn-light btn-sm"
       title="@lang('misc.view') {{ $item->getName() }}">
        <i class="fa fa-fw fa-eye"></i>
    </a>

    <a href="{{ $item->url->edit }}"
       class="btn btn-light btn-sm"
       title="@lang('misc.edit') {{ $item->getName() }}">
        <i class="fa fa-fw fa-pencil"></i>
    </a>

    <button type="submit"
            class="btn btn-danger btn-sm delete-btn"
            title="@lang('misc.delete') {{ $item->getName() }}">
        <i class="fa fa-fw fa-trash-o"></i>
    </button>
</form>
