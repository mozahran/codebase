<tr>
    <td>{{ $item->getId() }}</td>
    <td>{{ $item->getName() }}</td>
    <td class="text-center">{{ number_format($item->products_count) }}</td>
    <td class="text-right">
        @include('brands.partials.table-row-controls', ['item' => $item])
    </td>
</tr>