<div class="btn-toolbar">
    <div class="btn-group mb-2">
        @foreach ($filters as $keyword => $data)
            <a href="{{ route($route,  array_merge($query, [KEYWORD_SHOW => $keyword])) }}"
               class="btn @if ($active == $keyword) btn-primary @else btn-light @endif">
                {{ $data['text'] }}
                <span class="badge badge-light">{{ number_format($data['count']) }}</span>
            </a>
        @endforeach
    </div>
</div>