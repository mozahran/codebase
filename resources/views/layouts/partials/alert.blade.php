<div class="alert alert-{{ $type }}" role="alert">
    @isset($title)
        <h4 class="alert-heading">{{ $title }}</h4>
    @endisset
    {{ $slot }}
    @if (isset($dismissable) && $dismissable != false || ! isset($dismissable))
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    @endif
</div>