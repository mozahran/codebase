{{-- Required params: route --}}
<form action="{{ $route }}" class="form-inline mb-2">
    <input type="search" class="form-control mr-2" name="q" placeholder="@lang('misc.search_input_placeholder')"
           value="{{ request(KEYWORD_SEARCH_QUERY) }}">
    <button type="submit" class="btn btn-primary">
        <i class="fa fa-fw fa-search"></i>
    </button>
    @if (request(KEYWORD_SEARCH_QUERY))
        <a href="{{ $route }}" class="btn btn-link text-danger">
            <i class="fa fa-fw fa-times"></i>@lang('misc.clear_search')
        </a>
    @endif
</form>