{{-- Required params: routeName, perPage --}}
<form action="{{ route($routeName) }}" class="form-inline mb-2">
    <label for="{{ KEYWORD_PER_PAGE }}" class="control-label mr-2">@lang('misc.show_per_page'):</label>
    <select id="{{ KEYWORD_PER_PAGE }}" name="{{ KEYWORD_PER_PAGE }}"
            class="form-control"
            onchange="window.location='{{ route($routeName, array_except($urlQueryStringArray, [KEYWORD_PER_PAGE])) }}&per_page='+this.value">
        @foreach (allowed_results_pagination() as $i)
            <option @if ($perPage == $i) selected @endif value="{{ $i }}">{{ $i }} @lang('misc.items')</option>
        @endforeach
    </select>
</form>