<?php

return [
    'controls' => 'Controls',
    'edit' => 'Edit',
    'delete' => 'Delete',
    'view' => 'View',
    'show' => 'Show',
    'show_all' => 'All',
    'show_per_page' => 'Show per page',
    'items' => 'items',
    'no_results' => 'No resutls!',
    'search' => 'Search',
    'search_input_placeholder' => 'Search...',
    'clear_search' => 'Clear search'
];