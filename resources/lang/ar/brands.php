<?php

return [
    'name' => 'Name',
    'products_count' => 'Products Count',
    'status_featured' => 'Featured',
    'status_not_featured' => 'Not featured',
];