<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class PresenterGenerator 
extends AbstractGenerator 
implements GeneratorInterface
{
    /**
     * PresenterGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     * @param string                         $filename
     * @param string                         $stubFilename
     */
    public function __construct(Collection $params, string $filename, string $stubFilename)
    {
        parent::__construct(
            $params,
            $filename,
            'app/Presenters',
            $stubFilename
        );
    }
}