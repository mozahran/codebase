<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class EventGenerator 
extends AbstractGenerator 
implements GeneratorInterface
{
    /**
     * @var string
     */
    private $action;

    /**
     * EventGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     * @param string                         $action
     */
    public function __construct(Collection $params, string $action)
    {
        $this->action = ucfirst(strtolower($action));

        parent::__construct(
            $params,
            $params->get('name_studly') . $this->action . '.php',
            'app/Events',
            'event.stub'
        );
    }

    public function generate() : void
    {
        $contents = $this->getStubContents();

        $contents = str_replace('DummyNamespace', $this->params->get('package_namespace'), $contents);
        $contents = str_replace('Dummy', $this->params->get('name_studly'), $contents);
        $contents = str_replace('Action', $this->action, $contents);
        $contents = str_replace('dummy', $this->params->get('name_lower'), $contents);
        $contents = str_replace('dummies', $this->params->get('name_plural_lower'), $contents);

        file_put_contents($this->getOutputPath() . '/' . $this->getFilename(), $contents);
    }


}