<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class ControllerGenerator 
extends AbstractGenerator 
implements GeneratorInterface
{
    /**
     * ControllerGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     * @param null|string                    $outputPath
     */
    public function __construct(Collection $params, ?string $outputPath = null)
    {
        parent::__construct(
            $params,
            $params->get('name_studly') . 'Controller.php',
            'app/Http/Controllers',
            'controller.stub'
        );
    }
}