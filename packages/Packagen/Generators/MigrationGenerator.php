<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class MigrationGenerator
    extends AbstractGenerator
    implements GeneratorInterface
{
    /**
     * MigrationGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     */
    public function __construct(Collection $params)
    {
        parent::__construct(
            $params,
            time() . '_create_' . $params->get('name_plural_lower') . '_table.php',
            'database/migrations',
            'migration.stub'
        );
    }
}