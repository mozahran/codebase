<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

abstract class AbstractGenerator implements GeneratorInterface
{
    /**
     * The path of the stub file.
     *
     * @var string
     */
    protected $stubPath;

    /**
     * The output path where the generated file is going to be saved.
     *
     * @var string
     */
    protected $outputPath;

    /**
     * Package configurations collection.
     *
     * @var Collection
     */
    protected $params;

    /**
     * The filename of the generated file.
     *
     * @var string
     */
    protected $filename;

    /**
     * AbstractPackageGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     * @param string                         $filename
     * @param string                         $outputPath
     * @param string                         $stubFilename
     */
    public function __construct(Collection $params, string $filename, string $outputPath, string $stubFilename)
    {
        $this->params = $params;

        $outputPath = $params->get('package_path') . '/' . $outputPath;

        $this->findOrCreatePath($outputPath);

        $this->setFilename($filename);
        $this->setOutputPath($outputPath);
        $this->setStubPath($params->get('stubs_path') . '/' . $stubFilename);
    }

    /**
     * Makes sure the path exists by creating it if not.
     *
     * @param string $path
     */
    private function findOrCreatePath(string $path)
    {
        if ( ! is_dir($path)) {
            exec('mkdir -p ' . $path);
        }
    }

    /**
     * @inheritDoc
     */
    public function setStubPath(string $path) : void
    {
        $this->stubPath = $path;
    }

    /**
     * @inheritDoc
     */
    public function setOutputPath(string $path) : void
    {
        $this->outputPath = $path;
    }

    /**
     * @inheritdoc
     */
    public function getOutputPath() : string
    {
        return $this->outputPath;
    }

    /**
     * @inheritDoc
     */
    public function getStubContents() : string
    {
        return file_get_contents($this->stubPath);
    }

    /**
     * @inheritDoc
     */
    public function setFilename(string $name) : void
    {
        $this->filename = $name;
    }

    /**
     * @inheritdoc
     */
    public function getFilename() : string
    {
        return $this->filename;
    }

    /**
     * @inheritDoc
     */
    public function generate() : void
    {
        $contents = $this->getStubContents();

        $contents = str_replace('DummyNamespace', $this->params->get('package_namespace'), $contents);
        $contents = str_replace('Dummy', $this->params->get('name_studly'), $contents);
        $contents = str_replace('Dummies', $this->params->get('name_plural_studly'), $contents);
        $contents = str_replace('dummy', $this->params->get('name_lower'), $contents);
        $contents = str_replace('dummies', $this->params->get('name_plural_lower'), $contents);

        file_put_contents($this->getOutputPath() . '/' . $this->getFilename(), $contents);
    }
}