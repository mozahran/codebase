<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class FactoryGenerator
    extends AbstractGenerator
    implements GeneratorInterface
{
    /**
     * FactoryGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     */
    public function __construct(Collection $params)
    {
        parent::__construct(
            $params,
            $params->get('name_studly') . 'Factory.php',
            'database/factories',
            'factory.stub'
        );
    }
}