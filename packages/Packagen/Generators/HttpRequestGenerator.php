<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class HttpRequestGenerator 
extends AbstractGenerator 
implements GeneratorInterface
{
    /**
     * HttpRequestGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     */
    public function __construct(Collection $params)
    {
        parent::__construct(
            $params,
            $params->get('name_studly') . 'Request.php',
            'app/Http/Requests',
            'http-request.stub'
        );
    }
}