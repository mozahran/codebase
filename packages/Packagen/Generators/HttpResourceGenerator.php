<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class HttpResourceGenerator 
extends AbstractGenerator 
implements GeneratorInterface
{
    /**
     * HttpResourceGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     */
    public function __construct(Collection $params)
    {
        parent::__construct(
            $params,
            $params->get('name_studly') . 'Resource.php',
            'app/Http/Resources',
            'http-resource.stub'
        );
    }
}