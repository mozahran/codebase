<?php

namespace Packagen\Generators;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;

class ModelGenerator 
extends AbstractGenerator 
implements GeneratorInterface
{
    /**
     * ModelGenerator constructor.
     *
     * @param \Illuminate\Support\Collection $params
     */
    public function __construct(Collection $params)
    {
        parent::__construct(
            $params,
            $params->get('name_studly') . '.php',
            'app',
            'model.stub'
        );
    }
}