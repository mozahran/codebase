<?php

namespace Packagen\Interfaces;

interface GeneratorInterface
{
    /**
     * Sets the stub path & filename.
     *
     * @param string $path
     */
    public function setStubPath(string $path) : void;

    /**
     * Sets the output path.
     *
     * @param string $path
     */
    public function setOutputPath(string $path) : void;

    /**
     * Gets the output path.
     *
     * @return string
     */
    public function getOutputPath() : string;

    /**
     * Sets the filename of the generated file.
     *
     * @param string $name
     */
    public function setFilename(string $name) : void;

    /**
     * Gets the filename of the generated file.
     *
     * @return string
     */
    public function getFilename() : string;

    /**
     * Gets the contents of the stub.
     *
     * @return string
     */
    public function getStubContents() : string;

    /**
     * Generate sthe file.
     */
    public function generate() : void;
}