<?php

namespace Packagen\Interfaces;

use Packagen\Interfaces\GeneratorInterface;
use Illuminate\Support\Collection;

interface GeneratorManagerInterface
{
    /**
     * Registers a generator.
     * 
     * @param  GeneratorInterface The generator.
     * @return void
     */
    public function registerGenerator(GeneratorInterface $generator);

    /**
     * Registers a collection of generators.
     * 
     * @param  Collection A collection of generators.
     * @return void
     */
    public function registerGenerators(Collection $generators);

    /**
     * Clears all registered generators.
     * 
     * @return void
     */
    public function clearGenerators();

    /**
     * Executes the generate method on all registered generators.
     * 
     * @return void
     */
    public function generate();
}