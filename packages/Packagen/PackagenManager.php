<?php

namespace Packagen;

use Illuminate\Support\Collection;
use Packagen\Interfaces\GeneratorInterface;
use Packagen\Interfaces\GeneratorManagerInterface;
use Packagen\Traits\HasParams;

class PackagenManager implements GeneratorManagerInterface
{
    use HasParams;

    /**
     * The registered generators array.
     *
     * @var Collection
     */
    private $generators;

    /**
     * PackagenManager constructor.
     *
     * @param string          $packageName The name of the generated package.
     * @param Collection|null $generators A list of generators to be registered.
     */
    public function __construct(string $packageName, Collection $generators = null)
    {
        $this->generators = new Collection();

        $packageName = strtolower(str_singular($packageName));

        $this->params = new Collection([
            // Package Name's Variations
            'name_lower' => $packageName,
            'name_studly' => studly_case($packageName),
            'name_plural_lower' => str_plural($packageName),
            'name_plural_studly' => studly_case(str_plural($packageName)),
            // Paths
            'stubs_path' => __DIR__ . '/Stubs',
//            'package_path' => base_path('packages/Arteamx/src/' . studly_case($packageName)),
            'package_path' => base_path('packages/Arteamx/src/' . studly_case($packageName)),
            // Namespaces
            'base_namespace' => 'Arteamx\\',
            'package_namespace' => 'Arteamx\\' . studly_case($packageName),
        ]);

        $packagePath = $this->params->get('package_path');

        if ( ! is_dir($packagePath)) {
            exec('mkdir -p ' . $packagePath);
        }

        if ($generators !== null) {
            $this->registerGenerators($generators);
        }
    }

    /**
     * @inheritdoc
     */
    public function registerGenerator(GeneratorInterface $generator)
    {
        $this->generators->push($generator);
    }

    /**
     * @inheritdoc
     */
    public function registerGenerators(Collection $generators)
    {
        if ($generators != null) {
            $generators->map(function ($generator) {
                if ($generator instanceof GeneratorInterface) {
                    $this->registerGenerator($generator);
                }
            });
        }
    }

    /**
     * @inheritdoc
     */
    public function clearGenerators()
    {
        $this->generators = new Collection();
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        foreach ($this->generators as $generator) {
            if ($generator instanceof GeneratorInterface) {
                $generator->generate();
            }
        }
    }
}