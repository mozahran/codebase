<?php

namespace Packagen\Traits;

use Illuminate\Support\Collection;

trait HasParams
{
    /**
     * A collection of parameters.
     *
     * @var \Illuminate\Support\Collection
     */
    private $params;

    /**
     * Sets a new parameter.
     *
     * @param string $key
     * @param string  $value
     * @return void
     */
    public function setParam(string $key, string $value) : void
    {
        $this->params->prepend($value, $key);
    }

    /**
     * Sets the parameters.
     *
     * @param \Illuminate\Support\Collection $params
     * @return void
     */
    public function setParams(Collection $params) : void
    {
        $this->params = $params;
    }

    /**
     * Gets all set parameters.
     *
     * @return \Illuminate\Support\Collection
     */
    public function getParams() : Collection
    {
        return $this->params;
    }

    /**
     * Clears all set parameters.
     * 
     * @return void
     */
    public function clearParams() : void
    {
        $this->params = new Collection();
    }
}