<?php

namespace DummyNamespace;

use DummyScopesNamespace\ScopeInterface;

trait HasScopes
{
    /**
     * Registered scopes to be applied on the query below.
     *
     * @var array
     */
    private $scopes = [];

    /**
     * Add scope to the query.
     *
     * @param ScopeInterface $scope
     * @return void
     */
    public function addScope(ScopeInterface $scope) : void
    {
        $this->scopes[] = $scope;
    }

    /**
     * Add an array of scopes to the current query.
     *
     * @param array $scopes
     * @return void
     */
    public function addScopes(array $scopes) : void
    {
        foreach ($scopes as $scope) {
            if ($scope instanceof ScopeInterface) {
                $this->addScope($scope);
            }
        }
    }

    /**
     * Get all registered scopes.
     *
     * @return array
     */
    public function getScopes() : array
    {
        return $this->scopes;
    }

    /**
     * Apply all registered scopes to the current query.
     *
     * @param $query
     * @param void
     */
    protected function applyScopes(&$query) : void
    {
        foreach ($this->scopes as $scope) {
            if ($scope instanceof ScopeInterface) {
                $scope->apply($query);
            }
        }
    }

    /**
     * Clear all registered scopes.
     *
     * @return void
     */
    public function clearScopes() : void {
        $this->scopes = [];
    }
}