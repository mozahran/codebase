<?php

use App\Codebase\Constants\Fields;
use App\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    return [
        Fields::NAME => $faker->name,
        Fields::DESCRIPTION => $faker->text,
        Fields::PRICE => $faker->randomDigit,
        Fields::QUANTITY => $faker->randomDigit,
        Fields::VAT => $faker->randomDigit,
    ];
});
