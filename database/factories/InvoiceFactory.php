<?php

use App\Invoice;
use Faker\Generator as Faker;

$factory->define(Invoice::class, function (Faker $faker) {
    return [
        Invoice::FIELD_QTY => $faker->randomDigit,
        Invoice::FIELD_TOTAL => $faker->randomDigit,
        Invoice::FIELD_IS_PAID => $faker->boolean,
        Invoice::FIELD_COMMENTS => $faker->sentence,
    ];
});
