<?php

use App\Post;
use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        Post::FIELD_TITLE => $faker->sentence,
        Post::FIELD_CONTENT => $faker->text,
    ];
});
