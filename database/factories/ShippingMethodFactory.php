<?php

use App\ShippingMethod;
use Faker\Generator as Faker;

$factory->define(ShippingMethod::class, function (Faker $faker) {
    return [
        ShippingMethod::FIELD_NAME => $faker->name,
        ShippingMethod::FIELD_DESCRIPTION => $faker->sentence,
        ShippingMethod::FIELD_PRICE => $faker->randomDigit,
        ShippingMethod::FIELD_STATUS => $faker->boolean(100),
    ];
});
