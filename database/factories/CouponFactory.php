<?php

use App\Coupon;
use Faker\Generator as Faker;

$factory->define(Coupon::class, function (Faker $faker) {
    return [
        Coupon::FIELD_DESCRIPTION => $faker->sentence,
        Coupon::FIELD_CODE => $faker->randomDigit,
        Coupon::FIELD_DISCOUNT_TYPE => $faker->randomElement([
            Coupon::DISCOUNT_TYPE_PERCENTAGE_OFF_EACH_ITEM,
            Coupon::DISCOUNT_TYPE_AMOUNT_OFF_ORDER_TOTAL,
            Coupon::DISCOUNT_TYPE_AMOUNT_OFF_EACH_ITEM,
        ]),
        Coupon::FIELD_DISCOUNT_AMOUNT => $faker->randomDigit,
        Coupon::FIELD_USES_PER_COUPON => 1,
        Coupon::FIELD_USES_PER_USER => 1,
        Coupon::FIELD_USAGE_COUNT => 0,
        Coupon::FIELD_FREE_SHIPPING => false,
        Coupon::FIELD_IS_ENABLED => true,
        Coupon::FIELD_EXPIRES_AT => $faker->dateTimeBetween('+1month', '+15months'),
    ];
});
