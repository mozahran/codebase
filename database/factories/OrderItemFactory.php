<?php

use App\OrderItem;
use Faker\Generator as Faker;

$factory->define(OrderItem::class, function (Faker $faker) {
    return [
        OrderItem::FIELD_QTY => $faker->randomDigit,
        OrderItem::FIELD_PRICE => $faker->randomDigit,
        OrderItem::FIELD_DISCOUNT => $faker->randomDigit,
        OrderItem::FIELD_TAX => $faker->randomDigit,
        OrderItem::FIELD_SUBTOTAL => $faker->randomDigit,
    ];
});
