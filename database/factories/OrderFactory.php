<?php

use App\Order;
use Faker\Generator as Faker;

$factory->define(Order::class, function (Faker $faker) {
    return [
        Order::FIELD_QTY => $faker->randomDigit,
        Order::FIELD_DISCOUNT => false,
        Order::FIELD_TAX => $faker->randomDigit,
        Order::FIELD_SHIPPING_FEES => false,
        Order::FIELD_TOTAL => $faker->randomDigit,
        Order::FIELD_IS_FREE_SHIPPED => false,
        Order::FIELD_SHIPPED_AT => null,
        Order::FIELD_REFUNDED_AT => null,
        Order::FIELD_CANCELLED_AT => null,
        Order::FIELD_CANCEL_REASON => $faker->sentence,
    ];
});
