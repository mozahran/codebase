<?php

use App\Country;
use Faker\Generator as Faker;

$factory->define(App\Country::class, function (Faker $faker) {
    return [
        Country::FIELD_NAME => $faker->country,
        Country::FIELD_ISO_NAME => $faker->countryISOAlpha3,
    ];
});
