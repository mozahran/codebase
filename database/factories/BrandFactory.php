<?php

use Faker\Generator as Faker;
use App\Brand;

$factory->define(Brand::class, function (Faker $faker) {
    return [
        Brand::FIELD_NAME => $faker->name,
        Brand::FIELD_DESCRIPTION => $faker->paragraph,
    ];
});
