<?php

use App\User;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        User::FIELD_NAME => $faker->name,
        User::FIELD_EMAIL => $faker->unique()->safeEmail,
        User::FIELD_PASSWORD => bcrypt(123456),
        User::FIELD_API_TOKEN => str_random(60),
    ];
});
