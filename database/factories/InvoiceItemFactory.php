<?php

use App\InvoiceItem;
use Faker\Generator as Faker;

$factory->define(InvoiceItem::class, function (Faker $faker) {
    return [
        InvoiceItem::FIELD_NAME => $faker->name,
        InvoiceItem::FIELD_UNIT_PRICE => $faker->randomDigit,
        InvoiceItem::FIELD_QTY => $faker->randomDigit,
        InvoiceItem::FIELD_DISCOUNT => $faker->randomDigit,
        InvoiceItem::FIELD_TAX => $faker->randomDigit,
        InvoiceItem::FIELD_SUBTOTAL => $faker->randomDigit,
        InvoiceItem::FIELD_COMMENTS => $faker->sentence,
    ];
});
