<?php

use App\Term;
use Faker\Generator as Faker;

$factory->define(App\Term::class, function (Faker $faker) {
    return [
        Term::FIELD_NAME => $faker->name,
    ];
});
