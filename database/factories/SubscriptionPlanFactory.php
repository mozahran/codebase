<?php

use App\SubscriptionPlan;
use Faker\Generator as Faker;

$factory->define(App\SubscriptionPlan::class, function (Faker $faker) {
    return [
        SubscriptionPlan::FIELD_NAME => $faker->name,
        SubscriptionPlan::FIELD_FEES => $faker->randomDigit,
        SubscriptionPlan::FIELD_DURATION => $faker->randomDigit,
    ];
});
