<?php

use App\PostMeta;
use Faker\Generator as Faker;

$factory->define(App\PostMeta::class, function (Faker $faker) {
    return [
        PostMeta::FIELD_KEY => $faker->word,
        PostMeta::FIELD_VALUE => $faker->sentence,
    ];
});
