<?php

use App\Image;
use Faker\Generator as Faker;

$factory->define(Image::class, function (Faker $faker) {
    return [
        Image::FIELD_IMAGE_URL => $faker->imageUrl(),
        Image::FIELD_IMAGE_PATH => $faker->image(),
        Image::FIELD_THUMB_URL => $faker->imageUrl(),
        Image::FIELD_THUMB_PATH => $faker->image(),
    ];
});
