<?php

use App\TaxRate;
use Faker\Generator as Faker;

$factory->define(TaxRate::class, function (Faker $faker) {
    return [
        TaxRate::FIELD_NAME => $faker->name,
        TaxRate::FIELD_DESCRIPTION => $faker->sentence,
        TaxRate::FIELD_COST => $faker->randomDigit,
    ];
});
