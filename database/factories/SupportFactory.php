<?php

use App\Support;
use Faker\Generator as Faker;

$factory->define(App\Support::class, function (Faker $faker) {
    return [
        Support::FIELD_SUBJECT => $faker->sentence,
        Support::FIELD_CONTENT => $faker->text
    ];
});
