<?php

use App\TermTaxonomy;
use Faker\Generator as Faker;

$factory->define(App\TermTaxonomy::class, function (Faker $faker) {
    return [
        TermTaxonomy::FIELD_DESCRIPTION => $faker->text,
    ];
});
