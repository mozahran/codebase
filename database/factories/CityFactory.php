<?php

use App\City;
use Faker\Generator as Faker;

$factory->define(App\City::class, function (Faker $faker) {
    return [
        City::FIELD_NAME => $faker->city,
    ];
});
