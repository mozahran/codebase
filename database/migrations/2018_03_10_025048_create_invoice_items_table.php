<?php

use App\InvoiceItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoiceItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(InvoiceItem::TABLE_NAME, function (Blueprint $table) {
            $table->increments(InvoiceItem::FIELD_PK);
            $table->unsignedInteger(InvoiceItem::FIELD_INVOICE_ID);
            $table->morphs(InvoiceItem::MORPH_INVOICEABLE);
            $table->string(InvoiceItem::FIELD_NAME);
            $table->decimal(InvoiceItem::FIELD_UNIT_PRICE);
            $table->decimal(InvoiceItem::FIELD_DISCOUNT)->default(0);
            $table->decimal(InvoiceItem::FIELD_TAX)->default(0);
            $table->unsignedSmallInteger(InvoiceItem::FIELD_QTY);
            $table->decimal(InvoiceItem::FIELD_SUBTOTAL);
            $table->text(InvoiceItem::FIELD_COMMENTS);
            $table->timestamps();

            $table->index(InvoiceItem::FIELD_INVOICE_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(InvoiceItem::TABLE_NAME);
    }
}
