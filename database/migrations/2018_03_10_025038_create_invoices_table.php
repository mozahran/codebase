<?php

use App\Invoice;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInvoicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Invoice::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Invoice::FIELD_PK);
            $table->unsignedInteger(Invoice::FIELD_USER_ID);
            $table->unsignedInteger(Invoice::FIELD_ORDER_ID);
            $table->unsignedInteger(Invoice::FIELD_QTY);
            $table->unsignedInteger(Invoice::FIELD_TOTAL);
            $table->text(Invoice::FIELD_COMMENTS);
            $table->string(Invoice::FIELD_STATUS, 32)->default(Invoice::STATUS_DUE);
            $table->timestamps();

            $table->index(Invoice::FIELD_USER_ID);
            $table->index(Invoice::FIELD_ORDER_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Invoice::TABLE_NAME);
    }
}
