<?php

use App\CouponUser;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponUserTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(CouponUser::TABLE_NAME, function (Blueprint $table) {
            $table->unsignedInteger(CouponUser::FIELD_COUPON_ID);
            $table->unsignedInteger(CouponUser::FIELD_ORDER_ID);
            $table->unsignedInteger(CouponUser::FIELD_USER_ID);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(CouponUser::TABLE_NAME);
    }
}