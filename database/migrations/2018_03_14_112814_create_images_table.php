<?php

use App\Image;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateImagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Image::TABLE_NAME, function (Blueprint $table)
        {
            $table->increments(Image::FIELD_PK);
            $table->morphs(Image::FIELD_MORPHS);
            $table->string(Image::FIELD_NAME)->nullable();
            $table->string(Image::FIELD_DESCRIPTION)->nullable();
            $table->string(Image::FIELD_IMAGE_URL);
            $table->string(Image::FIELD_IMAGE_PATH);
            $table->string(Image::FIELD_THUMB_URL);
            $table->string(Image::FIELD_THUMB_PATH);
            $table->boolean(Image::FIELD_IS_DEFAULT)->default(Image::DEFAULT);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Image::TABLE_NAME);
    }
}