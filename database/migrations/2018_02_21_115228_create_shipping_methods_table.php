<?php

use App\ShippingMethod;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShippingMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(ShippingMethod::TABLE_NAME, function (Blueprint $table) {
            $table->increments(ShippingMethod::FIELD_PK);
            $table->string(ShippingMethod::FIELD_NAME);
            $table->text(ShippingMethod::FIELD_DESCRIPTION)->nullable();
            $table->decimal(ShippingMethod::FIELD_PRICE);
            $table->string(ShippingMethod::FIELD_STATUS, 32)->default(ShippingMethod::STATUS_ENABLED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(ShippingMethod::TABLE_NAME);
    }
}
