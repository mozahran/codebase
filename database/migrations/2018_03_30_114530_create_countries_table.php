<?php

use App\Country;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Country::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Country::FIELD_PK);
            $table->string(Country::FIELD_NAME);
            $table->char(Country::FIELD_ISO_NAME, 3);
            $table->string(Country::FIELD_STATUS, 32)->default(Country::STATUS_ENABLED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Country::TABLE_NAME);
    }
}
