<?php

use App\OrderItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(OrderItem::TABLE_NAME, function (Blueprint $table) {
            $table->increments(OrderItem::FIELD_PK);
            $table->unsignedInteger(OrderItem::FIELD_ORDER_ID);
            $table->unsignedInteger(OrderItem::FIELD_PRODUCT_ID);
            $table->unsignedInteger(OrderItem::FIELD_QTY);
            $table->decimal(OrderItem::FIELD_PRICE);
            $table->decimal(OrderItem::FIELD_DISCOUNT)->default(0);
            $table->decimal(OrderItem::FIELD_TAX)->default(0);
            $table->decimal(OrderItem::FIELD_SUBTOTAL);
            $table->timestamps();

            $table->index(OrderItem::FIELD_ORDER_ID);
            $table->index(OrderItem::FIELD_PRODUCT_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(OrderItem::TABLE_NAME);
    }
}
