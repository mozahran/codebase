<?php

use App\TermTaxonomy;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermTaxonomyTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TermTaxonomy::TABLE_NAME, function (Blueprint $table) {
            // Fields
            $table->bigIncrements(TermTaxonomy::FIELD_PK);
            $table->unsignedBigInteger(TermTaxonomy::FIELD_TERM_ID);
            $table->string(TermTaxonomy::FIELD_TAXONOMY, 32);
            $table->string(TermTaxonomy::FIELD_DESCRIPTION)->nullable();
            $table->unsignedBigInteger(TermTaxonomy::FIELD_PARENT_ID)->default(0);
            $table->unsignedBigInteger(TermTaxonomy::FIELD_COUNT)->default(0);
            // Indices
            $table->unique([
                TermTaxonomy::FIELD_TERM_ID,
                TermTaxonomy::FIELD_TAXONOMY
            ], 'term_id_taxonomy');
            $table->index(TermTaxonomy::FIELD_TAXONOMY, TermTaxonomy::FIELD_TAXONOMY);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TermTaxonomy::TABLE_NAME);
    }
}
