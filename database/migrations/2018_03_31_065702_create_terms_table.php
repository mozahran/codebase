<?php

use App\Term;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Term::TABLE_NAME, function (Blueprint $table) {
            // Fields
            $table->bigIncrements(Term::FIELD_PK);
            $table->string(Term::FIELD_NAME);
            // Indices
            $table->index(Term::FIELD_NAME, Term::FIELD_NAME);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Term::TABLE_NAME);
    }
}
