<?php

use App\City;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(City::TABLE_NAME, function (Blueprint $table) {
            $table->increments(City::FIELD_PK);
            $table->unsignedInteger(City::FIELD_COUNTRY_ID);
            $table->string(City::FIELD_NAME);
            $table->string(City::FIELD_STATUS, 32)->default(City::STATUS_ENABLED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(City::TABLE_NAME);
    }
}
