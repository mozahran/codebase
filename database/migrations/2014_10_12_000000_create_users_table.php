<?php

use App\User;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(User::TABLE_NAME, function (Blueprint $table) {
            // Fields
            $table->increments(User::FIELD_PK);
            $table->unsignedInteger(User::FIELD_COUNTRY_ID)->default(User::DEFAULT_COUNTRY);
            $table->unsignedInteger(User::FIELD_CITY_ID)->default(User::DEFAULT_CITY);
            $table->string(User::FIELD_NAME);
            $table->string(User::FIELD_EMAIL)->unique();
            $table->string(User::FIELD_PASSWORD);
            $table->string(User::FIELD_PHONE_NUMBER)->unique()->nullable();
            $table->string(User::FIELD_API_TOKEN, 60);
            $table->rememberToken();
            $table->boolean(User::FIELD_IS_ACTIVE)->default(User::STATUS_INACTIVE);
            $table->boolean(User::FIELD_IS_SUSPENDED)->default(User::STATUS_NOT_SUSPENDED);
            $table->timestamps();
            // Indices
            $table->index(User::FIELD_COUNTRY_ID, User::FIELD_COUNTRY_ID);
            $table->index(User::FIELD_CITY_ID, User::FIELD_CITY_ID);
            $table->index(User::FIELD_NAME, User::FIELD_NAME);
            $table->index(User::FIELD_API_TOKEN, User::FIELD_API_TOKEN);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(User::TABLE_NAME);
    }
}
