<?php

use App\Subscription;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Subscription::TABLE_NAME, function (Blueprint $table) {
            $table->unsignedInteger(Subscription::FIELD_PLAN_ID);
            $table->unsignedInteger(Subscription::FIELD_USER_ID);
            $table->decimal(Subscription::FIELD_FEES);
            $table->timestamp(Subscription::FIELD_EXPIRES_AT);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Subscription::TABLE_NAME);
    }
}
