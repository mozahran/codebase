<?php

use App\WishListItem;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWishListsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(WishListItem::TABLE_NAME, function (Blueprint $table) {
            $table->increments(WishListItem::FIELD_PK);
            $table->unsignedInteger(WishListItem::FIELD_PRODUCT_ID);
            $table->unsignedInteger(WishListItem::FIELD_USER_ID);
            $table->timestamps();

            $table->index(WishListItem::FIELD_PRODUCT_ID);
            $table->index(WishListItem::FIELD_USER_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(WishListItem::TABLE_NAME);
    }
}
