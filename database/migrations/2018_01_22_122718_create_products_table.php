<?php

use App\Codebase\Constants\Tables;
use App\Codebase\Constants\Fields;
use App\Codebase\Constants\Statuses;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductsTable extends Migration
{
    public function up()
    {
        Schema::create(Tables::PRODUCTS, function (Blueprint $table) {
            // Fields
            $table->increments(Fields::PK);
            $table->unsignedInteger(Fields::CATEGORY_ID);
            $table->unsignedInteger(Fields::BRAND_ID)->nullable();
            $table->unsignedInteger(Fields::USER_ID);
            $table->string(Fields::NAME);
            $table->text(Fields::DESCRIPTION);
            $table->decimal(Fields::PRICE);
            $table->unsignedInteger(Fields::QUANTITY);
            $table->decimal(Fields::VAT)->default(0);
            $table->string(Fields::STATUS, 32)->default(Statuses::PUBLISHED);
            $table->timestamps();
            // Indices
            $table->index(Fields::BRAND_ID);
            $table->index(Fields::CATEGORY_ID);
            $table->index(Fields::USER_ID);
        });
    }

    public function down()
    {
        Schema::dropIfExists(Tables::PRODUCTS);
    }
}