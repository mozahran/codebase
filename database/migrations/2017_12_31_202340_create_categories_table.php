<?php

use App\Category;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Category::TABLE_NAME, function (Blueprint $table) {
            // Fields
            $table->increments(Category::FIELD_PK);
            $table->unsignedInteger(Category::FIELD_PARENT_ID)->default(0);
            $table->string(Category::FIELD_NAME);
            $table->unsignedInteger(Category::FIELD_SORT_ORDER)->nullable();
            $table->boolean(Category::FIELD_IS_ACTIVE)->default(Category::STATUS_ACTIVE);
            $table->timestamps();
            // Indices
            $table->index(Category::FIELD_PARENT_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Category::TABLE_NAME);
    }
}
