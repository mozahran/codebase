<?php

use App\Comment;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Comment::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Comment::FIELD_PK);
            $table->unsignedInteger(Comment::FIELD_USER_ID);
            $table->unsignedInteger(Comment::FIELD_POST_ID);
            $table->text(Comment::FIELD_TEXT);
            $table->string(Comment::FIELD_STATUS, 32)->default(Comment::STATUS_PENDING);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Comment::TABLE_NAME);
    }
}
