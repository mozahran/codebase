<?php

use App\Post;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Post::TABLE_NAME, function (Blueprint $table) {
            $table->bigIncrements(Post::FIELD_PK);
            $table->unsignedInteger(Post::FIELD_CATEGORY_ID);
            $table->unsignedInteger(Post::FIELD_USER_ID);
            $table->string(Post::FIELD_TITLE);
            $table->text(Post::FIELD_CONTENT);
            $table->string(Post::FIELD_TYPE)->default(Post::TYPE_POST);
            $table->string(Post::FIELD_STATUS, 32)->default(Post::STATUS_PUBLISH);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Post::TABLE_NAME);
    }
}
