<?php

use App\Order;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Order::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Order::FIELD_PK);
            $table->unsignedInteger(Order::FIELD_USER_ID);
            $table->unsignedInteger(Order::FIELD_COUPON_ID)->nullable();
            $table->unsignedInteger(Order::FIELD_SHIPPING_METHOD_ID);
            $table->unsignedInteger(Order::FIELD_QTY);
            $table->decimal(Order::FIELD_DISCOUNT)->default(0);
            $table->decimal(Order::FIELD_TAX)->default(0);
            $table->decimal(Order::FIELD_SHIPPING_FEES)->default(0);
            $table->decimal(Order::FIELD_TOTAL);
            $table->boolean(Order::FIELD_IS_FREE_SHIPPED)->default(false);
            $table->enum(Order::FIELD_STATUS, [
                Order::STATUS_PENDING,
                Order::STATUS_ON_HOLD,
                Order::STATUS_SHIPPED,
                Order::STATUS_CANCELLED,
                Order::STATUS_REFUNDED,
            ])->default(Order::STATUS_PENDING);
            $table->timestamp(Order::FIELD_SHIPPED_AT)->nullable();
            $table->timestamp(Order::FIELD_REFUNDED_AT)->nullable();
            $table->timestamp(Order::FIELD_CANCELLED_AT)->nullable();
            $table->unsignedInteger(Order::FIELD_CANCELLED_BY)->nullable();
            $table->text(Order::FIELD_CANCEL_REASON)->nullable();
            $table->text(Order::FIELD_COMMENTS)->nullable();
            $table->timestamps();

            $table->index([Order::FIELD_USER_ID, Order::FIELD_STATUS], Order::FIELD_USER_ID . '_' . Order::FIELD_STATUS);
            $table->index(Order::FIELD_COUPON_ID, Order::FIELD_COUPON_ID);
            $table->index(Order::FIELD_SHIPPING_METHOD_ID, Order::FIELD_SHIPPING_METHOD_ID);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Order::TABLE_NAME);
    }
}
