<?php

use App\PostMeta;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostMetasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(PostMeta::TABLE_NAME, function (Blueprint $table) {
            $table->increments(PostMeta::FIELD_PK);
            $table->unsignedBigInteger(PostMeta::FIELD_POST_ID);
            $table->string(PostMeta::FIELD_KEY);
            $table->text(PostMeta::FIELD_VALUE);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(PostMeta::TABLE_NAME);
    }
}
