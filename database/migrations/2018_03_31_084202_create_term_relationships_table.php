<?php

use App\TermRelationship;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTermRelationshipsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TermRelationship::TABLE_NAME, function (Blueprint $table) {
            // Fields
            $table->unsignedBigInteger(TermRelationship::FIELD_OBJECT_ID);
            $table->string(TermRelationship::FIELD_OBJECT_TYPE);
            $table->unsignedBigInteger(TermRelationship::FIELD_TERM_TAXONOMY_ID);
            $table->unsignedInteger(TermRelationship::FIELD_TERM_ORDER)->default(0);
            // Indices
            $table->primary([
                TermRelationship::FIELD_OBJECT_ID,
                TermRelationship::FIELD_OBJECT_TYPE,
                TermRelationship::FIELD_TERM_TAXONOMY_ID
            ], 'object_term_taxonomy');
            $table->index(
                TermRelationship::FIELD_TERM_TAXONOMY_ID,
                TermRelationship::FIELD_TERM_TAXONOMY_ID
            );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TermRelationship::TABLE_NAME);
    }
}
