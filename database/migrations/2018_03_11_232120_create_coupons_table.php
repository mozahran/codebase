<?php

use App\Coupon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCouponsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Coupon::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Coupon::FIELD_PK);
            $table->string(Coupon::FIELD_DESCRIPTION);
            $table->string(Coupon::FIELD_CODE);
            $table->string(Coupon::FIELD_DISCOUNT_TYPE);
            $table->decimal(Coupon::FIELD_DISCOUNT_AMOUNT);
            $table->unsignedInteger(Coupon::FIELD_USES_PER_COUPON)->default(0);
            $table->unsignedInteger(Coupon::FIELD_USES_PER_USER)->default(0);
            $table->unsignedInteger(Coupon::FIELD_USAGE_COUNT)->default(0);
            $table->boolean(Coupon::FIELD_FREE_SHIPPING)->default(0);
            $table->text(Coupon::FIELD_PRODUCT_IDS)->nullable();
            $table->text(Coupon::FIELD_EXCLUDED_PRODUCT_IDS)->nullable();
            $table->text(Coupon::FIELD_PRODUCT_CATEGORIES)->nullable();
            $table->text(Coupon::FIELD_EXCLUDED_PRODUCT_CATEGORIES)->nullable();
            $table->string(Coupon::FIELD_STATUS, 32)->default(Coupon::STATUS_ENABLED);
            $table->timestamp(Coupon::FIELD_EXPIRES_AT)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Coupon::TABLE_NAME);
    }
}
