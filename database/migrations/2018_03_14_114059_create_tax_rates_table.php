<?php

use App\TaxRate;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTaxRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(TaxRate::TABLE_NAME, function (Blueprint $table) {
            $table->increments(TaxRate::FIELD_PK);
            $table->string(TaxRate::FIELD_NAME);
            $table->text(TaxRate::FIELD_DESCRIPTION);
            $table->decimal(TaxRate::FIELD_COST);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(TaxRate::TABLE_NAME);
    }
}