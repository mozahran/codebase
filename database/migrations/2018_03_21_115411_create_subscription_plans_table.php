<?php

use App\SubscriptionPlan;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubscriptionPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(SubscriptionPlan::TABLE_NAME, function (Blueprint $table) {
            $table->increments(SubscriptionPlan::FIELD_PK);
            $table->string(SubscriptionPlan::FIELD_NAME);
            $table->decimal(SubscriptionPlan::FIELD_FEES);
            $table->unsignedInteger(SubscriptionPlan::FIELD_DURATION);
            $table->string(SubscriptionPlan::FIELD_STATUS, 32)->default(SubscriptionPlan::STATUS_ENABLED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(SubscriptionPlan::TABLE_NAME);
    }
}
