<?php

use App\Brand;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Brand::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Brand::FIELD_PK);
            $table->string(Brand::FIELD_NAME);
            $table->text(Brand::FIELD_DESCRIPTION);
            $table->boolean(Brand::FIELD_IS_FEATURED)->default(Brand::STATUS_NOT_FEATURED);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Brand::TABLE_NAME);
    }
}
