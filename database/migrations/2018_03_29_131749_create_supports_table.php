<?php

use App\Support;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSupportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create(Support::TABLE_NAME, function (Blueprint $table) {
            $table->increments(Support::FIELD_PK);
            $table->unsignedInteger(Support::FIELD_PARENT_ID)->default(0);
            $table->unsignedInteger(Support::FIELD_SENDER_ID)->default(0);
            $table->unsignedInteger(Support::FIELD_ABUSER_ID)->nullable();
            $table->unsignedInteger(Support::FIELD_ABUSABLE_ID)->nullable();
            $table->string(Support::FIELD_ABUSABLE_TYPE)->nullable();
            $table->string(Support::FIELD_FULLNAME)->nullable();
            $table->string(Support::FIELD_PHONE_NUMBER)->nullable();
            $table->string(Support::FIELD_EMAIL)->nullable();
            $table->string(Support::FIELD_SUBJECT);
            $table->string(Support::FIELD_CONTENT);
            $table->timestamp(Support::FIELD_READ_AT)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists(Support::TABLE_NAME);
    }
}
